# OpenID Connect Simple Federation

A _draft_ 
[specification](https://openid.net/specs/openid-connect-federation-1_0.html) 
exists for doing OpenID Federation which we deem a little too complex. In order 
to make federations easy to build and use, we propose a simpler way of doing 
this.

While we understand the importance of some of the issues the draft 
specification aims at solving, we prefer to start with something simpler that 
solves the most obvious use case (teach RPs about OPs and the other way around) 
and continue from there. Call it a "Minimal Viable Product" (MVP). Chances are, 
we won't actually need anything (much) more complex.

We do not expect any (for profit) organization to adhere to this specification,
but we'll settle for community RPs and small OPs. OPs and RPs can easily be in 
multiple federations anyway! 😊

**Explicit Goal**: it MUST be possible to convert our metadata to the OpenID 
Federation draft format.

## Goals

- make it easy for RPs and OPs to configure a set of OPs and RPs that come from
  a trusted source, similar to [eduGAIN](https://edugain.org/);
- prescribe required RP and OP configurations that are secure, similar to 
  [saml2int](https://kantarainitiative.github.io/SAMLprofiles/saml2int.html);
- implement lessons from 
  [The OAuth 2.1 Authorization Framework](https://datatracker.ietf.org/doc/draft-ietf-oauth-v2-1/) 
  draft specification.

## OPs

OPs use a discovery file already on the `/.well-known/openid-configuration` in
many cases as part of the 
[OpenID Connect Discovery](https://openid.net/specs/openid-connect-discovery-1_0.html) 
specification. We'll reuse those for the metadata as much as possible. Example:

Look [here](https://openid.net/specs/openid-connect-discovery-1_0.html#ProviderMetadata) for all the options. We'll provide a minimal example that sticks to the defaults as much as possible, unless there is a reason to override it.

### Required by Specification

| Key                                     | Required Value           | Example                            |
| --------------------------------------- | ------------------------ | ---------------------------------- |
| `issuer`                                |                          | `https://op.example.org`           |
| `authorization_endpoint`                |                          | `https://op.example.org/authorize` |
| `token_endpoint`                        |                          | `https://op.example.org/token`     |
| `jwks_uri`                              |                          | `https://op.example.org/jwks`      |
| `response_types_supported`              | `["code"]`               |                                    |
| `grant_types_supported`                 | `["authorization_code"]` |                                    |
| `subject_types_supported`               | `["pairwise"]`           |                                    |
| `id_token_signing_alg_values_supported` | `["RS256"]`              |                                    |

**TODO**: do we also need to support `refresh_token` grant type? Probably!

### Required by Us

| Key                                | Required Value           | Example                           |
| ---------------------------------- | ------------------------ | --------------------------------- |
| `userinfo_endpoint`                |                          | `https://op.example.org/userinfo` |
| `response_modes_supported`         | `["query"]`              |                                   |
| `grant_types_supported`            | `["authorization_code"]` |                                   |
| `code_challenge_methods_supported` | `["S256"]`               |                                   |

Example:

```json
{
    "authorization_endpoint": "https://op.example.org/openid/authorize",
    "code_challenge_methods_supported": [
        "S256"
    ],
    "grant_types_supported": [
        "authorization_code",
        "refresh_token"
    ],
    "id_token_signing_alg_values_supported": [
        "RS256"
    ],
    "issuer": "https://op.example.org",
    "jwks_uri": "https://op.example.org/openid/jwks",
    "response_types_supported": [
        "code"
    ],
    "subject_types_supported": [
        "pairwise"
    ],
    "token_endpoint": "https://op.example.org/openid/token",
    "token_endpoint_auth_methods_supported": [
        "client_secret_basic",
        "client_secret_post"
    ],
    "userinfo_endpoint": "https://op.example.org/openid/userinfo"
}
```

We use this information to create an entry for the OP. We'll at some point 
describe which elements MUST be there and which values they are allowed to 
have. It will be more strict and limited compared to the discovery 
specification.

When using the discovery file we create a `jwks` entry by taking the contents
of the document `jwks_uri` points to as is also done in 
[OpenID Connect Dynamic Client Registration](https://openid.net/specs/openid-connect-registration-1_0.html).

The above example modified this way:

```json
{
    "authorization_endpoint": "https://op.example.org/openid/authorize",
    "code_challenge_methods_supported": [
        "S256"
    ],
    "grant_types_supported": [
        "authorization_code",
        "refresh_token"
    ],
    "id_token_signing_alg_values_supported": [
        "RS256"
    ],
    "issuer": "https://op.example.org",
    "jwks": {
        "keys": [
            {
                "alg": "RS256",
                "e": "AQAB",
                "kty": "RSA",
                "n": "5iXNMaNApJif9J56RGpgLFHY5Ea1WT-FAGMv5GJvO8DFGiNK_9KejcUN5l5GKIh-Esyy1cesAjzgz9wWOLU0NiH4iLHOv36tm6KKgEAoqsNam6ZYlb2KZdC21qN_YkdAoL3BojbO0eGoi1X70TTO0kw0ADy6w9T5RyW2Oiex2VLGlc88ObF00JOUDSrNIMr6r-Z4YOyepdYiSpmZqOmSfBYJMe4PiMXsa68spSqNuntWUQl8ZQJFPbHa9UyQ06fPryBfd3fHdGRG6AoeIorz_k1vvtczbQj-r70rT19HrqvPl_gt1o0im8fsOY5VwzPvkhw08h6mZiYG9sJyyZCkJQ",
                "use": "sig"
            }
        ]
    },
    "response_types_supported": [
        "code"
    ],
    "subject_types_supported": [
        "pairwise"
    ],
    "token_endpoint": "https://op.example.org/openid/token",
    "token_endpoint_auth_methods_supported": [
        "client_secret_basic",
        "client_secret_post"
    ],
    "userinfo_endpoint": "https://op.example.org/openid/userinfo"
}
```

## RPs

RPs generally do not have a `/.well-known/openid-configuration` endpoint, so it 
is more difficult to automatically process it, but there is the 
[OpenID Connect Dynamic Client Registration](https://openid.net/specs/openid-connect-registration-1_0.html)
and [OAuth 2.0 Dynamic Client Registration Protocol](https://www.rfc-editor.org/rfc/rfc7591)
specifications. We'll (use those as much as possible.

Example:

```json
{
    "client_id": "https://rp.example.org",
    "client_name": "My RP",
    "redirect_uris": [
        "https://rp.example.org/callback"
    ],
    "scope": "openid profile"
}
```

**TODO**: not sure the `client_id` is a good idea like this, but also keeping
it the "HTTP Origin" here (like `iss` for OPs) might be a good idea?

The default `scope`, if not specified, is `openid` which would only give 
access to the `sub` field.

## Client Registration

As all information in the metadata is public we can't use `client_secret`. So 
all RPs are "public". As we want to make it possible to support the `userinfo`
endpoint, we need to use the `code` flow.

So in this context, all OPs (and RPs) MUST support the `code` flow with public
clients.

### Client ID

Typically the OPs would generate a `client_id` and `client_secret` per RP. When
using metadata, we define the `client_id` to have the exact same value as the
`iss`, usually the HTTP Origin `https://op.example.org`.

In order to be more safe against authorization code misuse, we'll require the 
use of 
[Proof Key for Code Exchange by OAuth Public Clients](https://datatracker.ietf.org/doc/html/rfc7636). 

This protect against browser URL sniffing, but explicitly not against the user 
themselves. For this we _could_ also prescribe the use of JWE (encrypted JWTs), 
but that is really a 
[can or worms](https://paragonie.com/blog/2017/03/jwt-json-web-tokens-is-bad-standard-that-everyone-should-avoid) 
we'd rather keep closed.

## Metadata

The format:

```json
{
    "v": 0,
    "metadata": {
        "openid_relying_parties": [
            ...
        ],
        "openid_providers": [
            ...
        ]
    }
}
```

The `v` field indicates the version of the document. It is always `0` until 
such time a new specification is created.

Example:

```json
{
    "metadata": {
        "openid_relying_parties": [
            {
                "client_id": "https://vpn.example.org",
                "client_name": "(eduVPN) vpn.example.org",
                "redirect_uris": [
                    "https://vpn.example.org/vpn-user-portal/redirect_uri"
                ],
                "scope": "openid profile"
            }
        ]
    },
    "v": 0
}
```

### Signature

The metadata document is signed using a JWS, in "detached" mode. The JWT is 
returned as a HTTP response header `X-Metadata-Sig` when fetching the 
document, e.g.:

```
X-Metadata-Sig: eyJhbGciOiJSUzI1NiJ9..QxVg-UJ9e4qshf3EC2WCotGSLfT5trus__E1IaEQlnLNhoFujYxJfJ6yyiyqlQsQA24aAiPy5nrWKDxA3oBWtWpO16w2N10jNeGJLP0oDQJVIfWB0wRrZ9ETNoOYoczN8AbXiXYBF4L3e7v_aAQnOwGRdxCT_cEmbqFNZ2pdMRa24n7_maJjT-vTFJOkNfdMDByzojyOZwufazCnFQvpsGcDC9bZDo4RR2oGjwYcgkFgEaL3FvrTE5Y2WO74gKJzE2dwbp9FqWBmtrtLLyqf15OG7IqQzWME5RqEkk6gn1XR5JC9VvAy_v3W5eAaJ4IOtrPug3zWZk3ygn9fdf8dTA
```

The detached signature structure is documented in 
[here](https://www.rfc-editor.org/rfc/rfc7515.html#appendix-F) in Appendix F
of RFC 7515.

In order to verify the signature using "normal" JWT verification, you take the
response body, Base64UrlSafe encode it and add it as the payload before 
validating the signature.

**NOTE**: if at some point the metadata file becomes too big it might make 
sense to look into 
[JSON Web Signature (JWS) Unencoded Payload Option](https://datatracker.ietf.org/doc/html/rfc7797).

## Crypto Algorithms

The OP signs the `id_token` using `RS256` (legacy) or `EdDSA` (modern). There
is no need to implement any of the other algorithms. The metadata will also 
be signed by `RS256` and at a later date using `EdDSA` (as well).

Technically the `id_token` doesn't need to be signed (I think?) as the `code`
flow is used together with PKCE and TLS, so nobody else can obtain a valid 
`access_token` and `refresh_token`, or `id_token`.
