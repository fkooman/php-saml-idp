<?php

declare(strict_types=1);

/*
 * Copyright (c) 2018-2024 François Kooman <fkooman@tuxed.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

require_once 'vendor/autoload.php';
$baseDir = dirname(__DIR__);

use fkooman\Jwt\Jwt;
use fkooman\OAuth\Server\ClientInfo;
use fkooman\OAuth\Server\PdoClientDb;
use fkooman\SAML\IdP\Base64UrlSafe;
use fkooman\SAML\IdP\CurlHttpClient;
use fkooman\SAML\IdP\Json;
use fkooman\SAML\IdP\OIDC\FederationDb;
use fkooman\SAML\IdP\Storage;

try {
    $httpClient = new CurlHttpClient();
    $federationDb = new FederationDb();
    foreach ($federationDb->getAll() as $federationInfo) {
        $httpResponse = $httpClient->get($federationInfo->metadataUrl(), []);
        if (null === $mdSig = $httpResponse->getHeader('X-Metadata-Sig')) {
            throw new Exception('no "X-Metadata-Sig" header');
        }
        $jsonBody = $httpResponse->getBody();
        $encodedBodyHash = Base64UrlSafe::encodeUnpadded(hash('sha256', $jsonBody, true));
        //    echo 'Sig    : '.$mdSig.PHP_EOL;
        //    echo 'SHA-256: '.$encodedBodyHash.PHP_EOL;
        $decodedToken = Jwt::decode($federationInfo->publicKey(), $mdSig);
        if (0 !== $decodedToken['v']) {
            throw new Exception('invalid token version');
        }
        if (!hash_equals($encodedBodyHash, $decodedToken['h'])) {
            throw new Exception('invalid hash');
        }

        // extract the RPs from the metadata and import them
        $mdData = Json::decode($jsonBody);
        if (0 !== $mdData['v']) {
            throw new Exception('invalid metadata version');
        }

        $storage = new Storage(
            sprintf('sqlite:%s/data/db.sqlite', $baseDir),
            $baseDir . '/schema'
        );
        $clientDb = new PdoClientDb($storage->dbPdo(), 'oidc_rps');

        if (array_key_exists('metadata', $mdData)) {
            if (array_key_exists('openid_relying_parties', $mdData['metadata'])) {
                if (is_array($mdData['metadata']['openid_relying_parties'])) {
                    foreach ($mdData['metadata']['openid_relying_parties'] as $clientData) {
                        $clientInfo = ClientInfo::fromData($clientData);
                        if (null === $clientDb->get($clientInfo->clientId())) {
                            echo 'Adding: ' . $clientInfo->clientId() . ' (' . $clientInfo->displayName() . ')' . PHP_EOL;
                            $clientDb->add($clientInfo);

                            continue;
                        }
                        echo 'Updating: ' . $clientInfo->clientId() . ' (' . $clientInfo->displayName() . ')' . PHP_EOL;
                        $clientDb->update($clientInfo);
                    }
                }
            }
        }
    }
} catch (Exception $e) {
    echo 'ERROR: ' . $e->getMessage() . PHP_EOL;
    exit(1);
}
