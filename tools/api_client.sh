#!/bin/sh

if [ "Darwin" == "$(uname)" ]; then
    BROWSER=open        # macOS
else
    BROWSER=xdg-open	# other
fi

OP_URL="http://localhost:8080"
PORT=12345
REDIRECT_URI=http://127.0.0.1:${PORT}/callback
CLIENT_ID=99e9ef4ece39b6d4fd9f893f96921206
SCOPE="openid profile email"
STATE=$(openssl rand -base64 32 | tr '/+' '_-' | tr -d '=' | tr -d '\n')
SERVER_INFO_URL="${OP_URL}/.well-known/openid-configuration"
SERVER_INFO=$(curl -s "${SERVER_INFO_URL}")
AUTHZ_ENDPOINT=$(echo "${SERVER_INFO}" | jq -r '.authorization_endpoint')
TOKEN_ENDPOINT=$(echo "${SERVER_INFO}" | jq -r '.token_endpoint')
AUTHZ_URL="${AUTHZ_ENDPOINT}?client_id=${CLIENT_ID}&redirect_uri=${REDIRECT_URI}&response_type=code&scope=${SCOPE}&state=${STATE}"

# open the browser
${BROWSER} "${AUTHZ_URL}" &

SERVER() {
	P=/tmp/$$.fifo
	trap "rm ${P}" EXIT
	mkfifo ${P}
	cat ${P} | nc -l ${PORT} | while read -r L
	do
        echo "${L}"
		printf 'HTTP/1.0 200 OK\r\nContent-Type: text/plain\r\n\r\nAll done! Close browser tab.' >${P}
        break
    done
}

RESPONSE=$(SERVER)
ERROR=$(echo "${RESPONSE}" | cut -d '?' -f 2 | cut -d ' ' -f 1 | tr '&' '\n' | grep ^error | cut -d '=' -f 2)
if [ -n "${ERROR}" ]
then
    echo "ERROR: ${ERROR}"
    exit 1
fi

RESPONSE_STATE=$(echo "${RESPONSE}" | cut -d '?' -f 2 | cut -d ' ' -f 1 | tr '&' '\n' | grep ^state | cut -d '=' -f 2)
RESPONSE_CODE=$(echo "${RESPONSE}" | cut -d '?' -f 2 | cut -d ' ' -f 1 | tr '&' '\n' | grep ^code | cut -d '=' -f 2)

if [ "${STATE}" != "${RESPONSE_STATE}" ]
then
    echo "ERROR: request/response state does NOT match!"
    exit 1
fi

# use response code to obtain access token
TOKEN_RESPONSE=$(curl -s -d 'grant_type=authorization_code' -d "client_id=${CLIENT_ID}" -d "code=${RESPONSE_CODE}" -d "redirect_uri=${REDIRECT_URI}" "${TOKEN_ENDPOINT}")
BEARER_TOKEN=$(echo "${TOKEN_RESPONSE}" | jq -r '.access_token')
USERINFO_ENDPOINT=$(echo "${SERVER_INFO}" | jq -r '.userinfo_endpoint')

# obtain userinfo
echo "curl -s -H \"Authorization: Bearer ${BEARER_TOKEN}\" \"${USERINFO_ENDPOINT}\" | jq -S --indent 4"
curl -s -H "Authorization: Bearer ${BEARER_TOKEN}" "${USERINFO_ENDPOINT}" | jq -S --indent 4
