<?php declare(strict_types=1); ?>
<?php /** @var \fkooman\Template\Tpl $this */ ?>
<?php /** @var array<\fkooman\SAML\IdP\SAML\SpInfo> $spList */ ?>
<?php /** @var array<\fkooman\SAML\IdP\SAML\SpInfo> $spDb */ ?>
<?php /** @var array<string> $attributeNameList */ ?>
<?php /** @var array<array{entity_id:?string,from_attribute:?string,from_value:?string,to_attribute:string,to_value:?string}> $attributeMappingList */ ?>
<?php $this->layout('base'); ?>
<?php $this->start('main'); ?>
<h1><?=$this->t('SAML');?></h1>
<?php if (0 === count($spList)): ?>
<h2>SPs</h2>
    <p>
        <em>No SPs registered yet.</em>
    </p>
<?php else: ?>
    <table>
        <thead>
            <tr>
                <th>Name</th>
            </tr>
        </thead>
        <tbody>
<?php foreach($spList as $spInfo): ?>
            <tr>
                <td><a href="<?=$this->u('sp', ['entity_id' => $spInfo->entityId()]);?>"><?=$this->e($spInfo->displayName());?></a></td>
            </tr>
<?php endforeach; ?>
        </tbody>
    </table>
<?php endif; ?>

    <h3 id="add"><?=$this->t('Add SP');?></h3>

    <form method="post">
        <input type="hidden" name="action" value="addSp">
        <label>Name<sup>*</sup> <input type="text" required name="display_name" placeholder="Name"></label>
        <label>Entity ID<sup>*</sup> <input type="text" required name="entity_id" placeholder="Entity ID"></label>
        <label>ACS URL<sup>*</sup> <input type="text" required name="acs_url" placeholder="HTTP-POST ACS URL"></label>
        <button class="success" type="submit"><?=$this->t('Add');?></button>
    </form>

    <h3>Import SPs</h3>
    <p>For your convenience we have a list of common SPs which you can easily import in your IdP.</p>
        <form method="post">
        <select name="entity_id">
<?php foreach($spDb as $spInfo): ?>
            <option value="<?=$this->e($spInfo->entityId());?>"><?=$this->e($spInfo->displayName());?></option>
<?php endforeach; ?>
        </select>
        <button class="success" type="submit" name="action" value="importSp">Import</button>
    </form>

    <h2>Attribute Mapping</h2>
<?php if(0 === count($attributeMappingList)): ?>
    <p>
        <em>No attribute mappings yet.</em>
    </p>
<?php else: ?>
    <p>
        These mappings apply to <em>all</em> SPs. For SP specific mappings, see
        the SP configuration.
    </p>
    <table>
        <thead>
            <tr>
                <th>From Attribute</th>
                <th>From Value</th>
                <th>To Attribute</th>
                <th>To Value</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
<?php foreach($attributeMappingList as $attributeMapping): ?>
            <tr>
                <td>
<?php if(null === $attributeMapping['from_attribute']): ?>
-
<?php else: ?>
<?= $this->e($attributeMapping['from_attribute']);?>
<?php endif; ?>
                </td>
                <td>
<?php if(null === $attributeMapping['from_value']): ?>
-
<?php else: ?>
<code><?=$this->e($attributeMapping['from_value']);?></code>
<?php endif; ?>
                </td>
                <td>
                    <?=$this->e($attributeMapping['to_attribute']);?>
                </td>
                <td>
<?php if(null === $attributeMapping['to_value']): ?>
-
<?php else: ?>
<code><?=$this->e($attributeMapping['to_value']);?></code>
<?php endif; ?>
                </td>
<!--                <td class="end">
                    <form method="post">
                        <input type="hidden" name="action" value="deleteMapping">
                        <input type="hidden" name="attributeName" value="X">
                        <input type="hidden" name="attributeValue" value="Y">
                        <button class="error" type="submit">Delete</button>
                    </form>
                </td>
-->
            </tr>
<?php endforeach; ?>
        </tbody>
    </table>
<?php endif; ?>

    <h3>Add Mapping</h3>
    <form method="post">
        <label>From Attribute <input type="text" list="attributeNameList" name="from_attribute" placeholder="From Attribute"></label>
        <datalist id="attributeNameList">
<?php foreach($attributeNameList as $attributeName): ?>
        <option value="<?=$this->e($attributeName);?>"><?=$this->e($attributeName);?></option>
<?php endforeach; ?>
        </datalist>
        <label>From Value <input type="text" name="from_value" placeholder="From Value"></label>
        <label>To Attribute<sup>*</sup> <input type="text" list="attributeNameList" name="to_attribute" placeholder="To Attribute" required></label>
        <label>To Value <input type="text" name="to_value" placeholder="To Value"></label>
        <button class="success" name="action" value="addMapping" type="submit"><?=$this->t('Add');?></button>
    </form>
<?php $this->stop('main'); ?>
