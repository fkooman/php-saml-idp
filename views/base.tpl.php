<?php declare(strict_types=1); ?>
<?php /** @var \fkooman\Template\Tpl $this */ ?>
<?php /** @var bool $showLogoutButton */ ?>
<?php /** @var string $displayName */ ?>
<?php /** @var bool $enableUserManagement */ ?>
<?php /** @var bool $enablePasswordReset */ ?>
<?php /** @var ?\fkooman\SAML\IdP\Http\UserInfo $userInfo */ ?>
<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="application-name" content="php-saml-idp">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?=$this->e($displayName); ?></title>
    <link href="<?=$this->a('css/screen.css');?>" media="screen" rel="stylesheet">
</head>
<body>
    <header>
        <nav>
            <ul>
                <li><strong><?=$this->e($displayName);?></strong></li>
            </ul>
            <ul>
<?php if ($showLogoutButton): ?>
            <li><a href="<?=$this->u('');?>"><?=$this->t('Home'); ?></a></li>
<?php if ($enablePasswordReset): ?>
            <li><a href="<?=$this->u('passwd');?>"><?=$this->t('Password'); ?></a></li>
<?php endif; ?>
<?php if (null !== $userInfo && $userInfo->isAdmin()): ?>
<?php if ($enableUserManagement): ?>
            <li><a href="<?=$this->u('users');?>"><?=$this->t('Users'); ?></a></li>
            <li><a href="<?=$this->u('groups');?>"><?=$this->t('Groups'); ?></a></li>
<?php endif; ?>
            <li><a href="<?=$this->u('rps');?>"><?=$this->t('OpenID Connect'); ?></a></li>
            <li><a href="<?=$this->u('sps');?>"><?=$this->t('SAML'); ?></a></li>
<?php endif; ?>
<?php endif; ?>
            </ul>
            <ul>
<?php if ($showLogoutButton): ?>
                <li><a href="<?=$this->u('_logout');?>"><strong><?=$this->t('Sign Out'); ?></strong></a></li>
<?php endif; ?>
            </ul>
        </nav>
    </header>
    <main>
        <?= $this->section('main'); ?>
    </main>
    <footer>
        <small>Powered by <a href="https://codeberg.org/fkooman/php-saml-idp">php-saml-idp</a></small>
	</footer>
</body>
</html>
