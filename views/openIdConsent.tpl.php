<?php declare(strict_types=1); ?>
<?php /** @var \fkooman\Template\Tpl $this */ ?>
<?php /** @var array{client_id:string,display_name:string,scope:string,redirect_uri:string,max_age:?int,acr_values:?array<string>} $authzInfo */ ?>
<?php $this->layout('base'); ?>
<?php $this->start('main'); ?>
<h1>Approval</h1>
<div class="dialog">
<p>You are about to login to a service. You need to approve this.</p>

<h1><?=$this->e($authzInfo['display_name']);?></h1>

<details><summary>Details...</summary>
<p>
    The following information well be sent to the service. If you do not want this, please close this browser tab.
</p>
<dl>
    <dt>Permission(s)</dt>
    <dd>
        <ul>
<?php foreach(explode(' ', $authzInfo['scope']) as $s): ?>
    	    <li><em><?=$this->e($s);?></em></li>
<?php endforeach; ?>
        </ul>
    </dd>
</dl>
</details>

<form method="post">
    <button class="success" type="submit" name="approve" value="yes"><?=$this->t('Approve'); ?></button>
</form>
</div>
<?php $this->stop('main'); ?>
