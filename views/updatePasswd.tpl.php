<?php declare(strict_types=1); ?>
<?php /** @var \fkooman\Template\Tpl $this */ ?>
<?php /** @var string $userId */ ?>
<?php $this->layout('base'); ?>
<?php $this->start('main'); ?>
<h1>Password</h1>
<p>
    <?= $this->t('Here you can change the password for your account.'); ?>
</p>

<form method="post">
    <label for="userName"><?= $this->t('Username'); ?></label>
    <input size="30" type="text"     id="userName" name="userName" value="<?= $this->e($userId); ?>" autocapitalize="off" disabled="disabled" required>
    <label for="userPass"><?= $this->t('Current Password'); ?><sup>*</sup></label>
    <input size="30" type="password" id="userPass" name="userPass" placeholder="<?= $this->t('Current Password'); ?>" autofocus required>
    <label for="newUserPass"><?= $this->t('New Password'); ?><sup>*</sup></label>
    <input size="30" type="password" minlength="8" id="newUserPass" name="newUserPass" placeholder="<?= $this->t('New Password'); ?>" required>
    <label for="newUserPassConfirm"><?= $this->t('New Password (confirm)'); ?><sup>*</sup></label>
    <input size="30" type="password" minlength="8" id="newUserPassConfirm" name="newUserPassConfirm" placeholder="<?= $this->t('New Password (confirm)'); ?>" required>
    <button class="success" type="submit"><?= $this->t('Confirm'); ?></button>
</form>
<?php $this->stop('main'); ?>
