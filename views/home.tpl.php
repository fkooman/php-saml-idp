<?php declare(strict_types=1); ?>
<?php /** @var \fkooman\Template\Tpl $this */ ?>
<?php /** @var array<string,array<string>> $attributeList */ ?>
<?php $this->layout('base'); ?>
<?php $this->start('main'); ?>
<h1>Home</h1>
<h2>Attributes</h2>
<p>
    The information we have regarding your account.
</p>
<table>
    <tbody>
<?php foreach ($attributeList as $attributeName => $attributeValueList): ?>
        <tr>
            <th><?=$this->e($attributeName); ?></th>
            <td>
                <ul>
<?php foreach ($attributeValueList as $attributeValue): ?>
                    <li><?=$this->e($attributeValue); ?></li>
<?php endforeach; ?>
                </ul>
            </td>
        </tr>
<?php endforeach; ?>
    </tbody>
</table>
<?php $this->stop('main'); ?>
