<?php declare(strict_types=1); ?>
<?php /** @var \fkooman\Template\Tpl $this */ ?>
<?php $this->layout('base'); ?>
<?php $this->start('main'); ?>
<h1>Two Factor</h1>
<div class="dialog">
<p>
    <?= $this->t('Provide your second factor obtained from your authenticator app.'); ?>
</p>
<form method="post" action="<?= $this->u('_two_factor');?>">
    <input autofocus type="text" inputmode="numeric" name="otpKey" autocapitalize="off" placeholder="<?= $this->t('TOTP'); ?>" autocomplete="off" maxlength="6" required pattern="[0-9]{6}">
    <button class="success" type="submit" name="action" value="verify"><?= $this->t('Verify'); ?></button>
</form>

<details><summary>Skip</summary>
<p>
<?= $this->t('Two factor authentication may be skipped, however, this can result in not being able to use the service at all, or not having access to all desired functionality!'); ?>
</p>
<form method="post" action="<?= $this->u('_two_factor');?>">
    <button class="warning" type="submit" name="action" value="skip"><?= $this->t('Skip'); ?></button>
</form>
</details>

</div>
<?php $this->stop('main'); ?>
