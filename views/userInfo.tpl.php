<?php declare(strict_types=1); ?>
<?php /** @var \fkooman\Template\Tpl $this */ ?>
<?php /** @var string $userId */ ?>
<?php /** @var array<string,array<string>> $attributeList */ ?>
<?php /** @var array<string> $attributeNameList */ ?>
<?php /** @var array{created_at:\DateTimeImmutable,is_disabled:bool} $localUserInfo */ ?>
<?php /** @var bool $hasSecondFactor */ ?>
<?php /** @var array<array{group_id:string,display_name:string}> $userGroupList */ ?>
<?php /** @var array<array{group_id:string,display_name:string}> $groupList */ ?>
<?php $this->layout('base'); ?>
<?php $this->start('main'); ?>
<h1><?=$this->t('User');?></h1>
<table>
    <thead>
        <tr>
            <th>ID</th><th>Created On</th><th></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><?=$this->e($userId);?></td>
            <td><?=$this->e($localUserInfo['created_at']->format('Y-m-d'));?></td>
            <td class="end">
<?php if ($hasSecondFactor) : ?>
                <span class="badge success">2FA</span>
<?php endif; ?>
<?php if($localUserInfo['is_disabled']): ?>
                <span class="badge error">Disabled</span>
<?php endif; ?>
            </td>
        </tr>
    </tbody>
</table>

<form method="post">
<?php if ($hasSecondFactor) : ?>
    <button class="error" type="submit" name="action" value="removeSecondFactor">Remove 2FA</button>
<?php else: ?>
    <button class="success" type="submit" name="action" value="addSecondFactor">Enroll for 2FA</button>
<?php endif; ?>
<?php if ($localUserInfo['is_disabled']) : ?>
    <button class="success" type="submit" name="action" value="enableUser">Enable User</button>
<?php else: ?>
    <button class="warning" type="submit" name="action" value="disableUser">Disable User</button>
<?php endif; ?>
</form>

<h2><?=$this->t('Group Membership');?></h2>
<?php if(0 === count($userGroupList)): ?>
<p>
        <em>Not a member of any group.</em>
</p>
<?php else: ?>
<table>
    <thead>
        <tr>
            <th><?=$this->t('Group'); ?></th>
            <th></th>
        </tr>
    </thead>
    <tbody>
<?php foreach ($userGroupList as $groupInfo): ?>
        <tr>
            <td>
                <a href="<?=$this->u('group', ['group_id' => $groupInfo['group_id']]);?>"><?=$this->e($groupInfo['display_name']);?></a>
            </td>
            <td></td>
        </tr>
<?php endforeach; ?>
    </tbody>
</table>
<?php endif; ?>

<h3>Add To Group</h3>
<?php if(0 === count($groupList)): ?>
<p>
        <em>No groups available.</em>
</p>
<?php else: ?>
<form method="post">
<select required name="group_id">
<?php foreach($groupList as $groupInfo): ?>
    <option value="<?=$this->e($groupInfo['group_id']);?>"><?=$this->e($groupInfo['display_name']);?></option>
<?php endforeach; ?>
</select>
<button class="success" name="action" value="addUserGroup" type="submit">Add</button>
</form>
<?php endif; ?>

<h2>Attributes</h2>
<?php if (0 === count($attributeList)): ?>
        <p>
            <em>No attributes set yet.</em>
        </p>
<?php else: ?>
<table>
    <thead>
        <tr>
            <th><?=$this->t('Attribute'); ?></th>
            <th><?=$this->t('Value(s)'); ?></th>
            <th></th>
        </tr>
    </thead>
    <tbody>
<?php foreach ($attributeList as $attributeName => $attributeValueList): ?>
<?php foreach ($attributeValueList as $attributeValue): ?>
        <tr>
            <th><?=$this->e($attributeName); ?></th>
            <td>
                <code><?=$this->e($attributeValue); ?></code>
            </td>
            <td class="end">
                <form method="post">
                    <input type="hidden" name="action" value="deleteAttribute">
                    <input type="hidden" name="attributeName" value="<?=$this->e($attributeName); ?>">
                    <input type="hidden" name="attributeValue" value="<?=$this->e($attributeValue); ?>">
                    <button class="error" type="submit">Delete</button>
                </form>
            </td>
        </tr>
<?php endforeach; ?>
<?php endforeach; ?>
    </tbody>
</table>
<?php endif; ?>

<h3>Add Attribute</h3>
<form method="post">
<label>Name<sup>*</sup><input type="text" required list="attributeNameList" name="attributeName" placeholder="Name"></label>
<datalist id="attributeNameList">
<?php foreach($attributeNameList as $attributeName): ?>
    <option value="<?=$this->e($attributeName);?>"><?=$this->e($attributeName);?></option>
<?php endforeach; ?>
</datalist>
<label>Value<sup>*</sup><input type="text" name="attributeValue" required placeholder="Value"></label>
<button class="success" type="submit" name="action" value="addAttribute">Add</button>
</form>

<details>
    <summary class="warning">Danger Zone</summary>
    <form method="post">
        <input type="hidden" name="user_id" value="<?=$this->e($userId); ?>">
        <button class="error" name="action" value="deleteUser" type="submit">Delete User</button>
    </form>
</details>
<?php $this->stop('main'); ?>
