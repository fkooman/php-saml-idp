<?php declare(strict_types=1); ?>
<?php /** @var \fkooman\Template\Tpl $this */ ?>
<?php /** @var int $errorCode */ ?>
<?php /** @var string $errorMessage */ ?>
<?php $this->layout('base'); ?>
<?php $this->start('main'); ?>
    <h1><?= $this->e((string) $errorCode); ?></h1>
    <p>
        <?= $this->e($errorMessage); ?>
    </p>
<?php $this->stop('main'); ?>
