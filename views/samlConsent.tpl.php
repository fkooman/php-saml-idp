<?php declare(strict_types=1); ?>
<?php /** @var \fkooman\Template\Tpl $this */ ?>
<?php /** @var array<string,array<string>> $attributeList */ ?>
<?php /** @var string $spDisplayName */ ?>
<?php /** @var string $spEntityId */ ?>
<?php /** @var string $acsUrl */ ?>
<?php /** @var string $samlResponse */ ?>
<?php /** @var ?string $relayState */ ?>
<?php $this->layout('base'); ?>
<?php $this->start('main'); ?>
<h1>Approval</h1>
<div class="dialog">
<p>You are about to login to a service. You need to approve this.</p>

<h1><?=$this->e($spDisplayName);?></h1>

<details><summary>Details...</summary>
<p>
    The following information well be sent to the service. If you do not want this, please close this browser tab.
</p>
<dl>
<?php foreach ($attributeList as $attributeName => $attributeValueList): ?>
    <dt><strong><?=$this->e($attributeName); ?></strong></dt>
    <dd>
        <ul>
<?php foreach ($attributeValueList as $attributeValue): ?>
            <li><?=$this->e($attributeValue); ?></li>
<?php endforeach; ?>
        </ul>
    </dd>
<?php endforeach; ?>
</dl>
</details>

<form method="post" action="<?=$this->e($acsUrl); ?>">
    <input type="hidden" name="SAMLResponse" value="<?=$this->e($samlResponse); ?>">
<?php if (null !== $relayState): ?>
    <input type="hidden" name="RelayState" value="<?=$this->e($relayState); ?>">
<?php endif; ?>
    <button class="success"><?=$this->t('Approve'); ?></button>
</form>
</div>
<?php $this->stop('main'); ?>
