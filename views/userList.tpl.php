<?php declare(strict_types=1); ?>
<?php /** @var \fkooman\Template\Tpl $this */ ?>
<?php /** @var array<array{user_id:string,created_at:\DateTimeImmutable,is_disabled:bool,has_otp:bool}> $userList */ ?>
<?php $this->layout('base'); ?>
<?php $this->start('main'); ?>
<h1><?=$this->t('Users');?></h1>
<?php if (0 === count($userList)): ?>
    <p>
        <em>No users registered yet.</em>
    </p>
<?php else: ?>
    <table>
        <thead>
            <tr>
                <th>User ID</th><th></th>
            </tr>
        </thead>
        <tbody>
<?php foreach($userList as $userInfo): ?>
            <tr>
                <td><a href="<?=$this->u('user', ['user_id' => $userInfo['user_id']]);?>"><?=$this->e($userInfo['user_id']);?></a>
                </td>
                <td class="end">
<?php if($userInfo['has_otp']): ?>
 <span class="badge success">2FA</span>
<?php endif; ?>
<?php if($userInfo['is_disabled']): ?>
 <span class="badge error">Disabled</span>
<?php endif; ?>
                </td>
            </tr>
<?php endforeach; ?>
        </tbody>
    </table>
<?php endif; ?>

    <h2 id="add"><?=$this->t('Add User');?></h2>

    <form method="post">
        <input type="hidden" name="action" value="addUser">
        <label>User ID<sup>*</sup> <input type="text" required name="user_id" autocomplete="off" placeholder="User ID"></label>
        <label>Name <input type="text" name="display_name" autocomplete="off" placeholder="Name"></label>
        <label>Email Address <input type="text" name="user_email" autocomplete="off" placeholder="Email Address"></label>
        <button type="submit" class="success"><?=$this->t('Add');?></button>
    </form>
<?php $this->stop('main'); ?>
