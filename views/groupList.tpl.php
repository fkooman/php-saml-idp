<?php declare(strict_types=1); ?>
<?php /** @var \fkooman\Template\Tpl $this */ ?>
<?php /** @var array<array{group_id:string,display_name:string}> $groupList */ ?>
<?php $this->layout('base'); ?>
<?php $this->start('main'); ?>
<h1><?=$this->t('Groups');?></h1>
<?php if (0 === count($groupList)): ?>
    <p>
        <em>No groups defined yet.</em>
    </p>
<?php else: ?>
    <table>
        <thead>
            <tr>
                <th>Name</th>
            </tr>
        </thead>
        <tbody>
<?php foreach($groupList as $groupInfo): ?>
            <tr>
                <td><a title="<?=$this->e($groupInfo['group_id']);?>" href="<?=$this->u('group', ['group_id' => $groupInfo['group_id']]);?>"><?=$this->e($groupInfo['display_name']);?></a>
                </td>
            </tr>
<?php endforeach; ?>
        </tbody>
    </table>
<?php endif; ?>

<h2><?=$this->t('Add Group');?></h2>
<form method="post">
    <input type="hidden" name="action" value="addGroup">
    <label>Group ID<sup>*</sup> <input type="text" required name="group_id" autocomplete="off" placeholder="Group ID"></label>
    <label>Name<sup>*</sup> <input type="text" required name="display_name" autocomplete="off" placeholder="Name"></label>
    <button type="submit" class="success"><?=$this->t('Add');?></button>
</form>
<?php $this->stop('main'); ?>
