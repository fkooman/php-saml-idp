<?php declare(strict_types=1); ?>
<?php /** @var \fkooman\Template\Tpl $this */ ?>
<?php /** @var string $userId */ ?>
<?php /** @var string $userPass */ ?>
<?php $this->layout('base'); ?>
<?php $this->start('main'); ?>
<h1><?= $this->t('User');?></h1>
<p>We generated a password for this account. Please take note of it as you will
not be able to view it again.</p>
<table>
    <thead>
        <tr>
            <th>ID</th>
            <th>Password</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><?= $this->e($userId);?></td>
            <td><?= $this->e($userPass);?></td>
        </tr>
    </tbody>
</table>
<form method="get" action="<?= $this->u('user');?>">
    <input type="hidden" name="user_id" value="<?= $this->e($userId);?>">
    <button class="success" type="submit">Continue</button>
</form>
<?php $this->stop('main'); ?>
