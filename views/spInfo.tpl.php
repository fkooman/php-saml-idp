<?php declare(strict_types=1); ?>
<?php /** @var \fkooman\Template\Tpl $this */ ?>
<?php /** @var fkooman\SAML\IdP\SAML\SpInfo $spInfo */ ?>
<?php /** @var array<string> $attributeNameList */ ?>
<?php /** @var array<array{entity_id:?string,from_attribute:?string,from_value:?string,to_attribute:string,to_value:?string}> $attributeMappingList */ ?>
<?php $this->layout('base'); ?>
<?php $this->start('main'); ?>
<h1><?=$this->t('SAML');?></h1>
    <form method="post">
        <input type="hidden" name="action" value="updateSp">
        <label>Name<sup>*</sup> <input type="text" required name="display_name" placeholder="Name" value="<?=$this->e($spInfo->displayName()); ?>"></label>
        <label>Entity ID <input type="text" disabled name="entity_id" placeholder="Entity ID" value="<?=$this->e($spInfo->entityId()); ?>"></label>
        <label>ACS URL<sup>*</sup> <input type="text" required name="acs_url" placeholder="HTTP-POST ACS URL" value="<?=$this->e($spInfo->acsUrl()); ?>"></label>
        <label>SLO URL <input type="text" name="slo_url" placeholder="HTTP-Request SLO URL" value="<?=$this->e($spInfo->sloUrl() ?? ''); ?>"></label>
        <label>Signing Key <textarea name="signing_key" placeholder="SP Signing Key (PEM)"><?=$this->e($spInfo->signingKey() ?? ''); ?></textarea></label>
        <fieldset><legend>Attribute Release</legend>
<?php foreach($attributeNameList as $attributeName): ?>
<?php if(in_array($attributeName, $spInfo->attributeList(), true)): ?>
    <label><input type="checkbox" checked name="attr_<?=$this->e($attributeName);?>"> <?=$this->e($attributeName);?></label>
<?php endif; ?>
<?php endforeach; ?>
<?php foreach($attributeNameList as $attributeName): ?>
<?php if(!in_array($attributeName, $spInfo->attributeList(), true)): ?>
    <label><input type="checkbox" name="attr_<?=$this->e($attributeName);?>"> <?=$this->e($attributeName);?></label>
<?php endif; ?>
<?php endforeach; ?>
        </fieldset>
        <button class="success" type="submit"><?=$this->t('Save');?></button>
    </form>

    <h2>Attribute Mapping</h2>
<?php if(0 === count($attributeMappingList)): ?>
    <p>
        <em>No attribute mappings yet.</em>
    </p>
<?php else: ?>
    <p>
        SP specific mapping. The list <em>also</em> includes the mappings common for a SPs.
    </p>
    <table>
        <thead>
            <tr>
                <th>From Attribute</th>
                <th>From Value</th>
                <th>To Attribute</th>
                <th>To Value</th>
            </tr>
        </thead>
        <tbody>
<?php foreach($attributeMappingList as $attributeMapping): ?>
            <tr>
                <td>
<?php if(null === $attributeMapping['from_attribute']): ?>
-
<?php else: ?>
<?= $this->e($attributeMapping['from_attribute']);?>
<?php endif; ?>
                </td>
                <td>
<?php if(null === $attributeMapping['from_value']): ?>
-
<?php else: ?>
<code><?=$this->e($attributeMapping['from_value']);?></code>
<?php endif; ?>
                </td>
                <td>
                    <?=$this->e($attributeMapping['to_attribute']);?>
                </td>
                <td>
<?php if(null === $attributeMapping['to_value']): ?>
-
<?php else: ?>
<code><?=$this->e($attributeMapping['to_value']);?></code>
<?php endif; ?>
                </td>
            </tr>
<?php endforeach; ?>
        </tbody>
    </table>
<?php endif; ?>

<details>
    <summary class="warning">Danger Zone</summary>
    <form method="post">
        <input type="hidden" name="action" value="deleteSp">
        <input type="hidden" name="entity_id" value="<?=$this->e($spInfo->entityId()); ?>">
        <button class="error" type="submit">Delete SP</button>
    </form>
</details>
<?php $this->stop('main'); ?>
