<?php declare(strict_types=1); ?>
<?php /** @var \fkooman\Template\Tpl $this */ ?>
<?php /** @var array<\fkooman\OAuth\Server\ClientInfo> $rpList */ ?>
<?php /** @var \fkooman\SAML\IdP\OIDC\FederationDb $federationDb */ ?>
<?php $this->layout('base'); ?>
<?php $this->start('main'); ?>
<h1><?=$this->t('OpenID Connect');?></h1>
<?php if (0 === count($rpList)): ?>
    <p>
        <em>No RPs registered yet.</em>
    </p>
<?php else: ?>
    <table>
        <thead>
            <tr>
                <th>Name</th>
            </tr>
        </thead>
        <tbody>
<?php foreach($rpList as $clientInfo): ?>
            <tr>
                <td><a href="<?=$this->u('rp', ['client_id' => $clientInfo->clientId()]);?>"><?=$this->e($clientInfo->displayName());?></a></td>
            </tr>
<?php endforeach; ?>
        </tbody>
    </table>
<?php endif; ?>

    <h2 id="add"><?=$this->t('Add Relying Party');?></h2>
    <form method="post">
        <input type="hidden" name="action" value="addRp">
        <label>Name<sup>*</sup> <input type="text" required name="display_name" placeholder="Name"></label>
        <label>Redirect URI(s)<sup>*</sup> <textarea rows="2" name="redirect_uris" placeholder="Redirect URIs"></textarea></label>
        <button class="success" type="submit"><?=$this->t('Add');?></button>
    </form>

    <h2><?=$this->t('Import Relying Parties');?></h2>
    <p>You can import all RPs belonging to the federation(s) listed below.</p>
    <p><em>NOTE</em>: in the future this will allow you to enable/disable a federation instead of one-shot import where it will also automatically periodically update.</p>
    <form method="post">
        <select name="metadata_url" required>
<?php foreach($federationDb->getAll() as $federationInfo): ?>
            <option value="<?=$this->e($federationInfo->metadataUrl());?>"><?=$this->e($federationInfo->displayName());?></option>
<?php endforeach; ?>
        </select>
        <button class="success" type="submit" name="action" value="importFederation"><?=$this->t('Import');?></button>
    </form>
<?php $this->stop('main'); ?>
