<?php declare(strict_types=1); ?>
<?php /** @var \fkooman\Template\Tpl $this */ ?>
<?php /** @var string $groupId */ ?>
<?php /** @var array{group_id:string,display_name:string} $localGroupInfo */ ?>
<?php /** @var array<array{user_id:string,created_at:\DateTimeImmutable,has_otp:bool}> $userList */ ?>
<?php /** @var array<array{user_id:string}> $groupUserList */ ?>
<?php $this->layout('base'); ?>
<?php $this->start('main'); ?>
<h1><?= $this->t('Group');?></h1>
    <form method="post">
        <input type="hidden" name="action" value="updateGroup">
        <label>Group ID<sup>*</sup> <input type="text" disabled required name="group_id" autocomplete="off" placeholder="Group ID" value="<?= $this->e($groupId);?>"></label>
        <label>Name<sup>*</sup> <input type="text" required name="display_name" autocomplete="off" placeholder="Name" value="<?= $this->e($localGroupInfo['display_name']);?>"></label>
        <button type="submit" class="success"><?=$this->t('Save');?></button>
    </form>

<h2>Members</h2>
<?php if (0 === count($groupUserList)): ?>
        <em>No members yet.</em>
<?php else: ?>
<table>
    <thead>
        <tr>
            <th><?=$this->t('User'); ?></th>
            <th></th>
        </tr>
    </thead>
    <tbody>
<?php foreach ($groupUserList as $userInfo): ?>
        <tr>
            <td>
                <a href="<?=$this->u('user', ['user_id' => $userInfo['user_id']]);?>"><?=$this->e($userInfo['user_id']); ?></a>
            </td>
            <td class="end">
                <form method="post">
                    <input type="hidden" name="action" value="deleteGroupUser">
                    <input type="hidden" name="user_id" value="<?=$this->e($userInfo['user_id']); ?>">
                    <button class="error" type="submit">Delete</button>
                </form>
            </td>
        </tr>
<?php endforeach; ?>
    </tbody>
</table>
<?php endif; ?>

<h3>Add Member</h3>
<form method="post">
<input type="hidden" name="action" value="addGroupUser">
<label>User ID<sup>*</sup> <input type="text" required list="userList" name="userId" placeholder="User ID"></label>
<datalist id="userList">
<?php foreach($userList as $userInfo): ?>
    <option value="<?=$this->e($userInfo['user_id']);?>"><?=$this->e($userInfo['user_id']);?></option>
<?php endforeach; ?>
</datalist>
<button type="submit" class="success">Add</button>
</form>

<details>
    <summary class="warning">Danger Zone</summary>
    <form method="post">
        <input type="hidden" name="action" value="deleteGroup">
        <input type="hidden" name="group_id" value="<?= $this->e($groupId); ?>">
        <button class="error" type="submit">Delete Group</button>
    </form>
</details>
<?php $this->stop('main'); ?>
