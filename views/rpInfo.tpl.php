<?php declare(strict_types=1); ?>
<?php /** @var \fkooman\Template\Tpl $this */ ?>
<?php /** @var \fkooman\OAuth\Server\ClientInfo $rpInfo */ ?>
<?php $this->layout('base'); ?>
<?php $this->start('main'); ?>
<h1><?=$this->t('OpenID Connect');?></h1>
    <form method="post">
        <input type="hidden" name="action" value="updateRp">
        <label>Name<sup>*</sup> <input type="text" required name="display_name" placeholder="Display Name" value="<?=$this->e($rpInfo->displayName());?>"></label>
        <label>Client ID<sup>*</sup> <input type="text" disabled name="client_id" value="<?=$this->e($rpInfo->clientId());?>"></label>
        <label>Redirect URI(s)<sup>*</sup> <textarea rows="2" name="redirect_uris" placeholder="Redirect URIs"><?=$this->e(implode("\n", $rpInfo->redirectUriList()));?></textarea></label>
        <label>Client Secret <input type="text" name="client_secret" placeholder="Client Secret" value="<?=$this->e($rpInfo->clientSecret() ?? '');?>"></label>
        <fieldset><legend>Allowed Scope(s)</legend>
        <label><input type="checkbox" name="scope_openid" disabled checked> openid</label>
<?php foreach(['profile', 'email', 'groups'] as $s): ?>
<?php if(in_array($s, $rpInfo->allowedScope()->scopeTokenList(), true)): ?>
    <label><input type="checkbox" name="scope_<?=$s;?>" checked> <?=$s;?></label>
<?php else: ?>
    <label><input type="checkbox" name="scope_<?=$s;?>"> <?=$s;?></label>
<?php endif; ?>
<?php endforeach; ?>
        </fieldset>
        <fieldset><legend>Options</legend>
        <label><input type="checkbox" name="requires_approval" <?php if($rpInfo->requiresApproval()): ?>checked<?php endif; ?>> Require User Approval/Consent</label>
        </fieldset>
        <button class="success" type="submit"><?=$this->t('Save');?></button>
    </form>

<details>
    <summary class="warning">Danger Zone</summary>
    <form method="post">
        <input type="hidden" name="action" value="deleteRp">
        <input type="hidden" name="client_id" value="<?=$this->e($rpInfo->clientId()); ?>">
        <button class="error" type="submit">Delete RP</button>
    </form>
</details>

<?php $this->stop('main'); ?>
