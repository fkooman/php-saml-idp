<?php declare(strict_types=1); ?>
<?php /** @var \fkooman\Template\Tpl $this */ ?>
<?php /** @var string $userId */ ?>
<?php /** @var string $otpSecret */ ?>
<?php /** @var string $enrollmentUri */ ?>
<?php /** @var ?string $enrollmentQr */ ?>
<?php $this->layout('base'); ?>
<?php $this->start('main'); ?>
<h1><?= $this->t('User');?></h1>
<table>
    <tbody>
        <tr><th>ID</th><td><code><?= $this->e($userId);?></code></td></tr>
        <tr><th>OTP Secret</th><td><code><?= $this->e($otpSecret);?></code></td></tr>
<?php if(null !== $enrollmentQr): ?>
        <tr><th>QR</th><td><?=$enrollmentQr; ?></td></tr>
<?php else: ?>
        <tr><th>Enrollment URI</th><td><a href="<?=$this->e($enrollmentUri);?>"><?=$this->e($enrollmentUri);?></a></td></tr>
<?php endif; ?>
    </tbody>
</table>

<form method="get" action="<?= $this->u('user');?>">
    <input type="hidden" name="user_id" value="<?= $this->e($userId);?>">
<fieldset>
    <button class="success" type="submit">Continue</button>
    </fieldset>
</form>
<?php $this->stop('main'); ?>
