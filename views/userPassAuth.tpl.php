<?php declare(strict_types=1); ?>
<?php /** @var \fkooman\Template\Tpl $this */ ?>
<?php /** @var bool $_user_pass_auth_invalid_credentials */?>
<?php /** @var string $_user_pass_auth_invalid_credentials_user */?>
<?php /** @var string $_user_pass_auth_redirect_to */?>
<?php $this->layout('base', ['pageTitle' => $this->t('Sign In')]); ?>
<?php $this->start('main'); ?>
<h1>Sign In</h1>
<div class="dialog">
<?php if ($_user_pass_auth_invalid_credentials): ?>
<p class="error">
    <?=$this->t('🛑 The credentials you provided were not correct.'); ?>
</p>
<?php endif; ?>
<form method="post" action="<?=$this->a('_user_pass_auth/verify');?>">
<?php if ($_user_pass_auth_invalid_credentials): ?>
    <input type="text" id="userName" name="userName" autocapitalize="off" placeholder="<?=$this->t('Username'); ?>" value="<?=$this->e($_user_pass_auth_invalid_credentials_user); ?>" required>
    <input type="password" id="userPass" name="userPass" placeholder="<?=$this->t('Password'); ?>" autofocus required>
<?php else: ?>
    <input type="text" name="userName" autocapitalize="off" placeholder="<?=$this->t('Username'); ?>" autofocus required>
    <input type="password" name="userPass" placeholder="<?=$this->t('Password'); ?>" required>
<?php endif; ?>
    <input type="hidden" name="_user_pass_auth_redirect_to" value="<?=$this->e($_user_pass_auth_redirect_to); ?>">
    <button class="success" type="submit"><?= $this->t('Sign In'); ?></button>
</form>
</div>
<?php $this->stop('main'); ?>
