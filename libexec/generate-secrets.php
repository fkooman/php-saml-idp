<?php

declare(strict_types=1);

/*
 * Copyright (c) 2018-2024 François Kooman <fkooman@tuxed.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

require_once dirname(__DIR__) . '/vendor/autoload.php';
$baseDir = dirname(__DIR__);

use fkooman\Jwt\RS256\SecretKey;
use fkooman\Jwt\SecretKeySet;
use fkooman\OAuth\Server\SecretBox;
use fkooman\SAML\IdP\Base64UrlSafe;
use fkooman\SAML\IdP\FileIO;

// allow group to read the created files/folders
umask(0o027);

try {
    $keyDir = $baseDir . '/config/keys';
    FileIO::mkdir($keyDir);

    // we should get rid of this and use the HMAC also for SAML
    $saltKeyFile = $keyDir . '/salt.key';
    if (!FileIO::exists($saltKeyFile)) {
        FileIO::write($saltKeyFile, Base64UrlSafe::encodeUnpadded(random_bytes(32)));
    }

    $hmacKeyFile = $keyDir . '/hmac.key';
    if (!FileIO::exists($hmacKeyFile)) {
        FileIO::write($hmacKeyFile, random_bytes(32));
    }

    // SecretBox (OAuth) key
    $oauthKeyFile = $keyDir . '/oauth.key';
    if (!FileIO::exists($oauthKeyFile)) {
        FileIO::write($oauthKeyFile, SecretBox::genKey()->exportKey());
    }

    $keySetFile = sprintf('%s/openid.jwks', $keyDir);
    if (!FileIO::exists($keySetFile)) {
        $secretKeySet = new SecretKeySet();
        $secretKeySet->add(SecretKey::generate());
        FileIO::write($keySetFile, $secretKeySet->toJwks());
    }
} catch (Exception $e) {
    echo 'ERROR: ' . $e->getMessage() . PHP_EOL;

    exit(1);
}
