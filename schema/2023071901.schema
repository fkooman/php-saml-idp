CREATE TABLE IF NOT EXISTS local_users(
    user_id VARCHAR(255) NOT NULL PRIMARY KEY,
    password_hash VARCHAR(255) NOT NULL,
    created_at VARCHAR(255) NOT NULL,
    is_disabled BOOLEAN NOT NULL,
    UNIQUE(user_id)
);
CREATE TABLE IF NOT EXISTS local_groups(
    group_id VARCHAR(255) NOT NULL PRIMARY KEY,
    display_name VARCHAR(255) NOT NULL,
    UNIQUE(group_id)
);
CREATE TABLE IF NOT EXISTS local_groups_users(
    group_id VARCHAR(255) NOT NULL REFERENCES local_groups(group_id) ON DELETE CASCADE,
    user_id VARCHAR(255) NOT NULL REFERENCES local_users(user_id) ON DELETE CASCADE,
    UNIQUE(group_id, user_id)
);
CREATE TABLE IF NOT EXISTS local_user_attributes(
    user_id VARCHAR(255) NOT NULL REFERENCES local_users(user_id) ON DELETE CASCADE,
    attribute_key VARCHAR(255) NOT NULL,
    attribute_value VARCHAR(255) NOT NULL
);
CREATE TABLE IF NOT EXISTS otp(
    user_id VARCHAR(255) NOT NULL REFERENCES local_users(user_id) ON DELETE CASCADE,
    otp_secret VARCHAR(255) NOT NULL,
    otp_hash_algorithm VARCHAR(255) NOT NULL,
    otp_digits INTEGER NOT NULL,
    totp_period INTEGER NOT NULL
);
CREATE TABLE IF NOT EXISTS otp_log(
    user_id VARCHAR(255) NOT NULL REFERENCES local_users(user_id) ON DELETE CASCADE,
    otp_key VARCHAR(255) NOT NULL,
    date_time VARCHAR(255) NOT NULL,
    UNIQUE(user_id, otp_key)
);
CREATE TABLE IF NOT EXISTS oauth_authorizations (
    auth_key VARCHAR(255) NOT NULL PRIMARY KEY,
    user_id VARCHAR(255) NOT NULL,
    client_id VARCHAR(255) NOT NULL,
    scope VARCHAR(255) NOT NULL,
    authorized_at VARCHAR(255) NOT NULL,
    expires_at VARCHAR(255) NOT NULL,
    UNIQUE(auth_key)
);
CREATE TABLE IF NOT EXISTS oauth_refresh_token_log (
    auth_key VARCHAR(255) NOT NULL REFERENCES oauth_authorizations(auth_key) ON DELETE CASCADE,
    refresh_token_id VARCHAR(255) NOT NULL,
    UNIQUE(auth_key, refresh_token_id)
);
CREATE TABLE IF NOT EXISTS oidc_rps (
    client_id VARCHAR(255) NOT NULL,
    redirect_uris TEXT NOT NULL,
    client_name TEXT NOT NULL,
    client_secret TEXT,
    requires_approval BOOLEAN NOT NULL,
    scope TEXT NOT NULL,
    UNIQUE(client_id)
);
CREATE TABLE IF NOT EXISTS saml_sps (
    entity_id VARCHAR(255) NOT NULL,
    acs_url VARCHAR(255) NOT NULL,
    slo_url VARCHAR(255),
    signing_key TEXT,
    display_name TEXT NOT NULL,
    attribute_release_list TEXT NOT NULL,
    UNIQUE(entity_id)
);
CREATE TABLE IF NOT EXISTS saml_mapping (
    entity_id VARCHAR(255),
    from_attribute TEXT,
    from_value TEXT,
    to_attribute TEXT NOT NULL,
    to_value TEXT
);
CREATE INDEX IF NOT EXISTS saml_mapping_entity_id ON saml_mapping(entity_id);
