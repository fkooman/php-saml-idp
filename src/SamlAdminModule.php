<?php

declare(strict_types=1);

/*
 * Copyright (c) 2018-2024 François Kooman <fkooman@tuxed.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace fkooman\SAML\IdP;

use fkooman\SAML\IdP\Http\Exception\HttpException;
use fkooman\SAML\IdP\Http\HtmlResponse;
use fkooman\SAML\IdP\Http\RedirectResponse;
use fkooman\SAML\IdP\Http\Request;
use fkooman\SAML\IdP\Http\Response;
use fkooman\SAML\IdP\Http\ServiceInterface;
use fkooman\SAML\IdP\Http\ServiceModuleInterface;
use fkooman\SAML\IdP\Http\UserInfo;
use fkooman\SAML\IdP\SAML\Attributes;
use fkooman\SAML\IdP\SAML\PdoSpDb;
use fkooman\SAML\IdP\SAML\SpDb;
use fkooman\SAML\IdP\SAML\SpInfo;

class SamlAdminModule implements ServiceModuleInterface
{
    private Storage $storage;
    private TplInterface $tpl;
    private PdoSpDb $spDb;

    public function __construct(Storage $storage, TplInterface $tpl, PdoSpDb $spDb)
    {
        $this->storage = $storage;
        $this->tpl = $tpl;
        $this->spDb = $spDb;
    }

    public function init(ServiceInterface $service): void
    {
        $service->get(
            '/sps',
            fn(Request $request, UserInfo $userInfo) => $this->getSps($request, $userInfo)
        );

        $service->post(
            '/sps',
            fn(Request $request, UserInfo $userInfo) => $this->postSps($request, $userInfo)
        );

        $service->get(
            '/sp',
            fn(Request $request, UserInfo $userInfo) => $this->getSp($request, $userInfo)
        );

        $service->post(
            '/sp',
            fn(Request $request, UserInfo $userInfo) => $this->postSp($request, $userInfo)
        );
    }

    private function getSp(Request $request, UserInfo $userInfo): Response
    {
        if (!$userInfo->isAdmin()) {
            throw new HttpException('user is not an admin', 403);
        }

        $entityId = $request->requireQueryParameter('entity_id', fn(string $s) => Validator::entityId($s));

        return new HtmlResponse(
            $this->tpl->render(
                'spInfo',
                [
                    'spInfo' => $this->spDb->get($entityId),
                    'attributeNameList' => Attributes::friendlyAttributeNameList(),
                    'attributeMappingList' => $this->storage->samlAttributeMappingList($entityId),
                ]
            )
        );
    }

    private function getSps(Request $request, UserInfo $userInfo): Response
    {
        if (!$userInfo->isAdmin()) {
            throw new HttpException('user is not an admin', 403);
        }

        return new HtmlResponse(
            $this->tpl->render(
                'spList',
                [
                    'spList' => $this->spDb->getAll(),
                    'spDb' => SpDb::getAll(),
                    'attributeMappingList' => $this->storage->samlCommonAttributeMappingList(),
                    'attributeNameList' => Attributes::friendlyAttributeNameList(),
                ]
            )
        );
    }

    private function postSp(Request $request, UserInfo $userInfo): Response
    {
        if (!$userInfo->isAdmin()) {
            throw new HttpException('user is not an admin', 403);
        }

        $httpAction = $request->requirePostParameter('action', fn(string $s) => Validator::httpAction($s));

        switch ($httpAction) {
            case 'updateSp':
                return $this->updateSp($request, $userInfo);
            case 'deleteSp':
                return $this->deleteSp($request, $userInfo);
            default:
                throw new HttpException('unsupported "action"', 400);
        }
    }

    private function postSps(Request $request, UserInfo $userInfo): Response
    {
        if (!$userInfo->isAdmin()) {
            throw new HttpException('user is not an admin', 403);
        }

        $httpAction = $request->requirePostParameter('action', fn(string $s) => Validator::httpAction($s));
        switch ($httpAction) {
            case 'addSp':
                return $this->addSp($request, $userInfo);
            case 'importSp':
                return $this->importSp($request, $userInfo);
            case 'addMapping':
                return $this->addMapping($request, $userInfo);
            default:
                throw new HttpException('unsupported "action"', 400);
        }
    }

    private function addMapping(Request $request, UserInfo $userInfo): Response
    {
        $fromAttribute = $request->optionalPostParameter('from_attribute', fn(string $s) => Validator::attributeName($s));
        $fromValue = $request->optionalPostParameter('from_value', fn(string $s) => Validator::attributeValue($s));
        $toAttribute = $request->requirePostParameter('to_attribute', fn(string $s) => Validator::attributeName($s));
        $toValue = $request->optionalPostParameter('to_value', fn(string $s) => Validator::attributeValue($s));

        $this->storage->samlAddMapping(null, $fromAttribute, $fromValue, $toAttribute, $toValue);

        return new RedirectResponse($request->getUri());
    }

    private function addSp(Request $request, UserInfo $userInfo): Response
    {
        $entityId = $request->requirePostParameter('entity_id', fn(string $s) => Validator::entityId($s));

        $this->spDb->add(
            new SpInfo(
                $entityId,
                $request->requirePostParameter('acs_url', fn(string $s) => Validator::acsUrl($s)),
                null,   // SLO
                null,   // Public Key
                ['pairwise-id'],
                $request->requirePostParameter('display_name', fn(string $s) => Validator::displayName($s))
            )
        );

        return new RedirectResponse($request->getRootUri() . 'sp?entity_id=' . $entityId);
    }

    private function importSp(Request $request, UserInfo $userInfo): Response
    {
        $entityId = $request->requirePostParameter('entity_id', fn(string $s) => Validator::entityId($s));

        if (null === $spInfo = SpDb::get($entityId)) {
            throw new HttpException(sprintf('SP with entityId "%s" does not exist', $entityId), 400);
        }
        $this->spDb->add($spInfo);

        return new RedirectResponse($request->getRootUri() . 'sp?entity_id=' . $entityId);
    }
    private function deleteSp(Request $request, UserInfo $userInfo): Response
    {
        $this->spDb->delete($request->requirePostParameter('entity_id', fn(string $s) => Validator::entityId($s)));

        return new RedirectResponse($request->getRootUri() . 'sps');
    }

    private function updateSp(Request $request, UserInfo $userInfo): Response
    {
        $attributeNameList = Attributes::friendlyAttributeNameList();
        $attributeList = [];
        $postData = $request->postData();
        foreach ($postData as $k => $v) {
            if (!str_starts_with($k, 'attr_')) {
                continue;
            }
            $k = substr($k, 5);
            if (!in_array($k, $attributeNameList, true)) {
                continue;
            }
            $attributeList[] = $k;
        }

        $entityId = $request->requireQueryParameter('entity_id', fn(string $s) => Validator::entityId($s));

        $this->spDb->update(
            new SpInfo(
                $entityId,
                $request->requirePostParameter('acs_url', fn(string $s) => Validator::acsUrl($s)),
                $request->optionalPostParameter('slo_url', fn(string $s) => Validator::sloUrl($s)),
                $request->optionalPostParameter('signing_key', fn(string $s) => Validator::signingKey($s)),
                $attributeList,
                $request->requirePostParameter('display_name', fn(string $s) => Validator::displayName($s))
            )
        );

        return new RedirectResponse($request->getUri());
    }
}
