<?php

declare(strict_types=1);

/*
 * Copyright (c) 2018-2024 François Kooman <fkooman@tuxed.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace fkooman\SAML\IdP;

use fkooman\Otp\Exception\OtpException;
use fkooman\Otp\Storage as OtpStorage;
use fkooman\Otp\Totp;
use fkooman\SAML\IdP\Http\Exception\HttpException;
use fkooman\SAML\IdP\Http\HookInterface;
use fkooman\SAML\IdP\Http\HtmlResponse;
use fkooman\SAML\IdP\Http\RedirectResponse;
use fkooman\SAML\IdP\Http\Request;
use fkooman\SAML\IdP\Http\Response;
use fkooman\SAML\IdP\Http\ServiceInterface;
use fkooman\SAML\IdP\Http\ServiceModuleInterface;
use fkooman\SAML\IdP\Http\SessionInterface;
use fkooman\SAML\IdP\Http\UserInfo;

class TotpModule implements ServiceModuleInterface, HookInterface
{
    private const SESSION_TWO_FACTOR_VERIFIED = '__two_factor_verified';
    private const SESSION_TWO_FACTOR_REQUIRED = '__two_factor_required';
    private const SESSION_TWO_FACTOR_SKIP = '__two_factor_skip';

    private Totp $totp;
    private SessionInterface $session;
    private TplInterface $tpl;

    public function __construct(Storage $storage, SessionInterface $session, TplInterface $tpl)
    {
        $this->totp = new Totp(new OtpStorage($storage->dbPdo()));
        $this->session = $session;
        $this->tpl = $tpl;
    }

    public function init(ServiceInterface $service): void
    {
        $service->post(
            '/_two_factor',
            fn(Request $request, UserInfo $userInfo) => $this->twoFactor($request, $userInfo)
        );
    }

    public function beforeAuth(Request $request): ?Response
    {
        return null;
    }

    public function requireTwoFactor(bool $requireTwoFactor): void
    {
        $this->session->set(self::SESSION_TWO_FACTOR_REQUIRED, $requireTwoFactor ? 'yes' : 'no');
    }

    public function afterAuth(Request $request, UserInfo &$userInfo): ?Response
    {
        if ('GET' === $request->getRequestMethod() && '/_logout' === $request->getPathInfo()) {
            return null;
        }

        if ('POST' === $request->getRequestMethod() && '/_two_factor' === $request->getPathInfo()) {
            // we are actually in the process of verifyng the 2FA, so do not
            // interfere with that...
            return null;
        }

        if (!$this->isTwoFactorRequired()) {
            return null;
        }

        if ($this->isTwoFactorSkip()) {
            return null;
        }

        if ($this->isTwoFactorVerified()) {
            $userInfo->setTwoFactorVerified();

            return null;
        }

        return new HtmlResponse(
            $this->tpl->render('twoFactor')
        );
    }

    public function isEnrolled(string $userId): bool
    {
        return $this->totp->isEnrolled($userId);
    }

    public function isTwoFactorSkip(): bool
    {
        return 'yes' === $this->session->get(self::SESSION_TWO_FACTOR_SKIP);
    }

    private function isTwoFactorRequired(): bool
    {
        return 'yes' === $this->session->get(self::SESSION_TWO_FACTOR_REQUIRED);
    }

    private function isTwoFactorVerified(): bool
    {
        return 'yes' === $this->session->get(self::SESSION_TWO_FACTOR_VERIFIED);
    }

    private function twoFactor(Request $request, UserInfo $userInfo): Response
    {
        if ('skip' === $request->requirePostParameter('action', fn(string $s) => Validator::nop($s))) {
            $this->session->set(self::SESSION_TWO_FACTOR_SKIP, 'yes');

            return new RedirectResponse($request->requireReferrer());
        }

        try {
            $this->totp->verify(
                $userInfo->userId(),
                $request->requirePostParameter('otpKey', fn(string $s) => Validator::otpKey($s))
            );
            $this->session->set(self::SESSION_TWO_FACTOR_VERIFIED, 'yes');

            return new RedirectResponse($request->requireReferrer());
        } catch (OtpException $e) {
            throw new HttpException('unable to verify TOTP', 401);
        }
    }
}
