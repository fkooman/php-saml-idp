<?php

declare(strict_types=1);

/*
 * Copyright (c) 2018-2024 François Kooman <fkooman@tuxed.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace fkooman\SAML\IdP\SAML;

/**
 * A collection of "well known" SPs.
 */
class SpDb
{
    /**
     * @return array<SpInfo>
     */
    public static function getAll(): array
    {
        return [
            new SpInfo(
                'https://wayf.wayf.dk',
                'https://wayf.wayf.dk/module.php/saml/sp/saml2-acs.php/wayf.wayf.dk',
                'https://wayf.wayf.dk/module.php/saml/sp/saml2-logout.php/wayf.wayf.dk',
                <<< EOF
                    -----BEGIN CERTIFICATE-----
                    MIIC7TCCAdWgAwIBAgIBBzANBgkqhkiG9w0BAQsFADAwMQswCQYDVQQGEwJESzEN
                    MAsGA1UEChMEV0FZRjESMBAGA1UEAxMJd2F5Zi4yMDE2MB4XDTE1MDEwMTAwMDAw
                    MFoXDTI1MTIzMTAwMDAwMFowMDELMAkGA1UEBhMCREsxDTALBgNVBAoTBFdBWUYx
                    EjAQBgNVBAMTCXdheWYuMjAxNjCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoC
                    ggEBAOeCt61E+O909jreVUV1tFHQe9m3h612W38OauWftVVrwH0CJpYCFGAMUBcD
                    FkPgocA+XpB2qadF+/dnErIDbTVgxqyewB0TOWmMoqMknrkmS0x0AiRHIBtkzIWF
                    am+EwGtFGA5Hw3sjGPoDXg4cGT731uoCktsH5ELt+eFDXSBOUgxyKzZf8NTXRbLk
                    sIdPxNgZ04e5JawFo1cnYbTVYQcleMqOYY3rIDXxA8BTm4ZYCNkxLO0v7YK7+mfF
                    5T1Q5C7FXivoQI+A2mi/qGlwLt+oD81jdYki/v7ApXZi0sdcRovA9H4yFCv4tT5f
                    /Plu8YJ8aXSGpJ8gATPtkY9ul9cCAwEAAaMSMBAwDgYDVR0PAQH/BAQDAgIEMA0G
                    CSqGSIb3DQEBCwUAA4IBAQCaDrkKvC9mc8cOmjFhPd/4UgZxol7K0U7GwBY92MXz
                    dE/o4Dd+u3Dw0+O3UsK2kxExFlT3qXuG9XF987xUoGBy+Ip6A14nmKfW9a1hLS7z
                    ZoTVpxHebmms8n5voKLPBWowiMwb8jLdVaPzAx7bMnOfXrV3g0L8inPsqgYOgqku
                    9//8I7YnV/r8z0V0uLgi2n9eYDyqvktsL37tIw6RTX/l9J8KQlHy0eWMs9CXDaK1
                    gYdif1EsaHW4xLpjZsohIoovXMtQNTN+jIybXdEDScdLzwT9j9+BU9uHJRx3f3bf
                    wX9QINsDkafDOtBNAnW762LHylOBiXgV2s954JAVY3O+
                    -----END CERTIFICATE-----
                    EOF,
                [
                    'sn',
                    'givenName', // "gn" requested by metadata
                    'cn',
                    'eduPersonPrincipalName',
                    'eduPersonPrimaryAffiliation',
                    'o', // "organizationName" request by metadata
                    'eduPersonAssurance',
                ],
                'WAYF - Where Are You From'
            ),
            new SpInfo(
                'https://engine.surfconext.nl/authentication/sp/metadata',
                'https://engine.surfconext.nl/authentication/sp/consume-assertion',
                null,
                <<< EOF
                    -----BEGIN CERTIFICATE-----
                    MIIE5zCCA0+gAwIBAgIUIJeRiN7bgv0apHl2FId0MnX50AwwDQYJKoZIhvcNAQEL
                    BQAwgYIxCzAJBgNVBAYTAk5MMRAwDgYDVQQIDAdVdHJlY2h0MRAwDgYDVQQHDAdV
                    dHJlY2h0MRIwEAYDVQQKDAlTVVJGIEIuVi4xEzARBgNVBAsMClNVUkZjb25leHQx
                    JjAkBgNVBAMMHWVuZ2luZS5zdXJmY29uZXh0Lm5sIDIwMjMwNTAzMB4XDTIzMDUw
                    MzEwMDkwNFoXDTI4MDUwMjEwMDkwNFowgYIxCzAJBgNVBAYTAk5MMRAwDgYDVQQI
                    DAdVdHJlY2h0MRAwDgYDVQQHDAdVdHJlY2h0MRIwEAYDVQQKDAlTVVJGIEIuVi4x
                    EzARBgNVBAsMClNVUkZjb25leHQxJjAkBgNVBAMMHWVuZ2luZS5zdXJmY29uZXh0
                    Lm5sIDIwMjMwNTAzMIIBojANBgkqhkiG9w0BAQEFAAOCAY8AMIIBigKCAYEAmarO
                    z3JN7K2rZJxR5Oy061oRh3aZ3zmM8jbUaztSXNJwmZaN+AZjnowSkyT9MRRDLn4Z
                    X1hy1f4kQOhf8BxU9TxK1ATviGUegpT2fUa3HR3JQyi8zMSjS3aGJxwokE4uST10
                    SI1fV4aG8SsbKb0tzaH22eT6wJHgvfFs7iSOZdcI7kvXEGLPP7VIfRaiKqIxDT5D
                    cUe0QX5F1zcPPniazSCwW81Ta2xZnwrXKJiXwgl3K2SrBBN7ss/qmSRpBT1pLvBi
                    IDbM8n8fp8+lGQzX6DcGqJA7+5+UryRIY5f6ZzJs5qUNzTdLQLE82xaZsyhJVQrV
                    kuXk5ufCXOa5izspTnVMOgu56FH0b0iwvpEwq1OAtrWoj/CIOZt0VsTB4cn00sAT
                    WjV/ss33iauEqSsb8e7THIZfF2nZ5QBG3mUynsyTTU5cxouTQ+UdISvdAOhrV5a4
                    j/d5rbqplVFjCm0KNCIz/tnNIQpGDvmk9vMn9Z52tJ9gQYo/rv4pUGxKu7yTAgMB
                    AAGjUzBRMB0GA1UdDgQWBBSLXQ3uF1SkIkMTdXl9Om/jn3asZjAfBgNVHSMEGDAW
                    gBSLXQ3uF1SkIkMTdXl9Om/jn3asZjAPBgNVHRMBAf8EBTADAQH/MA0GCSqGSIb3
                    DQEBCwUAA4IBgQBX1rZrm9EakYBnr64BRTKEZ3bIju7CvdbDo2kJMJkLN94qndkw
                    ntiECJ53M5lEzR96MrYs20Puqs3/OuJPyJ6CyUM3fsYR8fgXqapgVdyf8nLCAKM+
                    HjhbLZzbkrdLeo+szeBbYV0XDotz4j/oLPnnJDNSDx9pW61woXdqJI8EYu61+IZ+
                    VV6VBChfxZmtVeFU8aaK2tQn498Dy5Jf4zibtmuBmx5DMNQLG4onOG6wp4WEvy6X
                    EEyXz+ve6KtJP9tBbxbIsJX0mfRP8w1pX8ttWUGGfCSb+YlX2NgdIayVxg8UjW1B
                    rjQnDX6+f8ENn3980J74LTRF2QdDtM5DtPHPzOfsUsTl7A1DMs9+86z9Y69XyFWM
                    ExZoouSo2a8krTW1fqXhmN1nuzndrcrkHd6sVZy7ikcrpXYdcxhn20mNRO1erqBG
                    ue/UyHRpNCUGvTIDx6U6VW9wDrN4gXp8HFygyAqN6bpvnATbHbOqGB2nA04PZRkl
                    Bqlod0mZCq4Z1pQ=
                    -----END CERTIFICATE-----
                    EOF,
                [
                    'uid',
                    'schacHomeOrganization',
                ],
                'SURFconext'
            ),
        ];
    }

    public static function get(string $entityId): ?SpInfo
    {
        foreach (self::getAll() as $spInfo) {
            if ($entityId === $spInfo->entityId()) {
                return $spInfo;
            }
        }

        return null;
    }
}
