<?php

declare(strict_types=1);

/*
 * Copyright (c) 2018-2024 François Kooman <fkooman@tuxed.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace fkooman\SAML\IdP\SAML;

use DateTimeImmutable;
use DateTimeZone;
use fkooman\OAuth\Server\AttributeFetcherInterface;
use fkooman\SAML\IdP\Base32;
use fkooman\SAML\IdP\Base64;
use fkooman\SAML\IdP\Base64UrlSafe;
use fkooman\SAML\IdP\Cfg\Config;
use fkooman\SAML\IdP\Http\HtmlResponse;
use fkooman\SAML\IdP\Http\RedirectResponse;
use fkooman\SAML\IdP\Http\Request;
use fkooman\SAML\IdP\Http\Response;
use fkooman\SAML\IdP\Http\ServiceInterface;
use fkooman\SAML\IdP\Http\ServiceModuleInterface;
use fkooman\SAML\IdP\Http\SessionInterface;
use fkooman\SAML\IdP\Http\UserInfo;
use fkooman\SAML\IdP\LoggerInterface;
use fkooman\SAML\IdP\Storage;
use fkooman\SAML\IdP\TotpModule;
use fkooman\SAML\IdP\TplInterface;
use fkooman\SAML\IdP\Validator;
use RuntimeException;

class SamlModule implements ServiceModuleInterface
{
    protected XmlTpl $xmlTpl;
    private AttributeFetcherInterface $attributeFetcher;
    private Storage $storage;
    private LoggerInterface $logger;
    private TotpModule $totpModule;
    private Key $samlKey;
    private Certificate $samlCert;
    private Config $config;
    private PdoSpDb $spDb;
    private SessionInterface $session;
    private TplInterface $tpl;
    private string $secretSalt;
    private DateTimeImmutable $dateTime;

    // XXX fiddle with session instead of requiring TotpModule here...
    public function __construct(AttributeFetcherInterface $attributeFetcher, Storage $storage, LoggerInterface $logger, Config $config, PdoSpDb $spDb, SessionInterface $session, TplInterface $tpl, TotpModule $totpModule, Key $samlKey, Certificate $samlCert, string $secretSalt)
    {
        $this->attributeFetcher = $attributeFetcher;
        $this->storage = $storage;
        $this->logger = $logger;
        $this->config = $config;
        $this->spDb = $spDb;
        $this->session = $session;
        $this->tpl = $tpl;
        $this->totpModule = $totpModule;
        $this->samlKey = $samlKey;
        $this->samlCert = $samlCert;
        $this->secretSalt = $secretSalt;
        $this->dateTime = new DateTimeImmutable('now', new DateTimeZone('UTC'));
        $this->xmlTpl = new XmlTpl();
    }

    public function init(ServiceInterface $service): void
    {
        $service->getBeforeAuth(
            '/saml/metadata',
            fn(Request $request) => $this->getMetadata($request)
        );

        $service->get(
            '/saml/sso',
            fn(Request $request, UserInfo $userInfo) => $this->processSso($request, $userInfo)
        );

        $service->get(
            '/saml/slo',
            fn(Request $request, UserInfo $userInfo) => $this->processSlo($request, $userInfo)
        );

        $service->post(
            '/saml/sso',
            fn(Request $request, UserInfo $userInfo) => $this->processSso($request, $userInfo)
        );
    }

    private function getMetadata(Request $request): Response
    {
        $entityId = $request->getRootUri() . 'saml/metadata';
        $ssoUri = $request->getRootUri() . 'saml/sso';
        $sloUri = $request->getRootUri() . 'saml/slo';
        $keyInfo = $this->samlCert->toKeyInfo();

        $metaDataDocument = $this->xmlTpl->render(
            __DIR__ . '/tpl/metadata.tpl.php',
            [
                'entityId' => $entityId,
                'keyInfo' => $keyInfo,
                'ssoUri' => $ssoUri,
                'sloUri' => $sloUri,
                'identifierScope' => $this->config->identifierScope(),
            ]
        );

        return new Response(
            $metaDataDocument,
            ['Content-Type' => 'application/samlmetadata+xml']
        );
    }

    private function processSso(Request $request, UserInfo $userInfo): Response
    {
        //        $userAttributeList = [];
        $idpEntityId = $request->getRootUri() . 'saml/metadata';

        $samlRequest = gzinflate(Base64::decode($request->requireQueryParameter('SAMLRequest', fn(string $s) => Validator::samlRequest($s))));
        $relayState = $request->optionalQueryParameter('RelayState', fn(string $s) => Validator::relayState($s));

        $requestDocument = XmlDocument::fromProtocolMessage($samlRequest);

        $authnRequestElement = XmlDocument::requireDomElement($requestDocument->domXPath->query('/samlp:AuthnRequest')->item(0));

        //        var_dump($authnRequestElement);

        // XXX validate it actually is an AuthnRequest!
        //        $authnRequest = $dom->getElementsByTagNameNS('urn:oasis:names:tc:SAML:2.0:protocol', 'AuthnRequest')->item(0);
        //        $authnRequestId = $authnRequest->getAttribute('ID');

        $authnRequestId = $authnRequestElement->getAttribute('ID');
        // XXX make sure it is a string

        //        $foo = $requestDocument->domXPath->query('/samlp:AuthnRequest/saml:Issuer');
        //        var_dump($foo->item(0));

        $issuerElement = XmlDocument::requireDomElement($requestDocument->domXPath->query('/samlp:AuthnRequest/saml:Issuer')->item(0));

        //        var_dump($issuerElement);

        $spEntityId = $issuerElement->textContent;

        //        $spEntityId = $dom->getElementsByTagNameNS('urn:oasis:names:tc:SAML:2.0:assertion', 'Issuer')->item(0)->nodeValue;
        // XXX make sure we are the audience
        // XXX make sure the SP is registered
        if (null === $spInfo = $this->spDb->get($spEntityId)) {
            throw new RuntimeException(sprintf('no such SP "%s"', $spEntityId));
        }

        //        $spConfig = $this->metadataConfig->get($spEntityId);

        // do we have a signing key for this SP?
        // maybe it is good enough to enforce signature checking iff we have a
        // public key for the SP...
        if (null !== $signingKey = $spInfo->signingKey()) {
            $sigAlg = $request->requireQueryParameter('SigAlg', fn(string $s) => Validator::sigAlg($s));
            $signature = Base64::decode($request->requireQueryParameter('Signature', fn(string $s) => Validator::signature($s)));

            // XXX we have to get the raw parameters, just like in php-saml-sp
            $httpQuery = http_build_query(
                [
                    'SAMLRequest' => $request->requireQueryParameter('SAMLRequest', fn(string $s) => Validator::samlRequest($s)),
                    'RelayState' => $request->requireQueryParameter('RelayState', fn(string $s) => Validator::relayState($s)),
                    'SigAlg' => 'http://www.w3.org/2001/04/xmldsig-more#rsa-sha256',
                ]
            );
            $rsaKey = new Key($signingKey);
            if (1 !== openssl_verify($httpQuery, $signature, $rsaKey->getPublicKey(), OPENSSL_ALGO_SHA256)) {
                throw new RuntimeException('signature invalid');
            }
        }

        $authnRequestAcsUrl = $spInfo->acsUrl();

        $samlResponse = new SAMLResponse(
            $this->storage->samlAttributeMappingList($spInfo->entityId()),
            $this->samlKey,
            $this->samlCert
        );

        //        // add common attributes
        //        if ($spConfig->has('staticAttributeList')) {
        //            $staticAttributeList = $spConfig->get('staticAttributeList')->toArray();
        //            foreach ($staticAttributeList as $k => $v) {
        //                $samlResponse->setAttribute($k, $v);
        //            }
        //        }

        // determine which AuthnContextClassRef we require
        //    <samlp:RequestedAuthnContext Comparison="exact">
        //        <saml:AuthnContextClassRef>urn:oasis:names:tc:SAML:2.0:ac:classes:TimesyncToken</saml:AuthnContextClassRef>
        //    </samlp:RequestedAuthnContext>

        $requireTwoFactor = false;
        $domNodeList = $requestDocument->domXPath->query('/samlp:AuthnRequest/samlp:RequestedAuthnContext/saml:AuthnContextClassRef');
        if (1 === $domNodeList->length) {
            $authnContextClassRefElement = XmlDocument::requireDomElement($domNodeList->item(0));
            //            die($authnContextClassRefElement->textContent);
            if ('urn:oasis:names:tc:SAML:2.0:ac:classes:TimeSyncToken' === $authnContextClassRefElement->textContent) {
                $requireTwoFactor = true;
            }
        }

        // if 2FA is requested, but we don't have it, require it
        if ($requireTwoFactor && !$userInfo->getTwoFactorVerified()) {
            $this->totpModule->requireTwoFactor(true);

            return new RedirectResponse($request->getUri());
        }

        if (null === $attributeNameValueList = $this->attributeFetcher->attributesForUser($userInfo->userId())) {
            throw new RuntimeException('no result from auth backend for this user, user deleted?');
        }
        foreach ($attributeNameValueList as $k => $v) {
            $samlResponse->setAttribute($k, $v);
        }

        $idHash = hash(
            'sha256',
            sprintf('%s|%s|%s|%s', $this->secretSalt, $userInfo->userId(), $idpEntityId, $spEntityId),
            true
        );
        $persistentId = Base64UrlSafe::encodeUnpadded($idHash);
        $pairwiseId = Base32::encodeUpperUnpadded($idHash);
        $eduPersonTargetedId = sprintf('%s!%s!%s', $idpEntityId, $spEntityId, $persistentId);

        $this->logger->info(
            sprintf('%s: [%s@%s,%s]', $userInfo->userId(), $pairwiseId, $this->config->identifierScope(), $eduPersonTargetedId)
        );

        $samlResponse->setAttribute('eduPersonTargetedID', [$persistentId]);
        $samlResponse->setAttribute(
            'pairwise-id',
            [
                sprintf('%s@%s', $pairwiseId, $this->config->identifierScope()),
            ]
        );
        $samlResponse->setAttribute(
            'subject-id',
            [
                sprintf('%s@%s', Base32::encodeUpperUnpadded(random_bytes(32)), $this->config->identifierScope()),
            ]
        );

        $transientNameId = Base64UrlSafe::encodeUnpadded(random_bytes(32));
        $this->session->set('__tid_' . $spEntityId, $transientNameId);

        $displayName = $spEntityId;

        $authnContextClassRef = $userInfo->getTwoFactorVerified() ? 'urn:oasis:names:tc:SAML:2.0:ac:classes:TimeSyncToken' : 'urn:oasis:names:tc:SAML:2.0:ac:classes:PasswordProtectedTransport';
        $responseXml = $samlResponse->getAssertion($spInfo, $spEntityId, $idpEntityId, $authnRequestId, $transientNameId, $authnContextClassRef);

        return new HtmlResponse(
            $this->tpl->render(
                'samlConsent',
                [
                    'spEntityId' => $spEntityId,
                    'relayState' => $relayState,
                    'acsUrl' => $authnRequestAcsUrl,
                    'spDisplayName' => $spInfo->displayName(),
                    'samlResponse' => Base64::encode($responseXml),
                    // XXX somehow improve this so it does not have to come from the object
                    // XXX if there are no attributes, don't show the table of attributes...
                    'attributeList' => $samlResponse->getAttributeList($spInfo),
                ]
            )
        );
    }

    private function processSlo(Request $request, UserInfo $userInfo): Response
    {
        // XXX we do NOT verify the signature here, we MUST according to saml2int spec

        // XXX input validation of everything
        $samlRequest = gzinflate(Base64::decode($request->requireQueryParameter('SAMLRequest', fn(string $s) => Validator::samlRequest($s))));

        $requestDocument = XmlDocument::fromProtocolMessage($samlRequest);

        $logoutRequestElement = XmlDocument::requireDomElement($requestDocument->domXPath->query('/samlp:LogoutRequest')->item(0));

        // XXX validate it actually is an LogoutRequest!
        //        $logoutRequest = $dom->getElementsByTagNameNS('urn:oasis:names:tc:SAML:2.0:protocol', 'LogoutRequest')->item(0);
        $logoutRequestId = $logoutRequestElement->getAttribute('ID');

        // XXX do we need to validate the signature?! mod_auth_mellon adds a signature it seems... check saml2int

        $issuerElement = XmlDocument::requireDomElement($requestDocument->domXPath->query('/samlp:LogoutRequest/saml:Issuer')->item(0));
        $spEntityId = $issuerElement->textContent;

        // 2. verify if we know the issuer <saml:Issuer>, i.e. is an existing entityID in the metadata
        //        $spEntityId = $dom->getElementsByTagNameNS('urn:oasis:names:tc:SAML:2.0:assertion', 'Issuer')->item(0)->nodeValue;
        if (null === $spInfo = $this->spDb->get($spEntityId)) {
            throw new RuntimeException('SP not registered here');
        }

        // do we have a signing key for this SP?
        // maybe it is good enough to enforce signature checking iff we have a
        // public key for the SP...
        if (null !== $signingKey = $spInfo->signingKey()) {
            $sigAlg = $request->requireQueryParameter('SigAlg', fn(string $s) => Validator::sigAlg($s));
            $signature = Base64::decode($request->requireQueryParameter('Signature', fn(string $s) => Validator::signature($s)));

            $httpQuery = http_build_query(
                [
                    'SAMLRequest' => $request->requireQueryParameter('SAMLRequest', fn(string $s) => Validator::samlRequest($s)),
                    'RelayState' => $request->requireQueryParameter('RelayState', fn(string $s) => Validator::relayState($s)),
                    'SigAlg' => 'http://www.w3.org/2001/04/xmldsig-more#rsa-sha256',
                ]
            );
            $rsaKey = new Key($signingKey);
            if (1 !== openssl_verify($httpQuery, $signature, $rsaKey->getPublicKey(), OPENSSL_ALGO_SHA256)) {
                throw new RuntimeException('signature invalid');
            }
        }

        // 1. verify "Destination"
        $ourSlo = $request->getRootUri() . 'saml/slo';
        $ourEntityId = $request->getRootUri() . 'saml/metadata';

        $destSlo = $logoutRequestElement->getAttribute('Destination');

        if (false === hash_equals($ourSlo, $destSlo)) {
            throw new RuntimeException('specified destination is not our destination');
        }

        // 3. see if we the transient ID provided is also set in the user's session for
        //    this SP
        if (null === $transientNameId = $this->session->get('__tid_' . $spEntityId)) {
            throw new RuntimeException('no session for this SP');
        }
        //        $logoutRequestTransientNameId = $dom->getElementsByTagNameNS('urn:oasis:names:tc:SAML:2.0:assertion', 'NameID')->item(0)->nodeValue;

        $nameIdElement = XmlDocument::requireDomElement($requestDocument->domXPath->query('/samlp:LogoutRequest/saml:NameID')->item(0));
        $nameIdValue = $nameIdElement->textContent;

        // XXX make sure the attributes are correct, i.e. SPNameQualifier, Format
        if (false === hash_equals($transientNameId, $nameIdValue)) {
            throw new RuntimeException('provided transient NameID does not match expected value');
        }

        // 4. XXX do something with sessionindex?!

        // 5. kill the user's session (?)
        $this->session->destroy();

        // XXX all seems to be fine at this point, log the user out, and send a
        // LogoutResponse

        if (null === $sloUrl = $spInfo->sloUrl()) {
            throw new RuntimeException('no SLO URL registered for this SP');
        }

        $responseXml = $this->xmlTpl->render(
            __DIR__ . '/tpl/logoutResponse.tpl.php',
            [
                'id' => '_' . bin2hex(random_bytes(32)),
                'issueInstant' => $this->dateTime->format('Y-m-d\TH:i:s\Z'),
                'destination' => $sloUrl,
                'inResponseTo' => $logoutRequestId,
                'issuer' => $ourEntityId,
            ]
        );

        $httpQuery = http_build_query(
            [
                'SAMLResponse' => Base64::encode(gzdeflate($responseXml)),
                'RelayState' => $request->requireQueryParameter('RelayState', fn(string $s) => Validator::relayState($s)),
                'SigAlg' => 'http://www.w3.org/2001/04/xmldsig-more#rsa-sha256',
            ]
        );

        // calculate the signature over httpQuery
        // add it to the query string
        openssl_sign(
            $httpQuery,
            $signedInfoSignature,
            $this->samlKey->getPrivateKey(),
            OPENSSL_ALGO_SHA256
        );

        $httpQuery .= '&' . http_build_query(
            [
                'Signature' => Base64::encode($signedInfoSignature),
            ]
        );

        // XXX make sure it does not already have a "?" in the SLO URL!
        $sloUrl = $sloUrl . '?' . $httpQuery;

        return new RedirectResponse($sloUrl);
    }
}
