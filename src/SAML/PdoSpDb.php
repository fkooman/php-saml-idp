<?php

declare(strict_types=1);

/*
 * Copyright (c) 2018-2024 François Kooman <fkooman@tuxed.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace fkooman\SAML\IdP\SAML;

use PDO;

class PdoSpDb
{
    private PDO $db;
    private string $tablePrefix;

    public function __construct(PDO $db, string $tablePrefix = '')
    {
        $this->db = $db;
        $this->tablePrefix = $tablePrefix;
    }

    public function add(SpInfo $spInfo): void
    {
        $stmt = $this->db->prepare(
            <<< SQL
                INSERT
                INTO
                    {$this->tablePrefix}saml_sps (
                        entity_id,
                        acs_url,
                        slo_url,
                        signing_key,
                        attribute_release_list,
                        display_name
                    )
                    VALUES (
                        :entity_id,
                        :acs_url,
                        :slo_url,
                        :signing_key,
                        :attribute_release_list,
                        :display_name
                    )
                SQL
        );

        $stmt->bindValue(':entity_id', $spInfo->entityId(), PDO::PARAM_STR);
        $stmt->bindValue(':acs_url', $spInfo->acsUrl(), PDO::PARAM_STR);
        $stmt->bindValue(':slo_url', $spInfo->sloUrl(), PDO::PARAM_STR | PDO::PARAM_NULL);
        $stmt->bindValue(':signing_key', $spInfo->signingKey(), PDO::PARAM_STR | PDO::PARAM_NULL);
        $stmt->bindValue(':attribute_release_list', implode(' ', $spInfo->attributeList()), PDO::PARAM_STR);
        $stmt->bindValue(':display_name', $spInfo->displayName(), PDO::PARAM_STR);
        $stmt->execute();
    }

    public function get(string $entityId): ?SpInfo
    {
        $stmt = $this->db->prepare(
            <<< SQL
                SELECT
                    acs_url,
                    slo_url,
                    signing_key,
                    attribute_release_list,
                    display_name
                FROM
                    {$this->tablePrefix}saml_sps
                WHERE
                    entity_id = :entity_id
                SQL
        );

        $stmt->bindValue(':entity_id', $entityId, PDO::PARAM_STR);
        $stmt->execute();

        if (false === $resultRow = $stmt->fetch(PDO::FETCH_ASSOC)) {
            return null;
        }

        if (null !== $sloUrl = $resultRow['slo_url']) {
            $sloUrl = (string) $resultRow['slo_url'];
        }

        if (null !== $signingKey = $resultRow['signing_key']) {
            $signingKey = (string) $resultRow['signing_key'];
        }

        return new SpInfo(
            $entityId,
            (string) $resultRow['acs_url'],
            $sloUrl,
            $signingKey,
            explode(' ', (string) $resultRow['attribute_release_list']),
            (string) $resultRow['display_name']
        );
    }

    /**
     * @return array<SpInfo>
     */
    public function getAll(): array
    {
        $stmt = $this->db->prepare(
            <<< SQL
                SELECT
                    entity_id,
                    acs_url,
                    slo_url,
                    signing_key,
                    attribute_release_list,
                    display_name
                FROM
                    {$this->tablePrefix}saml_sps
                ORDER BY
                    display_name
                SQL
        );

        $stmt->execute();

        $spInfoList = [];
        foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as $resultRow) {
            if (null !== $sloUrl = $resultRow['slo_url']) {
                $sloUrl = (string) $resultRow['slo_url'];
            }

            if (null !== $signingKey = $resultRow['signing_key']) {
                $signingKey = (string) $resultRow['signing_key'];
            }

            $spInfoList[] = new SpInfo(
                (string) $resultRow['entity_id'],
                (string) $resultRow['acs_url'],
                $sloUrl,
                $signingKey,
                explode(' ', (string) $resultRow['attribute_release_list']),
                (string) $resultRow['display_name']
            );
        }

        return $spInfoList;
    }

    public function delete(string $entityId): void
    {
        $stmt = $this->db->prepare(
            <<< SQL
                DELETE
                FROM
                    {$this->tablePrefix}saml_sps
                WHERE
                    entity_id = :entity_id
                SQL
        );
        $stmt->bindValue(':entity_id', $entityId, PDO::PARAM_STR);
        $stmt->execute();
    }

    public function update(SpInfo $spInfo): void
    {
        $stmt = $this->db->prepare(
            <<< SQL
                UPDATE
                    {$this->tablePrefix}saml_sps
                SET
                    acs_url = :acs_url,
                    slo_url = :slo_url,
                    signing_key = :signing_key,
                    attribute_release_list = :attribute_release_list,
                    display_name = :display_name
                WHERE
                    entity_id = :entity_id
                SQL
        );

        $stmt->bindValue(':entity_id', $spInfo->entityId(), PDO::PARAM_STR);
        $stmt->bindValue(':acs_url', $spInfo->acsUrl(), PDO::PARAM_STR);
        $stmt->bindValue(':slo_url', $spInfo->sloUrl(), PDO::PARAM_STR | PDO::PARAM_NULL);
        $stmt->bindValue(':signing_key', $spInfo->signingKey(), PDO::PARAM_STR | PDO::PARAM_NULL);
        $stmt->bindValue(':attribute_release_list', implode(' ', $spInfo->attributeList()), PDO::PARAM_STR);
        $stmt->bindValue(':display_name', $spInfo->displayName(), PDO::PARAM_STR);
        $stmt->execute();
    }
}
