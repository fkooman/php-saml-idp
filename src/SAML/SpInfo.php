<?php

declare(strict_types=1);

/*
 * Copyright (c) 2018-2024 François Kooman <fkooman@tuxed.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace fkooman\SAML\IdP\SAML;

class SpInfo
{
    private string $entityId;
    private string $acsUrl;
    private ?string $sloUrl;
    private ?string $signingKey;

    /** @var array<string> $attributeList */
    private array $attributeList;

    private string $displayName;

    /**
     * @param array<string> $attributeList
     */
    public function __construct(string $entityId, string $acsUrl, ?string $sloUrl, ?string $signingKey, array $attributeList, string $displayName)
    {
        $this->entityId = $entityId;
        $this->acsUrl = $acsUrl;
        $this->sloUrl = $sloUrl;
        $this->signingKey = $signingKey;
        $this->attributeList = $attributeList;
        $this->displayName = $displayName;
    }

    public function entityId(): string
    {
        return $this->entityId;
    }

    public function acsUrl(): string
    {
        return $this->acsUrl;
    }

    public function sloUrl(): ?string
    {
        return $this->sloUrl;
    }

    public function signingKey(): ?string
    {
        return $this->signingKey;
    }

    /**
     * @return array<string>
     */
    public function attributeList(): array
    {
        return $this->attributeList;
    }

    public function displayName(): string
    {
        return $this->displayName;
    }
}
