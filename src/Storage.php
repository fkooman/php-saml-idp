<?php

declare(strict_types=1);

/*
 * Copyright (c) 2018-2024 François Kooman <fkooman@tuxed.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace fkooman\SAML\IdP;

use DateTimeImmutable;
use PDO;

class Storage
{
    public const CURRENT_SCHEMA_VERSION = '2023071901';
    private PDO $db;

    public function __construct(string $dbDsn, string $schemaDir)
    {
        $db = new PDO($dbDsn);
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $dbDriver = $db->getAttribute(PDO::ATTR_DRIVER_NAME);
        if ('sqlite' === $dbDriver) {
            $db->exec('PRAGMA foreign_keys = ON');
        }

        // in PHP < 8.1 the ATTR_STRINGIFY_FETCHES attribute was always true,
        // but changed to a default of false in 8.1. Setting this option
        // restores the pre-8.1 behavior. We may switch it to false at some
        // point, but this requires proper testing...
        // @see https://www.php.net/manual/en/migration81.incompatible.php
        $db->setAttribute(PDO::ATTR_STRINGIFY_FETCHES, true);

        // run database initialization/migration if necessary and enabled
        Migration::run(
            $db,
            $schemaDir,
            self::CURRENT_SCHEMA_VERSION,
            'sqlite' === $dbDriver,     // auto initialization
            'sqlite' === $dbDriver      // auto migration
        );
        $this->db = $db;
    }

    public function dbPdo(): PDO
    {
        return $this->db;
    }

    /**
     * @return array<array{group_id:string,display_name:string}>
     */
    public function localGroupList(): array
    {
        $stmt = $this->db->prepare(
            <<< 'SQL'
                SELECT
                    group_id,
                    display_name
                FROM
                    local_groups
                ORDER BY
                    display_name
                SQL
        );
        $stmt->execute();
        $groupList = [];
        foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as $resultRow) {
            $groupList[] = [
                'group_id' => (string) $resultRow['group_id'],
                'display_name' => (string) $resultRow['display_name'],
            ];
        }

        return $groupList;
    }

    public function localGroupUpdate(string $groupId, string $displayName): void
    {
        $stmt = $this->db->prepare(
            <<< 'SQL'
                UPDATE
                    local_groups
                SET
                    display_name = :display_name
                WHERE
                    group_id = :group_id
                SQL
        );

        $stmt->bindValue(':group_id', $groupId, PDO::PARAM_STR);
        $stmt->bindValue(':display_name', $displayName, PDO::PARAM_STR);
        $stmt->execute();
    }

    public function localGroupAdd(string $groupId, string $displayName): void
    {
        $stmt = $this->db->prepare(
            <<< 'SQL'
                INSERT
                INTO
                    local_groups (
                        group_id,
                        display_name
                    )
                    VALUES (
                        :group_id,
                        :display_name
                    )
                SQL
        );

        $stmt->bindValue(':group_id', $groupId, PDO::PARAM_STR);
        $stmt->bindValue(':display_name', $displayName, PDO::PARAM_STR);
        $stmt->execute();
    }

    /**
     * @return ?array{display_name:string}
     */
    public function localGroupGet(string $groupId): ?array
    {
        $stmt = $this->db->prepare(
            <<< 'SQL'
                SELECT
                    display_name
                FROM
                    local_groups
                WHERE
                    group_id = :group_id
                SQL
        );
        $stmt->bindValue(':group_id', $groupId, PDO::PARAM_STR);
        $stmt->execute();

        if (false === $resultRow = $stmt->fetch(PDO::FETCH_ASSOC)) {
            return null;
        }

        return [
            'display_name' => (string) $resultRow['display_name'],
        ];
    }

    public function localGroupAddUser(string $groupId, string $userId): void
    {
        $stmt = $this->db->prepare(
            <<< 'SQL'
                INSERT
                INTO
                    local_groups_users (
                        group_id,
                        user_id
                    )
                    VALUES (
                        :group_id,
                        :user_id
                    )
                SQL
        );

        $stmt->bindValue(':group_id', $groupId, PDO::PARAM_STR);
        $stmt->bindValue(':user_id', $userId, PDO::PARAM_STR);
        $stmt->execute();
    }

    /**
     * @return array<array{user_id:string}>
     */
    public function localGroupUserList(string $groupId): array
    {
        $stmt = $this->db->prepare(
            <<< 'SQL'
                SELECT
                    user_id
                FROM
                    local_groups_users
                WHERE
                    group_id = :group_id
                ORDER BY
                    user_id
                SQL
        );

        $stmt->bindValue(':group_id', $groupId, PDO::PARAM_STR);
        $stmt->execute();

        $userList = [];
        foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as $resultRow) {
            $userList[] = [
                'user_id' => (string) $resultRow['user_id'],
            ];
        }

        return $userList;
    }

    public function localGroupDeleteUser(string $groupId, string $userId): void
    {
        $stmt = $this->db->prepare(
            <<< 'SQL'
                DELETE
                FROM
                    local_groups_users
                WHERE
                    group_id = :group_id
                AND
                    user_id = :user_id
                SQL
        );

        $stmt->bindValue(':group_id', $groupId, PDO::PARAM_STR);
        $stmt->bindValue(':user_id', $userId, PDO::PARAM_STR);
        $stmt->execute();
    }

    public function localGroupDelete(string $groupId): void
    {
        $stmt = $this->db->prepare(
            <<< 'SQL'
                DELETE
                FROM
                    local_groups
                WHERE
                    group_id = :group_id
                SQL
        );

        $stmt->bindValue(':group_id', $groupId, PDO::PARAM_STR);
        $stmt->execute();
    }

    /**
     * @return array<array{user_id:string,password_hash:string,created_at:\DateTimeImmutable,is_disabled:bool,has_otp:bool}>
     */
    public function localUserList(): array
    {
        $stmt = $this->db->prepare(
            <<< 'SQL'
                SELECT
                    user_id,
                    password_hash,
                    created_at,
                    is_disabled,
                    (
                        SELECT
                            COUNT(user_id)
                        FROM
                            otp
                        WHERE
                            otp.user_id = local_users.user_id
                    ) AS otp_cnt
                FROM
                    local_users
                ORDER BY
                    user_id
                SQL
        );
        $stmt->execute();
        $userList = [];
        foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as $resultRow) {
            $userList[] = [
                'user_id' => (string) $resultRow['user_id'],
                'password_hash' => (string) $resultRow['password_hash'],
                'created_at' => new DateTimeImmutable((string) $resultRow['created_at']),
                'is_disabled' => (bool) $resultRow['is_disabled'],
                'has_otp' => (bool) $resultRow['otp_cnt'],
            ];
        }

        return $userList;
    }

    /**
     * @return ?array{password_hash:string,created_at:\DateTimeImmutable,is_disabled:bool}
     */
    public function localUserGet(string $userId): ?array
    {
        $stmt = $this->db->prepare(
            <<< 'SQL'
                SELECT
                    password_hash,
                    created_at,
                    is_disabled
                FROM
                    local_users
                WHERE
                    user_id = :user_id
                SQL
        );
        $stmt->bindValue(':user_id', $userId, PDO::PARAM_STR);
        $stmt->execute();

        if (false === $resultRow = $stmt->fetch(PDO::FETCH_ASSOC)) {
            return null;
        }

        return [
            'password_hash' => (string) $resultRow['password_hash'],
            'created_at' => new DateTimeImmutable((string) $resultRow['created_at']),
            'is_disabled' => (bool) $resultRow['is_disabled'],
        ];
    }

    /**
     * @return array<array{group_id:string,display_name:string}>
     */
    public function localUserGroupList(string $userId): array
    {
        $stmt = $this->db->prepare(
            <<< 'SQL'
                SELECT
                    gu.group_id,
                    g.display_name AS display_name
                FROM
                    local_groups_users gu,
                    local_groups g
                WHERE
                    user_id = :user_id
                AND
                    gu.group_id = g.group_id
                SQL
        );
        $stmt->bindValue(':user_id', $userId, PDO::PARAM_STR);
        $stmt->execute();

        $groupList = [];
        foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as $resultRow) {
            $groupList[] = [
                'group_id' => (string) $resultRow['group_id'],
                'display_name' => (string) $resultRow['display_name'],
            ];
        }

        return $groupList;

    }

    public function localUserSetIsDisabled(string $userId, bool $isDisabled): void
    {
        $stmt = $this->db->prepare(
            <<< 'SQL'
                UPDATE
                    local_users
                SET
                    is_disabled = :is_disabled
                WHERE
                    user_id = :user_id
                SQL
        );

        $stmt->bindValue(':user_id', $userId, PDO::PARAM_STR);
        $stmt->bindValue(':is_disabled', $isDisabled, PDO::PARAM_BOOL);
        $stmt->execute();
    }

    public function localUserDelete(string $userId): void
    {
        $stmt = $this->db->prepare(
            <<< 'SQL'
                DELETE
                FROM
                    local_users
                WHERE
                    user_id = :user_id
                SQL
        );

        $stmt->bindValue(':user_id', $userId, PDO::PARAM_STR);
        $stmt->execute();
    }

    public function localUserAdd(string $userId, DateTimeImmutable $createdAt): void
    {
        $stmt = $this->db->prepare(
            <<< 'SQL'
                INSERT
                INTO
                    local_users (
                        user_id,
                        password_hash,
                        created_at,
                        is_disabled
                    )
                    VALUES (
                        :user_id,
                        :password_hash,
                        :created_at,
                        0
                    )
                SQL
        );

        $stmt->bindValue(':user_id', $userId, PDO::PARAM_STR);
        $stmt->bindValue(':password_hash', '*', PDO::PARAM_STR);
        $stmt->bindValue(':created_at', $createdAt->format(DateTimeImmutable::ATOM), PDO::PARAM_STR);
        $stmt->execute();
    }

    public function localUserSetPassword(string $userId, string $passwordHash): void
    {
        $stmt = $this->db->prepare(
            <<< 'SQL'
                UPDATE
                    local_users
                SET
                    password_hash = :password_hash
                WHERE
                    user_id = :user_id
                SQL
        );

        $stmt->bindValue(':user_id', $userId, PDO::PARAM_STR);
        $stmt->bindValue(':password_hash', $passwordHash, PDO::PARAM_STR);
        $stmt->execute();
    }

    // XXX rename variable to attributeName
    // XXX rename type of value to TEXT instead of VARCHAR
    public function localUserAddAttribute(string $userId, string $attributeKey, string $attributeValue): void
    {
        $stmt = $this->db->prepare(
            <<< 'SQL'
                INSERT
                INTO
                    local_user_attributes (
                        user_id,
                        attribute_key,
                        attribute_value
                    )
                    VALUES (
                        :user_id,
                        :attribute_key,
                        :attribute_value
                    )
                SQL
        );

        $stmt->bindValue(':user_id', $userId, PDO::PARAM_STR);
        $stmt->bindValue(':attribute_key', $attributeKey, PDO::PARAM_STR);
        $stmt->bindValue(':attribute_value', $attributeValue, PDO::PARAM_STR);
        $stmt->execute();
    }

    public function localUserDeleteAttribute(string $userId, string $attributeKey, string $attributeValue): void
    {
        $stmt = $this->db->prepare(
            <<< 'SQL'
                DELETE
                FROM
                    local_user_attributes
                WHERE
                    user_id = :user_id
                AND
                    attribute_key = :attribute_key
                AND
                    attribute_value = :attribute_value
                SQL
        );

        $stmt->bindValue(':user_id', $userId, PDO::PARAM_STR);
        $stmt->bindValue(':attribute_key', $attributeKey, PDO::PARAM_STR);
        $stmt->bindValue(':attribute_value', $attributeValue, PDO::PARAM_STR);
        $stmt->execute();
    }

    /**
     * @return array<string,array<string>>
     */
    public function localUserAttributes(string $userId): array
    {
        $stmt = $this->db->prepare(
            <<< 'SQL'
                SELECT
                    attribute_key,
                    attribute_value
                FROM
                    local_user_attributes
                WHERE
                    user_id = :user_id
                ORDER BY
                    attribute_key
                SQL
        );

        $stmt->bindValue(':user_id', $userId, PDO::PARAM_STR);
        $stmt->execute();

        $attributeList = [];
        foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as $resultRow) {
            $attributeKey = (string) $resultRow['attribute_key'];
            if (!\array_key_exists($attributeKey, $attributeList)) {
                $attributeList[$attributeKey] = [];
            }
            $attributeList[$attributeKey][] = (string) $resultRow['attribute_value'];
        }

        return $attributeList;
    }

    /**
     * @return array<array{from_attribute:?string,from_value:?string,to_attribute:string,to_value:?string}>
     */
    public function samlCommonAttributeMappingList(): array
    {
        $stmt = $this->db->prepare(
            <<< 'SQL'
                SELECT
                    entity_id,
                    from_attribute,
                    from_value,
                    to_attribute,
                    to_value
                FROM
                    saml_mapping
                WHERE
                    entity_id IS NULL
                SQL
        );
        $stmt->execute();

        $samlMapping = [];
        foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as $resultRow) {
            $samlMapping[] = [
                'from_attribute' => null === $resultRow['from_attribute'] ? null : (string) $resultRow['from_attribute'],
                'from_value' => null === $resultRow['from_value'] ? null : (string) $resultRow['from_value'],
                'to_attribute' => (string) $resultRow['to_attribute'],
                'to_value' => null === $resultRow['to_value'] ? null : (string) $resultRow['to_value'],
            ];
        }

        return $samlMapping;
    }

    /**
     * @return array<array{from_attribute:?string,from_value:?string,to_attribute:string,to_value:?string}>
     */
    public function samlAttributeMappingList(string $entityId): array
    {
        $stmt = $this->db->prepare(
            <<< 'SQL'
                SELECT
                    from_attribute,
                    from_value,
                    to_attribute,
                    to_value
                FROM
                    saml_mapping
                WHERE
                    entity_id IS NULL
                OR
                    entity_id = :entity_id
                SQL
        );
        $stmt->bindValue(':entity_id', $entityId, PDO::PARAM_STR);
        $stmt->execute();

        $samlMapping = [];
        foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as $resultRow) {
            $samlMapping[] = [
                'from_attribute' => null === $resultRow['from_attribute'] ? null : (string) $resultRow['from_attribute'],
                'from_value' => null === $resultRow['from_value'] ? null : (string) $resultRow['from_value'],
                'to_attribute' => (string) $resultRow['to_attribute'],
                'to_value' => null === $resultRow['to_value'] ? null : (string) $resultRow['to_value'],
            ];
        }

        return $samlMapping;
    }

    public function samlAddMapping(?string $entityId, ?string $fromAttribute, ?string $fromValue, string $toAttribute, ?string $toValue): void
    {
        $stmt = $this->db->prepare(
            <<< 'SQL'
                INSERT
                INTO
                    saml_mapping (
                        entity_id,
                        from_attribute,
                        from_value,
                        to_attribute,
                        to_value
                    )
                    VALUES (
                        :entity_id,
                        :from_attribute,
                        :from_value,
                        :to_attribute,
                        :to_value
                    )
                SQL
        );

        $stmt->bindValue(':entity_id', $entityId, PDO::PARAM_STR | PDO::PARAM_NULL);
        $stmt->bindValue(':from_attribute', $fromAttribute, PDO::PARAM_STR | PDO::PARAM_NULL);
        $stmt->bindValue(':from_value', $fromValue, PDO::PARAM_STR | PDO::PARAM_NULL);
        $stmt->bindValue(':to_attribute', $toAttribute, PDO::PARAM_STR);
        $stmt->bindValue(':to_value', $toValue, PDO::PARAM_STR | PDO::PARAM_NULL);
        $stmt->execute();
    }
}
