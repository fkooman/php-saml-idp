<?php

declare(strict_types=1);

/*
 * Copyright (c) 2018-2024 François Kooman <fkooman@tuxed.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace fkooman\SAML\IdP;

use fkooman\SAML\IdP\Exception\LdapClientException;
use LDAP\Connection;
use RuntimeException;
use UnexpectedValueException;

class LdapClient
{
    private const CONNECTION_OPTIONS = [
        LDAP_OPT_PROTOCOL_VERSION => 3,
        LDAP_OPT_REFERRALS => 0,
        // make sure we use at least TLSv1.2, unfortunately there's no constant
        // yet for TLSv1.3 exposed in PHP
        LDAP_OPT_X_TLS_PROTOCOL_MIN => LDAP_OPT_X_TLS_PROTOCOL_TLS1_2,
    ];

    private Connection $ldapResource;

    public function __construct(string $ldapUri)
    {
        if (!extension_loaded('ldap')) {
            throw new RuntimeException('"ldap" PHP extension not available');
        }
        if (false === $ldapResource = ldap_connect($ldapUri)) {
            throw new LdapClientException(sprintf('invalid LDAP URI "%s"', $ldapUri));
        }
        foreach (self::CONNECTION_OPTIONS as $k => $v) {
            if (!ldap_set_option($ldapResource, $k, $v)) {
                throw new LdapClientException(sprintf('unable to set LDAP option "%d"', $k));
            }
        }
        $this->ldapResource = $ldapResource;
    }

    public function __destruct()
    {
        ldap_unbind($this->ldapResource);
    }

    public function bind(?string $bindUser = null, ?string $bindPass = null): void
    {
        if (!ldap_bind($this->ldapResource, $bindUser, $bindPass)) {
            throw new LdapClientException(sprintf('(%d) %s', ldap_errno($this->ldapResource), ldap_error($this->ldapResource)));
        }
    }

    public static function escapeDn(string $inputStr): string
    {
        return ldap_escape($inputStr, '', LDAP_ESCAPE_DN);
    }

    public static function escapeFilter(string $inputStr): string
    {
        return ldap_escape($inputStr, '', LDAP_ESCAPE_FILTER);
    }

    /**
     * @param array<string> $attributeList
     *
     * @return ?array{dn:string,result:array<string,array<string>>}
     */
    public function search(string $baseDn, ?string $searchFilter, array $attributeList = []): ?array
    {
        // for efficienty purposes, if the of requested attributes is empty,
        // we simply request 'dn', even though it is always part of the
        // response... if we do not request anything, ldap_search will return
        // *all* attributes/values
        if (0 === count($attributeList)) {
            $attributeList = ['dn'];
        }
        // make sure we request the same attribute not >1
        // (this should be case *in*sensitive, but well...
        $attributeList = array_values(array_unique($attributeList));

        $searchResource = ldap_search(
            $this->ldapResource,                // link_identifier
            $baseDn,                            // base_dn
            $searchFilter ?? '(objectClass=*)', // filter
            $attributeList,                     // attributes (dn is always returned...)
            0,                                  // attrsonly
            0,                                  // sizelimit
            10                                  // timelimit
        );
        if (false === $searchResource) {
            throw new LdapClientException(sprintf('(%d) %s', ldap_errno($this->ldapResource), ldap_error($this->ldapResource)));
        }

        if (is_array($searchResource)) {
            // ldap_search can return array when doing parallel search, as we
            // don't do that this should not occur, but just making sure and
            // to silence vimeo/psalm
            // @see https://www.php.net/ldap_search
            throw new UnexpectedValueException('we only expected 1 result from ldap_search');
        }

        if (0 === $resultCount = ldap_count_entries($this->ldapResource, $searchResource)) {
            // no results for this search
            return null;
        }

        if (1 !== $resultCount) {
            throw new LdapClientException(sprintf('we require exactly 1 result, got %d results', $resultCount));
        }

        if (false === $ldapEntry = ldap_first_entry($this->ldapResource, $searchResource)) {
            throw new LdapClientException(sprintf('(%d) %s', ldap_errno($this->ldapResource), ldap_error($this->ldapResource)));
        }

        if (false === $entryDn = ldap_get_dn($this->ldapResource, $ldapEntry)) {
            throw new LdapClientException('unable to determine "dn"');
        }

        $attributeNameValues = [];
        foreach ($attributeList as $attributeName) {
            if ('dn' === $attributeName) {
                continue;
            }
            if (false === $attributeValues = ldap_get_values($this->ldapResource, $ldapEntry, $attributeName)) {
                // we do not have this attribute
                continue;
            }
            unset($attributeValues['count']);
            $attributeNameValues[$attributeName] = array_values($attributeValues);
        }

        return [
            'dn' => $entryDn,
            'result' => $attributeNameValues,
        ];
    }

    /**
     * @param array<string,array<string>> $attributeNameValues
     */
    public function add(string $userDn, array $attributeNameValues): void
    {
        if (false === ldap_add($this->ldapResource, $userDn, $attributeNameValues)) {
            throw new LdapClientException(sprintf('LDAP error: (%d) %s', ldap_errno($this->ldapResource), ldap_error($this->ldapResource)));
        }
    }

    public function delete(string $userDn): void
    {
        if (false === ldap_delete($this->ldapResource, $userDn)) {
            throw new LdapClientException(sprintf('LDAP error: (%d) %s', ldap_errno($this->ldapResource), ldap_error($this->ldapResource)));
        }
    }

    public function passwd(string $userDn, string $authPass, string $newAuthPass): void
    {
        if (true !== ldap_exop_passwd($this->ldapResource, $userDn, $authPass, $newAuthPass)) {
            throw new LdapClientException(sprintf('LDAP error: (%d) %s', ldap_errno($this->ldapResource), ldap_error($this->ldapResource)));
        }
    }
}
