<?php

declare(strict_types=1);

/*
 * Copyright (c) 2018-2024 François Kooman <fkooman@tuxed.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace fkooman\SAML\IdP;

use fkooman\Jwt\Jwt;
use fkooman\OAuth\Server\ClientInfo;
use fkooman\OAuth\Server\PdoClientDb;
use fkooman\OAuth\Server\Scope;
use fkooman\SAML\IdP\Http\Exception\HttpException;
use fkooman\SAML\IdP\Http\HtmlResponse;
use fkooman\SAML\IdP\Http\RedirectResponse;
use fkooman\SAML\IdP\Http\Request;
use fkooman\SAML\IdP\Http\Response;
use fkooman\SAML\IdP\Http\ServiceInterface;
use fkooman\SAML\IdP\Http\ServiceModuleInterface;
use fkooman\SAML\IdP\Http\UserInfo;
use fkooman\SAML\IdP\OIDC\FederationDb;
use RangeException;

class OidcAdminModule implements ServiceModuleInterface
{
    private TplInterface $tpl;
    private PdoClientDb $clientDb;
    private FederationDb $federationDb;
    private HttpClientInterface $httpClient;

    public function __construct(TplInterface $tpl, PdoClientDb $clientDb)
    {
        $this->tpl = $tpl;
        $this->clientDb = $clientDb;
        $this->federationDb = new FederationDb();
        $this->httpClient = new CurlHttpClient();
    }

    public function init(ServiceInterface $service): void
    {
        $service->get(
            '/rps',
            fn(Request $request, UserInfo $userInfo) => $this->showRps($request, $userInfo)
        );

        $service->post(
            '/rps',
            fn(Request $request, UserInfo $userInfo) => $this->handleRps($request, $userInfo)
        );

        $service->get(
            '/rp',
            fn(Request $request, UserInfo $userInfo) => $this->showRpInfo($request, $userInfo)
        );

        $service->post(
            '/rp',
            fn(Request $request, UserInfo $userInfo) => $this->handleRpInfo($request, $userInfo)
        );
    }

    private function showRpInfo(Request $request, UserInfo $userInfo): Response
    {
        if (!$userInfo->isAdmin()) {
            throw new HttpException('user is not an admin', 403);
        }

        $clientId = $request->requireQueryParameter('client_id', fn(string $s) => Validator::clientId($s));

        return new HtmlResponse(
            $this->tpl->render(
                'rpInfo',
                [
                    'rpInfo' => $this->clientDb->get($clientId),
                ]
            )
        );
    }

    private function showRps(Request $request, UserInfo $userInfo): Response
    {
        if (!$userInfo->isAdmin()) {
            throw new HttpException('user is not an admin', 403);
        }

        return new HtmlResponse(
            $this->tpl->render(
                'rpList',
                [
                    'rpList' => $this->clientDb->getAll(),
                    'federationDb' => $this->federationDb,
                ]
            )
        );
    }

    private function handleRps(Request $request, UserInfo $userInfo): Response
    {
        if (!$userInfo->isAdmin()) {
            throw new HttpException('user is not an admin', 403);
        }

        $httpAction = $request->requirePostParameter('action', fn(string $s) => Validator::httpAction($s));
        switch ($httpAction) {
            case 'addRp':
                return $this->addRp($request, $userInfo);
            case 'importFederation':
                return $this->importFederation($request, $userInfo);
            default:
                throw new HttpException('unsupported "action"', 400);
        }
    }

    /**
     * Import OIDC Federation by downloading the JSON metadata and verfiying
     * the signature.
     */
    private function importFederation(Request $request, UserInfo $userInfo): Response
    {
        $metadataUrl = $request->requirePostParameter('metadata_url', fn(string $s) => Validator::nop($s));
        if (null === $federationInfo = $this->federationDb->get($metadataUrl)) {
            throw new HttpException('no such federation', 400);
        }

        $httpResponse = $this->httpClient->get($federationInfo->metadataUrl(), []);
        if (null === $mdSig = $httpResponse->getHeader('X-Metadata-Sig')) {
            throw new HttpException('no "X-Metadata-Sig" header on metadata', 500);
        }

        $jsonBody = $httpResponse->getBody();

        // restore the JWT by Base64UrlSafe encoding the HTTP response body
        // and adding it as payload
        $decodedToken = explode('.', $mdSig, 3);
        if (3 !== count($decodedToken)) {
            throw new HttpException('invalid token format', 500);
        }
        [$h,, $s] = $decodedToken;
        $mdSig = implode('.', [$h, Base64UrlSafe::encodeUnpadded($jsonBody), $s]);
        // try to decode, we do not care about the result as we already have
        // the HTTP response body
        Jwt::decode($federationInfo->publicKey(), $mdSig);

        // extract the RPs from the metadata and import them
        $mdData = Json::decode($jsonBody);
        if (0 !== $mdData['v']) {
            throw new HttpException('invalid metadata version', 500);
        }

        if (array_key_exists('metadata', $mdData)) {
            if (array_key_exists('openid_relying_parties', $mdData['metadata'])) {
                if (is_array($mdData['metadata']['openid_relying_parties'])) {
                    foreach ($mdData['metadata']['openid_relying_parties'] as $clientData) {
                        $clientInfo = ClientInfo::fromData($clientData);
                        if (null === $this->clientDb->get($clientInfo->clientId())) {
                            $this->clientDb->add($clientInfo);

                            continue;
                        }
                        $this->clientDb->update($clientInfo);
                    }
                }
            }
        }

        return new RedirectResponse($request->getUri());
    }

    private function handleRpInfo(Request $request, UserInfo $userInfo): Response
    {
        if (!$userInfo->isAdmin()) {
            throw new HttpException('user is not an admin', 403);
        }

        $httpAction = $request->requirePostParameter('action', fn(string $s) => Validator::httpAction($s));
        switch ($httpAction) {
            case 'deleteRp':
                return $this->deleteRp($request, $userInfo);
            case 'updateRp':
                return $this->updateRp($request, $userInfo);
            default:
                throw new HttpException('unsupported "action"', 400);
        }
    }

    private function addRp(Request $request, UserInfo $userInfo): Response
    {
        $clientId = sodium_bin2hex(random_bytes(16));
        $clientSecret = sodium_bin2hex(random_bytes(16));
        $this->clientDb->add(
            new ClientInfo(
                $clientId,
                self::prepareRedirectUris($request->requirePostParameter('redirect_uris', fn(string $s) => Validator::redirectUris($s))),
                $clientSecret,
                $request->requirePostParameter('display_name', fn(string $s) => Validator::displayName($s)),
                true,
                new Scope('openid')
            )
        );

        return new RedirectResponse($request->getRootUri() . 'rp?client_id=' . $clientId);
    }

    private function deleteRp(Request $request, UserInfo $userInfo): Response
    {
        $this->clientDb->delete($request->requirePostParameter('client_id', fn(string $s) => Validator::clientId($s)));

        return new RedirectResponse($request->getRootUri() . 'rps');
    }

    private function updateRp(Request $request, UserInfo $userInfo): Response
    {
        $allowedScopeList = ['openid'];
        if ((bool) $request->optionalPostParameter('scope_profile', fn(string $s) => Validator::htmlCheckbox($s))) {
            $allowedScopeList[] = 'profile';
        }
        if ((bool) $request->optionalPostParameter('scope_email', fn(string $s) => Validator::htmlCheckbox($s))) {
            $allowedScopeList[] = 'email';
        }
        if ((bool) $request->optionalPostParameter('scope_groups', fn(string $s) => Validator::htmlCheckbox($s))) {
            $allowedScopeList[] = 'groups';
        }

        $clientInfo = new ClientInfo(
            $request->requireQueryParameter('client_id', fn(string $s) => Validator::clientId($s)),
            self::prepareRedirectUris($request->requirePostParameter('redirect_uris', fn(string $s) => Validator::redirectUris($s))),
            $request->optionalPostParameter('client_secret', fn(string $s) => Validator::clientSecret($s)),
            $request->requirePostParameter('display_name', fn(string $s) => Validator::displayName($s)),
            (bool) $request->optionalPostParameter('requires_approval', fn(string $s) => Validator::htmlCheckbox($s)),
            new Scope(implode(' ', $allowedScopeList))
        );
        $this->clientDb->update($clientInfo);

        return new RedirectResponse($request->getUri());
    }

    /**
     * @return array<string>
     */
    private static function prepareRedirectUris(string $redirectUris): array
    {
        $redirectUriList = [];
        $e = explode("\n", $redirectUris);
        foreach ($e as $redirectUri) {
            $redirectUri = trim($redirectUri);
            if (0 === strlen($redirectUri)) {
                continue;
            }
            if (false === filter_var($redirectUri, FILTER_VALIDATE_URL, FILTER_FLAG_PATH_REQUIRED)) {
                throw new RangeException(sprintf('invalid URL "%s"', $redirectUri));
            }
            $redirectUriList[] = $redirectUri;
        }

        return $redirectUriList;
    }
}
