<?php

declare(strict_types=1);

/*
 * Copyright (c) 2018-2024 François Kooman <fkooman@tuxed.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace fkooman\SAML\IdP;

use fkooman\OAuth\Server\AttributeFetcherInterface;
use fkooman\SAML\IdP\Http\Auth\CredentialValidatorInterface;
use fkooman\SAML\IdP\Http\Exception\HttpException;
use fkooman\SAML\IdP\Http\HtmlResponse;
use fkooman\SAML\IdP\Http\RedirectResponse;
use fkooman\SAML\IdP\Http\Request;
use fkooman\SAML\IdP\Http\Response;
use fkooman\SAML\IdP\Http\ServiceInterface;
use fkooman\SAML\IdP\Http\ServiceModuleInterface;
use fkooman\SAML\IdP\Http\UserInfo;

class UserModule implements ServiceModuleInterface
{
    private AttributeFetcherInterface $attributeFetcher;
    private CredentialValidatorInterface $credentialValidator;
    private TplInterface $tpl;

    public function __construct(AttributeFetcherInterface $attributeFetcher, CredentialValidatorInterface $credentialValidator, TplInterface $tpl)
    {
        $this->attributeFetcher = $attributeFetcher;
        $this->credentialValidator = $credentialValidator;
        $this->tpl = $tpl;
    }

    public function init(ServiceInterface $service): void
    {
        $service->get(
            '/',
            fn(Request $request, UserInfo $userInfo) => $this->showHome($request, $userInfo)
        );

        // XXX we have to split this out in a separate module as not all
        // authentication backends have support for (re) setting the password
        // Change Password
        $service->get(
            '/passwd',
            fn(Request $request, UserInfo $userInfo) => $this->passwdPage($request, $userInfo)
        );

        $service->post(
            '/passwd',
            fn(Request $request, UserInfo $userInfo) => $this->updatePasswd($request, $userInfo)
        );
    }

    private function passwdPage(Request $request, UserInfo $userInfo): Response
    {
        return new HtmlResponse(
            $this->tpl->render(
                'updatePasswd',
                [
                    'userId' => $userInfo->userId(),
                    'userInfo' => $userInfo,
                ]
            )
        );
    }

    private function updatePasswd(Request $request, UserInfo $userInfo): Response
    {
        $userPass = $request->requirePostParameter('userPass', fn(string $s) => Validator::userAuthPass($s));
        $newUserPass = $request->requirePostParameter('newUserPass', fn(string $s) => Validator::userPass($s));
        $newUserPassConfirm = $request->requirePostParameter('newUserPassConfirm', fn(string $s) => Validator::userPass($s));

        if ($newUserPass !== $newUserPassConfirm) {
            throw new HttpException('provided new passwords do not match', 400);
        }

        $this->credentialValidator->updateAuthPass($userInfo->userId(), $userPass, $newUserPass);

        return new RedirectResponse($request->getRootUri());
    }

    private function showHome(Request $request, UserInfo $userInfo): Response
    {
        if (null === $attributeList = $this->attributeFetcher->attributesForUser($userInfo->userId())) {
            // XXX better error?
            throw new HttpException('user no longer exists', 400);
        }

        return new HtmlResponse(
            $this->tpl->render(
                'home',
                [
                    'attributeList' => $attributeList,
                    'userInfo' => $userInfo,
                ]
            )
        );
    }
}
