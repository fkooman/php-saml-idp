<?php

declare(strict_types=1);

/*
 * Copyright (c) 2018-2024 François Kooman <fkooman@tuxed.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace fkooman\SAML\IdP;

use fkooman\SAML\IdP\Http\Exception\HttpException;
use fkooman\SAML\IdP\Http\HtmlResponse;
use fkooman\SAML\IdP\Http\RedirectResponse;
use fkooman\SAML\IdP\Http\Request;
use fkooman\SAML\IdP\Http\Response;
use fkooman\SAML\IdP\Http\ServiceInterface;
use fkooman\SAML\IdP\Http\ServiceModuleInterface;
use fkooman\SAML\IdP\Http\UserInfo;

class GroupAdminModule implements ServiceModuleInterface
{
    private Storage $storage;
    private TplInterface $tpl;

    public function __construct(Storage $storage, TplInterface $tpl)
    {
        $this->storage = $storage;
        $this->tpl = $tpl;
    }

    public function init(ServiceInterface $service): void
    {
        // Users
        $service->get(
            '/groups',
            fn(Request $request, UserInfo $userInfo) => $this->showGroups($request, $userInfo)
        );

        $service->post(
            '/groups',
            fn(Request $request, UserInfo $userInfo) => $this->handleGroups($request, $userInfo)
        );

        $service->get(
            '/group',
            fn(Request $request, UserInfo $userInfo) => $this->showGroupInfo($request, $userInfo)
        );

        $service->post(
            '/group',
            fn(Request $request, UserInfo $userInfo) => $this->handleGroupInfo($request, $userInfo)
        );
    }

    private function showGroups(Request $request, UserInfo $userInfo): Response
    {
        if (!$userInfo->isAdmin()) {
            throw new HttpException('user is not an admin', 403);
        }

        return new HtmlResponse(
            $this->tpl->render(
                'groupList',
                [
                    'groupList' => $this->storage->localGroupList(),
                ]
            )
        );
    }

    private function handleGroups(Request $request, UserInfo $userInfo): Response
    {
        if (!$userInfo->isAdmin()) {
            throw new HttpException('user is not an admin', 403);
        }

        $httpAction = $request->requirePostParameter('action', fn(string $s) => Validator::httpAction($s));
        switch ($httpAction) {
            case 'addGroup':
                return $this->addGroup($request, $userInfo);
            default:
                throw new HttpException('unsupported "action"', 400);
        }
    }

    private function addGroup(Request $request, UserInfo $userInfo): Response
    {
        $groupId = $request->requirePostParameter('group_id', fn(string $s) => Validator::groupId($s));
        $displayName = $request->requirePostParameter('display_name', fn(string $s) => Validator::displayName($s));

        if (null !== $this->storage->localGroupGet($groupId)) {
            throw new HttpException('group already exists', 400);
        }

        $this->storage->localGroupAdd($groupId, $displayName);

        return new RedirectResponse($request->getRootUri() . 'group?group_id=' . $groupId);
    }

    private function showGroupInfo(Request $request, UserInfo $userInfo): Response
    {
        if (!$userInfo->isAdmin()) {
            throw new HttpException('user is not an admin', 403);
        }

        $groupId = $request->requireQueryParameter('group_id', fn(string $s) => Validator::groupId($s));
        if (null === $localGroupInfo = $this->storage->localGroupGet($groupId)) {
            throw new HttpException('group does not exist', 404);
        }

        return new HtmlResponse(
            $this->tpl->render(
                'groupInfo',
                [
                    'groupId' => $groupId,
                    'localGroupInfo' => $localGroupInfo,
                    'userList' => $this->storage->localUserList(),
                    'groupUserList' => $this->storage->localGroupUserList($groupId),
                ]
            )
        );
    }

    private function handleGroupInfo(Request $request, UserInfo $userInfo): Response
    {
        if (!$userInfo->isAdmin()) {
            throw new HttpException('user is not an admin', 403);
        }

        $httpAction = $request->requirePostParameter('action', fn(string $s) => Validator::httpAction($s));

        switch ($httpAction) {
            case 'updateGroup':
                return $this->updateGroup($request, $userInfo);
            case 'addGroupUser':
                return $this->addGroupUser($request, $userInfo);
            case 'deleteGroup':
                return $this->deleteGroup($request, $userInfo);
            case 'deleteGroupUser':
                return $this->deleteGroupUser($request, $userInfo);
            default:
                throw new HttpException('unsupported "action"', 400);
        }
    }

    private function updateGroup(Request $request, UserInfo $userInfo): Response
    {
        $groupId = $request->requireQueryParameter('group_id', fn(string $s) => Validator::groupId($s));
        $displayName = $request->requirePostParameter('display_name', fn(string $s) => Validator::displayName($s));

        if (null === $this->storage->localGroupGet($groupId)) {
            throw new HttpException('group does not exist', 400);
        }

        $this->storage->localGroupUpdate($groupId, $displayName);

        return new RedirectResponse($request->getUri());
    }

    private function deleteGroup(Request $request, UserInfo $userInfo): Response
    {
        $groupId = $request->requirePostParameter('group_id', fn(string $s) => Validator::groupId($s));

        $this->storage->localGroupDelete($groupId);

        return new RedirectResponse($request->getRootUri() . 'groups');
    }

    private function addGroupUser(Request $request, UserInfo $userInfo): Response
    {
        $groupId = $request->requireQueryParameter('group_id', fn(string $s) => Validator::groupId($s));
        $userId = $request->requirePostParameter('userId', fn(string $s) => Validator::userId($s));

        if (null === $this->storage->localGroupGet($groupId)) {
            throw new HttpException('group does not exist', 400);
        }
        if (null === $this->storage->localUserGet($userId)) {
            throw new HttpException('user does not exist', 400);
        }

        $this->storage->localGroupAddUser($groupId, $userId);

        return new RedirectResponse($request->getUri());
    }

    private function deleteGroupUser(Request $request, UserInfo $userInfo): Response
    {
        $groupId = $request->requireQueryParameter('group_id', fn(string $s) => Validator::groupId($s));
        $userId = $request->requirePostParameter('user_id', fn(string $s) => Validator::userId($s));

        $this->storage->localGroupDeleteUser($groupId, $userId);

        return new RedirectResponse($request->getUri());
    }
}
