<?php

declare(strict_types=1);

/*
 * Copyright (c) 2018-2024 François Kooman <fkooman@tuxed.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace fkooman\SAML\IdP\Cfg;

use fkooman\SAML\IdP\Cfg\Exception\ConfigException;

trait ConfigTrait
{
    private function s(string $k): self
    {
        return new self($this->requireArray($k, []));
    }

    private function optionalString(string $k): ?string
    {
        if (!\array_key_exists($k, $this->configData)) {
            return null;
        }
        if (!\is_string($this->configData[$k])) {
            throw new ConfigException('key "' . $k . '" not of type string');
        }

        return $this->configData[$k];
    }

    private function requireString(string $k, ?string $d = null): string
    {
        if (null === $v = $this->optionalString($k)) {
            if (null !== $d) {
                return $d;
            }

            throw new ConfigException('key "' . $k . '" not available');
        }

        return $v;
    }

    private function optionalInt(string $k): ?int
    {
        if (!\array_key_exists($k, $this->configData)) {
            return null;
        }
        if (!\is_int($this->configData[$k])) {
            throw new ConfigException('key "' . $k . '" not of type int');
        }

        return $this->configData[$k];
    }

    private function requireInt(string $k, ?int $d = null): int
    {
        if (null === $v = $this->optionalInt($k)) {
            if (null !== $d) {
                return $d;
            }

            throw new ConfigException('key "' . $k . '" not available');
        }

        return $v;
    }

    private function optionalBool(string $k): ?bool
    {
        if (!\array_key_exists($k, $this->configData)) {
            return null;
        }
        if (!\is_bool($this->configData[$k])) {
            throw new ConfigException('key "' . $k . '" not of type bool');
        }

        return $this->configData[$k];
    }

    private function requireBool(string $k, ?bool $d = null): bool
    {
        if (null === $v = $this->optionalBool($k)) {
            if (null !== $d) {
                return $d;
            }

            throw new ConfigException('key "' . $k . '" not available');
        }

        return $v;
    }

    private function optionalArray(string $k): ?array
    {
        if (!\array_key_exists($k, $this->configData)) {
            return null;
        }
        if (!\is_array($this->configData[$k])) {
            throw new ConfigException('key "' . $k . '" not of type array');
        }

        return $this->configData[$k];
    }

    private function requireArray(string $k, ?array $d = null): array
    {
        if (null === $v = $this->optionalArray($k)) {
            if (null !== $d) {
                return $d;
            }

            throw new ConfigException('key "' . $k . '" not available');
        }

        return $v;
    }

    /**
     * @return ?array<int>
     */
    private function optionalIntArray(string $k): ?array
    {
        if (!\array_key_exists($k, $this->configData)) {
            return null;
        }
        if (!\is_array($this->configData[$k])) {
            throw new ConfigException('key "' . $k . '" not of type array');
        }

        if ($this->configData[$k] !== array_filter($this->configData[$k], 'is_int')) {
            throw new ConfigException('key "' . $k . '" not of type array<int>');
        }

        return $this->configData[$k];
    }

    /**
     * @param ?array<int> $d
     *
     * @return array<int>
     */
    private function requireIntArray(string $k, ?array $d = null): array
    {
        if (null === $v = $this->optionalIntArray($k)) {
            if (null !== $d) {
                return $d;
            }

            throw new ConfigException('key "' . $k . '" not available');
        }

        return $v;
    }

    /**
     * @return ?array<string>
     */
    private function optionalStringArray(string $k): ?array
    {
        if (!\array_key_exists($k, $this->configData)) {
            return null;
        }
        if (!\is_array($this->configData[$k])) {
            throw new ConfigException('key "' . $k . '" not of type array');
        }

        if ($this->configData[$k] !== array_filter($this->configData[$k], 'is_string')) {
            throw new ConfigException('key "' . $k . '" not of type array<string>');
        }

        return $this->configData[$k];
    }

    /**
     * @param ?array<string> $d
     *
     * @return array<string>
     */
    private function requireStringArray(string $k, ?array $d = null): array
    {
        if (null === $v = $this->optionalStringArray($k)) {
            if (null !== $d) {
                return $d;
            }

            throw new ConfigException('key "' . $k . '" not available');
        }

        return $v;
    }

    /**
     * @param ?array<string> $d
     *
     * @return array<string>
     */
    private function requireStringOrStringArray(string $k, ?array $d = null): array
    {
        if (null === $v = $this->optionalStringOrStringArray($k)) {
            if (null !== $d) {
                return $d;
            }

            throw new ConfigException('key "' . $k . '" not available');
        }

        return $v;
    }

    /**
     * @return ?array<string>
     */
    private function optionalStringOrStringArray(string $k): ?array
    {
        if (!\array_key_exists($k, $this->configData)) {
            return null;
        }
        $this->configData[$k] = (array) $this->configData[$k];

        return $this->requireStringArray($k);
    }

    /**
     * @return ?array<string,string>
     */
    private function optionalStringStringArray(string $k): ?array
    {
        if (!\array_key_exists($k, $this->configData)) {
            return null;
        }

        if (!\is_array($this->configData[$k])) {
            throw new ConfigException('key "' . $k . '" not of type array');
        }

        $arrayKeys = array_keys($this->configData[$k]);
        $arrayValues = array_values($this->configData[$k]);

        if ($arrayKeys !== array_filter($arrayKeys, 'is_string')) {
            throw new ConfigException('key "' . $k . '" not of type array<string,string>');
        }

        if ($arrayValues !== array_filter($arrayValues, 'is_string')) {
            throw new ConfigException('key "' . $k . '" not of type array<string,string>');
        }

        return array_combine($arrayKeys, $arrayValues);
    }

    /**
     * @param ?array<string,string> $d
     *
     * @return array<string,string>
     */
    private function requireStringStringArray(string $k, ?array $d = null): array
    {
        if (null === $v = $this->optionalStringStringArray($k)) {
            if (null !== $d) {
                return $d;
            }

            throw new ConfigException('key "' . $k . '" not available');
        }

        return $v;
    }

    private function toArray(): array
    {
        return $this->configData;
    }
}
