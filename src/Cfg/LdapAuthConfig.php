<?php

declare(strict_types=1);

/*
 * Copyright (c) 2018-2024 François Kooman <fkooman@tuxed.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace fkooman\SAML\IdP\Cfg;

class LdapAuthConfig
{
    use ConfigTrait;

    private array $configData;

    public function __construct(array $configData)
    {
        $this->configData = $configData;
    }

    public function ldapUri(): string
    {
        return $this->requireString('ldapUri');
    }

    public function bindDnTemplate(): ?string
    {
        return $this->optionalString('bindDnTemplate');
    }

    public function userBaseDn(): string
    {
        return $this->requireString('userBaseDn');
    }

    public function baseDn(): ?string
    {
        return $this->optionalString('baseDn');
    }

    public function userFilterTemplate(): ?string
    {
        return $this->optionalString('userFilterTemplate');
    }

    public function userIdAttribute(): string
    {
        return $this->requireString('userIdAttribute');
    }

    public function addRealm(): ?string
    {
        return $this->optionalString('addRealm');
    }

    /**
     * @return array<string>
     */
    public function attributeNameList(): array
    {
        return array_values(
            array_unique(
                array_merge(
                    $this->requireStringArray('attributeNameList', []),
                    [$this->userIdAttribute()]
                )
            )
        );
    }

    public function searchBindDn(): ?string
    {
        return $this->optionalString('searchBindDn');
    }

    public function searchBindPass(): ?string
    {
        return $this->optionalString('searchBindPass');
    }

    public function adminBindDn(): string
    {
        return $this->requireString('adminBindDn');
    }

    public function adminBindPass(): string
    {
        return $this->requireString('adminBindPass');
    }
}
