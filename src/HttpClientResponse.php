<?php

declare(strict_types=1);

/*
 * Copyright (c) 2018-2024 François Kooman <fkooman@tuxed.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace fkooman\SAML\IdP;

class HttpClientResponse
{
    private int $responseCode;
    private string $headerList;
    private string $responseBody;

    public function __construct(int $responseCode, string $headerList, string $responseBody)
    {
        $this->responseCode = $responseCode;
        $this->headerList = $headerList;
        $this->responseBody = $responseBody;
    }

    public function getCode(): int
    {
        return $this->responseCode;
    }

    /**
     * We loop over all available headers and return the value of the first
     * matching header key. If multiple headers with the same name are present
     * the next ones are ignored!
     */
    public function getHeader(string $headerKey): ?string
    {
        foreach (\explode("\r\n", $this->headerList) as $headerLine) {
            if (!\str_contains($headerLine, ':')) {
                continue;
            }
            $keyValue = \explode(':', $headerLine, 2);
            if (2 !== count($keyValue)) {
                continue;
            }
            [$k, $v] = $keyValue;
            if (\strtolower(\trim($headerKey)) === \strtolower(\trim($k))) {
                return \trim($v);
            }
        }

        return null;
    }

    public function getHeaderList(): string
    {
        return $this->headerList;
    }

    public function getBody(): string
    {
        return $this->responseBody;
    }
}
