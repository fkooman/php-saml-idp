<?php

declare(strict_types=1);

/*
 * Copyright (c) 2018-2024 François Kooman <fkooman@tuxed.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace fkooman\SAML\IdP\OIDC;

use DateInterval;
use DateTimeImmutable;
use fkooman\Jwt\PublicKeySet;
use fkooman\OAuth\Server\BearerValidator as OidcBearerValidator;
use fkooman\OAuth\Server\Exception\OAuthException;
use fkooman\OAuth\Server\Http\Response as OAuthResponse;
use fkooman\OAuth\Server\OAuthServer as OidcOAuthServer;
use fkooman\OAuth\Server\UserInfo as OAuthUserInfo;
use fkooman\OAuth\Server\UserInfoEndpoint;
use fkooman\SAML\IdP\Http\Exception\HttpException;
use fkooman\SAML\IdP\Http\HtmlResponse;
use fkooman\SAML\IdP\Http\JsonResponse;
use fkooman\SAML\IdP\Http\RedirectResponse;
use fkooman\SAML\IdP\Http\Request;
use fkooman\SAML\IdP\Http\Response;
use fkooman\SAML\IdP\Http\ServiceInterface;
use fkooman\SAML\IdP\Http\ServiceModuleInterface;
use fkooman\SAML\IdP\Http\SessionInterface;
use fkooman\SAML\IdP\Http\UserInfo;
use fkooman\SAML\IdP\Storage;
use fkooman\SAML\IdP\TotpModule;
use fkooman\SAML\IdP\TplInterface;
use fkooman\SAML\IdP\Validator;

class OpenIdModule implements ServiceModuleInterface
{
    public const PROFILE_SFA = 'https://refeds.org/profile/sfa';
    public const PROFILE_MFA = 'https://refeds.org/profile/mfa';
    protected DatetimeImmutable $dateTime;
    private Storage $storage;
    private SessionInterface $session;
    private OidcOAuthServer $oauthServer;
    private OidcBearerValidator $bearerValidator;
    private UserInfoEndpoint $userInfoEndpoint;
    private PublicKeySet $publicKeySet;
    private TotpModule $totpModule;
    private TplInterface $tpl;

    public function __construct(Storage $storage, SessionInterface $session, OidcOAuthServer $oauthServer, OidcBearerValidator $bearerValidator, UserInfoEndpoint $userInfoEndpoint, PublicKeySet $publicKeySet, TotpModule $totpModule, TplInterface $tpl)
    {
        $this->storage = $storage;
        $this->session = $session;
        $this->oauthServer = $oauthServer;
        $this->bearerValidator = $bearerValidator;
        $this->userInfoEndpoint = $userInfoEndpoint;
        $this->publicKeySet = $publicKeySet;
        $this->totpModule = $totpModule;
        $this->tpl = $tpl;
        $this->dateTime = new DateTimeImmutable();
    }

    public function init(ServiceInterface $service): void
    {
        $service->get(
            '/openid/authorize',
            fn(Request $request, UserInfo $userInfo) => $this->getAuthorize($request, $userInfo)
        );

        $service->post(
            '/openid/authorize',
            fn(Request $request, UserInfo $userInfo) => $this->postAuthorize($request, $userInfo)
        );

        $service->postBeforeAuth(
            '/openid/token',
            fn(Request $request) => $this->postToken($request)
        );

        $service->getBeforeAuth(
            '/openid/userinfo',
            fn(Request $request) => $this->userInfo($request)
        );

        $service->getBeforeAuth(
            '/openid/jwks',
            fn(Request $request) => $this->getJwks($request)
        );
    }

    private function getAuthorize(Request $request, UserInfo $userInfo): Response
    {
        try {
            // check 2FA here...
            // XXX implement a proper validator at some point, we do a exact compare, so it is kinda safe
            if (null !== $acrValues = $request->optionalQueryParameter('acr_values', fn(string $s) => Validator::nop($s))) {
                $acrValues = explode(' ', $acrValues);
                if (in_array(self::PROFILE_MFA, $acrValues, true)) {
                    // MFA required
                    if (!$userInfo->getTwoFactorVerified() && !$this->totpModule->isTwoFactorSkip()) {
                        $this->totpModule->requireTwoFactor(true);

                        return new RedirectResponse($request->getUri());
                    }
                }
            }

            if (null !== $maxAge = $request->optionalQueryParameter('max_age', fn(string $s) => Validator::nonNegativeInt($s))) {
                // figure out whether or not to kill the logged in session and
                // trigger new login...
                // NOTE: this is only a hint, the RP is required to verify the
                // auth_time field in the ID Token anyway!
                $authExpiresAt = $userInfo->authTime()->add(new DateInterval(sprintf('PT%dS', (int) $maxAge)));
                if ($this->dateTime > $authExpiresAt) {
                    $this->session->destroy();

                    // redirect to ourselves
                    return new RedirectResponse($request->getUri());
                }
            }

            $authorizeRequest = $this->oauthServer->getAuthorize();
            $oauthUserInfo = new OAuthUserInfo(
                $userInfo->userId(),
                $userInfo->authTime()
            );

            if (false !== $authorizeResponse = $this->oauthServer->getAuthorizeResponse($oauthUserInfo)) {
                // optimization where we do not ask for approval
                return $this->prepareReturnResponse($authorizeResponse);
            }

            // ask for approving this client/scope
            return new HtmlResponse(
                $this->tpl->render(
                    'openIdConsent',
                    [
                        'authzInfo' => $this->oauthServer->getAuthorize(),
                    ]
                )
            );
        } catch (OAuthException $e) {
            throw new HttpException(sprintf('ERROR: %s (%s)', $e->getMessage(), $e->getDescription() ?? ''), $e->getStatusCode());
        }
    }

    private function postAuthorize(Request $request, UserInfo $userInfo): Response
    {
        try {

            $authorizeRequest = $this->oauthServer->getAuthorize();
            $oauthUserInfo = new OAuthUserInfo(
                $userInfo->userId(),
                $userInfo->authTime(),
                $userInfo->getTwoFactorVerified() ? self::PROFILE_MFA : self::PROFILE_SFA
            );
            $authorizeResponse = $this->oauthServer->postAuthorize($oauthUserInfo);

            return $this->prepareReturnResponse($authorizeResponse);
        } catch (OAuthException $e) {
            throw new HttpException(sprintf('ERROR: %s (%s)', $e->getMessage(), $e->getDescription() ?? ''), $e->getStatusCode());
        }
    }

    private function postToken(Request $request): Response
    {
        try {
            $postTokenResponse = $this->oauthServer->postToken();

            return new Response($postTokenResponse->getBody(), $postTokenResponse->getHeaders(), $postTokenResponse->getStatusCode());
        } catch (OAuthException $e) {
            $jsonResponse = $e->getJsonResponse();

            return new Response($jsonResponse->getBody(), $jsonResponse->getHeaders(), $jsonResponse->getStatusCode());
        }
    }

    private function userInfo(Request $request): Response
    {
        $accessToken = $this->bearerValidator->validate();
        $accessToken->scope()->requireAll(['openid']);

        return new JsonResponse(
            $this->userInfoEndpoint->userInfo($accessToken)
        );
    }

    private function getJwks(Request $request): Response
    {
        return new Response(
            $this->publicKeySet->toJwks(),
            ['Content-Type' => 'application/jwk-set+json']
        );
    }

    private function prepareReturnResponse(OAuthResponse $authorizeResponse): Response
    {
        return new Response($authorizeResponse->getBody(), $authorizeResponse->getHeaders(), $authorizeResponse->getStatusCode());
    }

    private static function getOrigin(string $redirectUri): string
    {
        if (false === $urlParts = parse_url($redirectUri)) {
            throw new HttpException('unable to parse RP "redirect_uri"', 500);
        }

        if (!array_key_exists('scheme', $urlParts) || !array_key_exists('host', $urlParts)) {
            throw new HttpException('unable to parse RP "redirect_uri"', 500);
        }

        if (array_key_exists('port', $urlParts)) {
            return sprintf('%s://%s:%s', $urlParts['scheme'], $urlParts['host'], $urlParts['port']);
        }

        return sprintf('%s://%s', $urlParts['scheme'], $urlParts['host']);
    }
}
