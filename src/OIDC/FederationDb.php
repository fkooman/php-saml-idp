<?php

declare(strict_types=1);

/*
 * Copyright (c) 2018-2024 François Kooman <fkooman@tuxed.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace fkooman\SAML\IdP\OIDC;

use fkooman\Jwt\RS256\PublicKey;

class FederationDb
{
    /** @var array<FederationInfo> */
    private array $db;

    public function __construct()
    {
        $this->db = [
            new FederationInfo(
                'https://argon.tuxed.net/fkooman/oidc-fed/metadata.json',
                PublicKey::fromPem(
                    <<< EOF
                        -----BEGIN PUBLIC KEY-----
                        MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAqeUYli14mujmr+fpgn2k
                        qaHZoodxrnZex3xMu40ZUOi8msLrlf7pXAiRPwCoXgMeGTT+pK/ktLXDHY12ZbfD
                        2Qy47yOEnxxQb9RmQcQR6cHfEAlP0g0/kXZSVDQJ8qJdLN7QJQjPS5M9RAf/jA/a
                        nA9/EvwbFrYqoKd8/I473RbtBwJswo4VPz4DwUZb9VTnituXMBudYCPFg9M2/dzq
                        AhkYv43iIDccriz2Cetr65nGhU1aH9EmJ4G9x5MK/XPXwsT3399f8dFfWAKoxKD2
                        BMxI3tRq6MIEwNa6YdjRNuYb6H08nmX2FGQHUevn/hRfogJINdyF/bXeWYTaqIrD
                        ywIDAQAB
                        -----END PUBLIC KEY-----
                        EOF
                ),
                'FrkOpenID Federation (dev)'
            ),
        ];
    }

    public function get(string $metadataUrl): ?FederationInfo
    {
        foreach ($this->db as $federationInfo) {
            if ($metadataUrl === $federationInfo->metadataUrl()) {
                return $federationInfo;
            }
        }

        return null;
    }

    /**
     * @return array<FederationInfo>
     */
    public function getAll(): array
    {
        return $this->db;
    }
}
