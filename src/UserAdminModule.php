<?php

declare(strict_types=1);

/*
 * Copyright (c) 2018-2024 François Kooman <fkooman@tuxed.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace fkooman\SAML\IdP;

use DateTimeImmutable;
use fkooman\Otp\OtpInfo;
use fkooman\Otp\OtpStorageInterface;
use fkooman\Otp\Totp;
use fkooman\SAML\IdP\Cfg\Config;
use fkooman\SAML\IdP\Exception\QrCodeException;
use fkooman\SAML\IdP\Http\Auth\CredentialValidatorInterface;
use fkooman\SAML\IdP\Http\Exception\HttpException;
use fkooman\SAML\IdP\Http\HtmlResponse;
use fkooman\SAML\IdP\Http\RedirectResponse;
use fkooman\SAML\IdP\Http\Request;
use fkooman\SAML\IdP\Http\Response;
use fkooman\SAML\IdP\Http\ServiceInterface;
use fkooman\SAML\IdP\Http\ServiceModuleInterface;
use fkooman\SAML\IdP\Http\UserInfo;
use fkooman\SAML\IdP\SAML\Attributes;

class UserAdminModule implements ServiceModuleInterface
{
    private Config $config;
    private Storage $storage;
    private CredentialValidatorInterface $credentialValidator;
    private TplInterface $tpl;
    private OtpStorageInterface $otpStorage;

    public function __construct(Config $config, Storage $storage, CredentialValidatorInterface $credentialValidator, TplInterface $tpl, OtpStorageInterface $otpStorage)
    {
        $this->config = $config;
        $this->storage = $storage;
        $this->credentialValidator = $credentialValidator;
        $this->tpl = $tpl;
        $this->otpStorage = $otpStorage;
    }

    public function init(ServiceInterface $service): void
    {
        // Users
        $service->get(
            '/users',
            fn(Request $request, UserInfo $userInfo) => $this->showUsers($request, $userInfo)
        );

        $service->post(
            '/users',
            fn(Request $request, UserInfo $userInfo) => $this->handleUsers($request, $userInfo)
        );

        $service->get(
            '/user',
            fn(Request $request, UserInfo $userInfo) => $this->showUserInfo($request, $userInfo)
        );

        $service->post(
            '/user',
            fn(Request $request, UserInfo $userInfo) => $this->handleUserInfo($request, $userInfo)
        );
    }

    private function showUserInfo(Request $request, UserInfo $userInfo): Response
    {
        if (!$userInfo->isAdmin()) {
            throw new HttpException('user is not an admin', 403);
        }

        $userId = $request->requireQueryParameter('user_id', fn(string $s) => Validator::userId($s));
        if (null === $localUserInfo = $this->storage->localUserGet($userId)) {
            throw new HttpException('user does not exist', 404);
        }

        return new HtmlResponse(
            $this->tpl->render(
                'userInfo',
                [
                    'userId' => $userId,
                    'userPass' => null,
                    'localUserInfo' => $localUserInfo,
                    'attributeList' => $this->storage->localUserAttributes($userId),
                    'attributeNameList' => Attributes::friendlyAttributeNameList(),
                    'userGroupList' => $this->storage->localUserGroupList($userId),
                    'groupList' => $this->storage->localGroupList(),
                    'hasSecondFactor' => false !== $this->otpStorage->getOtpSecret($userId),
                ]
            )
        );
    }

    private function handleUserInfo(Request $request, UserInfo $userInfo): Response
    {
        if (!$userInfo->isAdmin()) {
            throw new HttpException('user is not an admin', 403);
        }

        $httpAction = $request->requirePostParameter('action', fn(string $s) => Validator::httpAction($s));

        switch ($httpAction) {
            case 'disableUser':
                return $this->disableUser($request, $userInfo);
            case 'enableUser':
                return $this->enableUser($request, $userInfo);
            case 'addAttribute':
                return $this->addAttribute($request, $userInfo);
            case 'deleteAttribute':
                return $this->deleteAttribute($request, $userInfo);
            case 'deleteUser':
                return $this->deleteUser($request, $userInfo);
            case 'removeSecondFactor':
                return $this->removeSecondFactor($request, $userInfo);
            case 'addSecondFactor':
                return $this->addSecondFactor($request, $userInfo);
            case 'addUserGroup':
                return $this->addUserGroup($request, $userInfo);
            default:
                throw new HttpException('unsupported "action"', 400);
        }
    }

    private function addUserGroup(Request $request, UserInfo $userInfo): Response
    {
        $userId = $request->requireQueryParameter('user_id', fn(string $s) => Validator::userId($s));
        $groupId = $request->requirePostParameter('group_id', fn(string $s) => Validator::groupId($s));

        if (null === $this->storage->localUserGet($userId)) {
            throw new HttpException('user does not exist', 400);
        }
        if (null === $this->storage->localGroupGet($groupId)) {
            throw new HttpException('group does not exist', 400);
        }

        $this->storage->localGroupAddUser($groupId, $userId);

        return new RedirectResponse($request->getUri());
    }

    private function disableUser(Request $request, UserInfo $userInfo): Response
    {
        $userId = $request->requireQueryParameter('user_id', fn(string $s) => Validator::userId($s));

        $this->storage->localUserSetIsDisabled($userId, true);

        return new RedirectResponse($request->getUri());
    }

    private function enableUser(Request $request, UserInfo $userInfo): Response
    {
        $userId = $request->requireQueryParameter('user_id', fn(string $s) => Validator::userId($s));

        $this->storage->localUserSetIsDisabled($userId, false);

        return new RedirectResponse($request->getUri());
    }

    private function removeSecondFactor(Request $request, UserInfo $userInfo): Response
    {
        $userId = $request->requireQueryParameter('user_id', fn(string $s) => Validator::userId($s));

        $this->otpStorage->deleteOtpSecret($userId);

        return new RedirectResponse($request->getUri());
    }

    private function addSecondFactor(Request $request, UserInfo $userInfo): Response
    {
        $userId = $request->requireQueryParameter('user_id', fn(string $s) => Validator::userId($s));
        $otpSecret = Totp::generateSecret();

        $this->otpStorage->setOtpSecret(
            $userId,
            new OtpInfo(
                $otpSecret,
                'sha1',
                6,
                30
            )
        );
        $totp = new Totp($this->otpStorage);

        $enrollmentUri = $totp->getEnrollmentUri($userId, $otpSecret, $this->config->displayName());

        try {
            $enrollmentQr = QrCode::generate($enrollmentUri);
        } catch (QrCodeException $e) {
            $enrollmentQr = null;
        }

        return new HtmlResponse(
            $this->tpl->render(
                'userAddSecondFactor',
                [
                    'userId' => $userId,
                    'otpSecret' => $otpSecret,
                    'enrollmentUri' => $enrollmentUri,
                    'enrollmentQr' => $enrollmentQr,
                ]
            )
        );
    }

    private function addAttribute(Request $request, UserInfo $userInfo): Response
    {
        $userId = $request->requireQueryParameter('user_id', fn(string $s) => Validator::userId($s));
        $attributeName = $request->requirePostParameter('attributeName', fn(string $s) => Validator::attributeName($s));
        $attributeValue = $request->requirePostParameter('attributeValue', fn(string $s) => Validator::attributeValue($s));

        $this->storage->localUserAddAttribute($userId, $attributeName, $attributeValue);

        return new RedirectResponse($request->getUri());
    }

    private function deleteAttribute(Request $request, UserInfo $userInfo): Response
    {
        $userId = $request->requireQueryParameter('user_id', fn(string $s) => Validator::userId($s));
        $attributeName = $request->requirePostParameter('attributeName', fn(string $s) => Validator::attributeName($s));
        $attributeValue = $request->requirePostParameter('attributeValue', fn(string $s) => Validator::attributeValue($s));

        $this->storage->localUserDeleteAttribute($userId, $attributeName, $attributeValue);

        return new RedirectResponse($request->getUri());
    }

    private function showUsers(Request $request, UserInfo $userInfo): Response
    {
        if (!$userInfo->isAdmin()) {
            throw new HttpException('user is not an admin', 403);
        }

        return new HtmlResponse(
            $this->tpl->render(
                'userList',
                [
                    'userList' => $this->storage->localUserList(),
                ]
            )
        );
    }

    private function handleUsers(Request $request, UserInfo $userInfo): Response
    {
        if (!$userInfo->isAdmin()) {
            throw new HttpException('user is not an admin', 403);
        }

        $httpAction = $request->requirePostParameter('action', fn(string $s) => Validator::httpAction($s));
        switch ($httpAction) {
            case 'addUser':
                return $this->addUser($request, $userInfo);
            default:
                throw new HttpException('unsupported "action"', 400);
        }
    }

    private function deleteUser(Request $request, UserInfo $userInfo): Response
    {
        $userId = $request->requirePostParameter('user_id', fn(string $s) => Validator::userId($s));

        if ($userId === $userInfo->userId()) {
            throw new HttpException('you can not delete your own account', 400);
        }

        $this->storage->localUserDelete($userId);

        return new RedirectResponse($request->getRootUri() . 'users');
    }

    private function addUser(Request $request, UserInfo $userInfo): Response
    {
        $userId = $request->requirePostParameter('user_id', fn(string $s) => Validator::userId($s));
        $displayName = $request->optionalPostParameter('display_name', fn(string $s) => Validator::displayName($s));
        $userMail = $request->optionalPostParameter('user_email', fn(string $s) => Validator::userEmail($s));

        if (null !== $this->storage->localUserGet($userId)) {
            throw new HttpException('user already exists', 400);
        }

        // we generate a password for the account and show that on the next
        // page...
        $userPass = Base64UrlSafe::encodeUnpadded(random_bytes(16));

        $this->storage->localUserAdd($userId, new DateTimeImmutable());
        $this->credentialValidator->setAuthPass($userId, $userPass);
        $this->storage->localUserAddAttribute($userId, 'uid', $userId);
        if (null !== $displayName) {
            $this->storage->localUserAddAttribute($userId, 'displayName', $displayName);
        }
        if (null !== $userMail) {
            $this->storage->localUserAddAttribute($userId, 'mail', $userMail);
        }

        return new HtmlResponse(
            $this->tpl->render(
                'userPass',
                [
                    'userId' => $userId,
                    'userPass' => $userPass,
                ]
            )
        );
    }
}
