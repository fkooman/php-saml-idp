<?php

declare(strict_types=1);

/*
 * Copyright (c) 2018-2024 François Kooman <fkooman@tuxed.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace fkooman\SAML\IdP\Http;

use Closure;
use fkooman\SAML\IdP\Http\Exception\HttpException;

abstract class Service implements ServiceInterface
{
    protected AuthModuleInterface $authModule;

    /** @var array<string,array<string,Closure(Request):Response>> */
    protected array $beforeAuthRouteList = [];

    /** @var array<string,array<string,Closure(Request,UserInfo):Response>> */
    protected array $routeList = [];

    /** @var array<HookInterface> */
    protected array $beforeHookList = [];

    public function __construct(AuthModuleInterface $authModule)
    {
        $authModule->init($this);
        $this->authModule = $authModule;
    }

    public function addBeforeHook(HookInterface $beforeHook): void
    {
        $this->beforeHookList[] = $beforeHook;
    }

    /**
     * @param Closure(Request,UserInfo):Response $closure
     */
    public function get(string $pathInfo, Closure $closure): void
    {
        $this->routeList[$pathInfo]['GET'] = $closure;
    }

    /**
     * @param Closure(Request,UserInfo):Response $closure
     */
    public function post(string $pathInfo, Closure $closure): void
    {
        $this->routeList[$pathInfo]['POST'] = $closure;
    }

    /**
     * @param Closure(Request):Response $closure
     */
    public function getBeforeAuth(string $pathInfo, Closure $closure): void
    {
        $this->beforeAuthRouteList[$pathInfo]['GET'] = $closure;
    }

    /**
     * @param Closure(Request):Response $closure
     */
    public function postBeforeAuth(string $pathInfo, Closure $closure): void
    {
        $this->beforeAuthRouteList[$pathInfo]['POST'] = $closure;
    }

    public function addModule(ServiceModuleInterface $module): void
    {
        $module->init($this);
    }

    public function run(Request $request): Response
    {
        foreach ($this->beforeHookList as $beforeHook) {
            $hookResponse = $beforeHook->beforeAuth($request);
            // if we get back a Response object, return it immediately
            if ($hookResponse instanceof Response) {
                return $hookResponse;
            }
        }

        $requestMethod = $request->getRequestMethod();
        $pathInfo = $request->getPathInfo();

        // modules can use get/postBeforeAuth that require no authentication,
        // if the current request is for such a URL, execute the callback
        // immediately
        if (\array_key_exists($pathInfo, $this->beforeAuthRouteList) && \array_key_exists($requestMethod, $this->beforeAuthRouteList[$pathInfo])) {
            return $this->beforeAuthRouteList[$pathInfo][$requestMethod]($request);
        }

        // make sure we are authenticated
        if (null === $userInfo = $this->authModule->userInfo($request)) {
            if (null !== $authResponse = $this->authModule->startAuth($request)) {
                return $authResponse;
            }

            throw new HttpException('unable to authenticate user', 401);
        }

        foreach ($this->beforeHookList as $beforeHook) {
            $hookResponse = $beforeHook->afterAuth($request, $userInfo);
            // if we get back a Response object, return it immediately
            if ($hookResponse instanceof Response) {
                return $hookResponse;
            }
        }

        if (!\array_key_exists($pathInfo, $this->routeList)) {
            throw new HttpException(sprintf('"%s" not found', $pathInfo), 404);
        }
        if (!\array_key_exists($requestMethod, $this->routeList[$pathInfo])) {
            throw new HttpException(sprintf('method "%s" not allowed', $requestMethod), 405, ['Allow' => implode(',', array_keys($this->routeList[$pathInfo]))]);
        }

        return $this->routeList[$pathInfo][$requestMethod]($request, $userInfo);
    }
}
