<?php

declare(strict_types=1);

/*
 * Copyright (c) 2018-2024 François Kooman <fkooman@tuxed.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace fkooman\SAML\IdP\Http;

use DateTimeImmutable;

class UserInfo
{
    private string $userId;
    private DateTimeImmutable $authTime;

    // XXX do we need these two here? could be just based on sessions instead
    // and no need to store this in UserInfo object. Do we even *need* a
    // userInfo object? Could just be userId string perhaps?
    private bool $twoFactorVerified = false;
    private bool $isAdmin = false;

    public function __construct(string $userId, DateTimeImmutable $authTime)
    {
        $this->userId = $userId;
        $this->authTime = $authTime;
    }

    public function userId(): string
    {
        return $this->userId;
    }

    public function setIsAdmin(bool $isAdmin): void
    {
        $this->isAdmin = $isAdmin;
    }

    // XXX do we need this here? could be just based on sessions?
    public function setTwoFactorVerified(): void
    {
        $this->twoFactorVerified = true;
    }

    // XXX do we need this here? could be just based on sessions?
    public function getTwoFactorVerified(): bool
    {
        return $this->twoFactorVerified;
    }

    public function authTime(): DateTimeImmutable
    {
        return $this->authTime;
    }

    public function isAdmin(): bool
    {
        return $this->isAdmin;
    }
}
