<?php

declare(strict_types=1);

/*
 * Copyright (c) 2018-2024 François Kooman <fkooman@tuxed.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace fkooman\SAML\IdP\Http\Auth;

use DateTimeImmutable;
use fkooman\SAML\IdP\Http\Auth\Exception\CredentialValidatorException;
use fkooman\SAML\IdP\Http\HtmlResponse;
use fkooman\SAML\IdP\Http\RedirectResponse;
use fkooman\SAML\IdP\Http\Request;
use fkooman\SAML\IdP\Http\Response;
use fkooman\SAML\IdP\Http\ServiceInterface;
use fkooman\SAML\IdP\Http\SessionInterface;
use fkooman\SAML\IdP\Http\UserInfo;
use fkooman\SAML\IdP\TplInterface;
use fkooman\SAML\IdP\Validator;

class UserPassAuthModule extends AbstractAuthModule
{
    protected TplInterface $tpl;
    private CredentialValidatorInterface $credentialValidator;
    private SessionInterface $session;

    public function __construct(CredentialValidatorInterface $credentialValidator, SessionInterface $session, TplInterface $tpl)
    {
        $this->credentialValidator = $credentialValidator;
        $this->session = $session;
        $this->tpl = $tpl;
    }

    public function init(ServiceInterface $service): void
    {
        $service->postBeforeAuth(
            '/_user_pass_auth/verify',
            function (Request $request): Response {
                $this->session->remove('_user_pass_auth_user_id');

                $authUser = $request->requirePostParameter('userName', fn(string $s) => Validator::userId($s));
                $authPass = $request->requirePostParameter('userPass', fn(string $s) => Validator::userAuthPass($s));
                $redirectTo = $request->requirePostParameter('_user_pass_auth_redirect_to', fn(string $s) => Validator::matchesOrigin($request->getOrigin(), $s));

                try {
                    $userInfo = new UserInfo(
                        $this->credentialValidator->validate($authUser, $authPass),
                        new DateTimeImmutable()
                    );
                    $this->session->set('_user_pass_auth_user_id', $userInfo->userId());
                    $this->session->set('_user_pass_auth_auth_time', $userInfo->authTime()->format(DateTimeImmutable::ATOM));

                    return new RedirectResponse($redirectTo);
                } catch (CredentialValidatorException $e) {
                    // invalid authentication
                    $responseBody = $this->tpl->render(
                        'userPassAuth',
                        [
                            '_user_pass_auth_invalid_credentials' => true,
                            '_user_pass_auth_invalid_credentials_user' => $authUser,
                            '_user_pass_auth_redirect_to' => $redirectTo,
                            'showLogoutButton' => false,
                        ]
                    );

                    return new HtmlResponse($responseBody);
                }
            }
        );
    }

    public function userInfo(Request $request): ?UserInfo
    {
        if (null === $authUser = $this->session->get('_user_pass_auth_user_id')) {
            return null;
        }

        $authTime = new DateTimeImmutable();
        if (null !== $sessionAuthTime = $this->session->get('_user_pass_auth_auth_time')) {
            $authTime = new DateTimeImmutable($sessionAuthTime);
        }

        return new UserInfo(
            $authUser,
            $authTime
        );
    }

    public function startAuth(Request $request): ?Response
    {
        $responseBody = $this->tpl->render(
            'userPassAuth',
            [
                '_user_pass_auth_invalid_credentials' => false,
                '_user_pass_auth_redirect_to' => $request->getUri(),
                'showLogoutButton' => false,
            ]
        );

        return new HtmlResponse($responseBody, [], 200);
    }
}
