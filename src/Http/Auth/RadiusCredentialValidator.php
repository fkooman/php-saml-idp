<?php

declare(strict_types=1);

/*
 * Copyright (c) 2018-2024 François Kooman <fkooman@tuxed.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace fkooman\SAML\IdP\Http\Auth;

use fkooman\OAuth\Server\AttributeFetcherInterface;
use fkooman\Radius\Exception\AccessRejectException;
use fkooman\Radius\RadiusClient;
use fkooman\Radius\ServerInfo;
use fkooman\SAML\IdP\Cfg\RadiusAuthConfig;
use fkooman\SAML\IdP\Http\Auth\Exception\CredentialValidatorException;
use fkooman\SAML\IdP\LoggerInterface;

class RadiusCredentialValidator implements CredentialValidatorInterface, AttributeFetcherInterface
{
    private LoggerInterface $logger;
    private RadiusClient $radiusClient;

    public function __construct(RadiusAuthConfig $radiusAuthConfig, LoggerInterface $logger)
    {
        $this->radiusClient = new RadiusClient();
        foreach ($radiusAuthConfig->serverList() as $radiusServer) {
            $this->radiusClient->addServer(new ServerInfo($radiusServer, $radiusAuthConfig->serverSecret()));
        }
        $this->logger = $logger;
    }

    /**
     * Validate a user's credentials are return the (normalized) internal
     * user ID to use.
     */
    public function validate(string $authUser, string $authPass): string
    {
        try {
            $accessResponse = $this->radiusClient->accessRequest($authUser, $authPass);

            // "normalize" the User-Name if we get one in the RADIUS response
            if (null !== $userName = $accessResponse->attributeCollection()->getOne('User-Name')) {
                $authUser = $userName;
            }

            return $authUser;
        } catch (AccessRejectException $e) {
            throw new CredentialValidatorException($e->getMessage());
        }
    }

    /**
     * Update a user's password after validating the current password.
     */
    public function updateAuthPass(string $authUser, string $currentAuthPass, string $newAuthPass): void
    {
        // NOP
    }

    /**
     * Set a user's password without validating the current password.
     */
    public function setAuthPass(string $authUser, string $newAuthPass): void
    {
        // NOP
    }

    /**
     * Obtain attributes and values for specified user.
     *
     * If the user does not exist (anymore), `null` is returned. If there are
     * no attributes (and values) for the user, an empty `array` is returned.
     *
     * @return ?array<string,array<string>>
     */
    public function attributesForUser(string $userId): ?array
    {
        return [
            'uid' => [$userId],
        ];
    }
}
