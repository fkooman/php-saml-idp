<?php

declare(strict_types=1);

/*
 * Copyright (c) 2018-2024 François Kooman <fkooman@tuxed.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace fkooman\SAML\IdP\Http\Auth;

use fkooman\OAuth\Server\AttributeFetcherInterface;
use fkooman\SAML\IdP\Cfg\LdapAuthConfig;
use fkooman\SAML\IdP\Exception\LdapClientException;
use fkooman\SAML\IdP\Http\Auth\Exception\CredentialValidatorException;
use fkooman\SAML\IdP\LdapClient;
use fkooman\SAML\IdP\LoggerInterface;

class LdapCredentialValidator implements CredentialValidatorInterface, AttributeFetcherInterface
{
    private LdapAuthConfig $ldapAuthConfig;

    private LoggerInterface $logger;

    private LdapClient $ldapClient;

    public function __construct(LdapAuthConfig $ldapAuthConfig, LoggerInterface $logger)
    {
        $this->ldapAuthConfig = $ldapAuthConfig;
        $this->logger = $logger;
        $this->ldapClient = new LdapClient(
            $ldapAuthConfig->ldapUri()
        );
    }

    /**
     * Validate a user's credentials are return the (normalized) internal
     * user ID to use.
     */
    public function validate(string $authUser, string $authPass): string
    {
        $bindDn = $this->authUserToDn($authUser);

        try {
            $this->ldapClient->bind($bindDn, $authPass);
            $attributeNameValueList = $this->attributesForDn($bindDn);
            $userIdAttribute = $this->ldapAuthConfig->userIdAttribute();
            if (!isset($attributeNameValueList[$userIdAttribute][0])) {
                throw new CredentialValidatorException('unable to find uid');
            }

            return $attributeNameValueList[$userIdAttribute][0];
        } catch (LdapClientException $e) {
            // convert LDAP errors into `CredentialValidatorException`
            throw new CredentialValidatorException($e->getMessage());
        }
    }

    /**
     * Update a user's password after validating the current password.
     */
    public function updateAuthPass(string $authUser, string $currentAuthPass, string $newAuthPass): void
    {
        $bindDn = $this->authUserToDn($authUser);
        $this->ldapClient->bind($bindDn, $currentAuthPass);
        // XXX do we also need to provide the current password when setting
        // the password after we already bound as the user?
        $this->ldapClient->passwd($bindDn, $currentAuthPass, $newAuthPass);
    }

    /**
     * Set a user's password without validating the current password.
     */
    public function setAuthPass(string $authUser, string $newAuthPass): void
    {
        $bindDn = $this->authUserToDn($authUser);
        //        $this->ldapClient->bind($bindDn, $currentAuthPass);
        // XXX do we also need to provide the current password when setting
        // the password after we already bound as the user?
        $this->ldapClient->passwd($bindDn, '', $newAuthPass);
    }

    /**
     * Obtain attributes and values for specified user.
     *
     * If the user does not exist (anymore), `null` is returned. If there are
     * no attributes (and values) for the user, an empty `array` is returned.
     *
     * @return ?array<string,array<string>>
     */
    public function attributesForUser(string $userId): ?array
    {
        return $this->attributesForDn(
            $this->authUserToDn($userId)
        );
    }

    /**
     * @param array<string,array<string>> $attributeNameValues
     */
    public function addUser(string $userId, string $userPass, array $attributeNameValues): void
    {
        if (null === $bindDnTemplate = $this->ldapAuthConfig->bindDnTemplate()) {
            throw new CredentialValidatorException('we do not know how to construct a DN from a userId');
        }
        $this->ldapClient->bind($this->ldapAuthConfig->adminBindDn(), $this->ldapAuthConfig->adminBindPass());
        $userDn = str_replace('{{UID}}', LdapClient::escapeDn($userId), $bindDnTemplate);
        $this->ldapClient->add(
            $userDn,
            array_merge(
                $attributeNameValues,
                [
                    'objectClass' => [
                        'person',
                        'inetOrgPerson',
                    ],
                ]
            )
        );
        $this->setAuthPass($userId, $userPass);
    }

    private function authUserToDn(string $authUser): string
    {
        try {
            // add "realm" after user name if none is specified
            if (null !== $addRealm = $this->ldapAuthConfig->addRealm()) {
                if (!str_contains($authUser, '@')) {
                    $authUser .= '@' . $addRealm;
                }
            }

            if (null !== $bindDnTemplate = $this->ldapAuthConfig->bindDnTemplate()) {
                // we have a bind DN template to bind to the LDAP with the user's
                // provided "Username", so use that
                return str_replace('{{UID}}', LdapClient::escapeDn($authUser), $bindDnTemplate);
            }

            // Do (anonymous) LDAP bind to find the DN based on userFilterTemplate
            $this->ldapClient->bind($this->ldapAuthConfig->searchBindDn(), $this->ldapAuthConfig->searchBindPass());
            if (null === $userFilterTemplate = $this->ldapAuthConfig->userFilterTemplate()) {
                throw new CredentialValidatorException(__CLASS__ . ': "userFilterTemplate" not set, unable to search for DN');
            }

            $userFilter = str_replace('{{UID}}', LdapClient::escapeFilter($authUser), $userFilterTemplate);
            if (null === $baseDn = $this->ldapAuthConfig->baseDn()) {
                throw new CredentialValidatorException(__CLASS__ . ': "baseDn" not set, unable to search for DN');
            }

            if (null === $ldapEntries = $this->ldapClient->search($baseDn, $userFilter)) {
                throw new CredentialValidatorException(__CLASS__ . ': user not found');
            }

            return $ldapEntries['dn'];
        } catch (LdapClientException $e) {
            // convert LDAP errors into `CredentialValidatorException`
            throw new CredentialValidatorException($e->getMessage());
        }
    }

    /**
     * Obtain attributes and values for specified user.
     *
     * If the user does not exist (anymore), `null` is returned. If there are
     * no attributes (and values) for the user, an empty `array` is returned.
     *
     * @return ?array<string,array<string>>
     */
    private function attributesForDn(string $userDn): ?array
    {
        try {
            $attributeNameList = $this->ldapAuthConfig->attributeNameList();
            if (0 === count($attributeNameList)) {
                return [];
            }

            if (null === $ldapEntries = $this->ldapClient->search($userDn, null, $attributeNameList)) {
                // user not found, i.e. no results
                return null;
            }

            return $ldapEntries['result'];
        } catch (LdapClientException $e) {
            // convert LDAP errors into `CredentialValidatorException`
            throw new CredentialValidatorException($e->getMessage());
        }
    }
}
