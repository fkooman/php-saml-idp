<?php

declare(strict_types=1);

/*
 * Copyright (c) 2018-2024 François Kooman <fkooman@tuxed.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace fkooman\SAML\IdP\Http\Auth;

use fkooman\SAML\IdP\Http\AuthModuleInterface;
use fkooman\SAML\IdP\Http\RedirectResponse;
use fkooman\SAML\IdP\Http\Request;
use fkooman\SAML\IdP\Http\Response;
use fkooman\SAML\IdP\Http\ServiceInterface;
use fkooman\SAML\IdP\Http\UserInfo;

abstract class AbstractAuthModule implements AuthModuleInterface
{
    public function init(ServiceInterface $service): void {}

    public function userInfo(Request $request): ?UserInfo
    {
        return null;
    }

    public function startAuth(Request $request): ?Response
    {
        return null;
    }

    public function triggerLogout(Request $request): Response
    {
        // by default we return to the place the users came from, it is up to
        // authentication mechanisms that implement their own logout, e.g.
        // SAML authentication to override this method
        return new RedirectResponse($request->requireReferrer());
    }
}
