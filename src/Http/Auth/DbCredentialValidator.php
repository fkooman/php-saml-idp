<?php

declare(strict_types=1);

/*
 * Copyright (c) 2018-2024 François Kooman <fkooman@tuxed.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace fkooman\SAML\IdP\Http\Auth;

use DateTimeImmutable;
use fkooman\OAuth\Server\AttributeFetcherInterface;
use fkooman\SAML\IdP\Http\Auth\Exception\CredentialValidatorException;
use fkooman\SAML\IdP\Storage;

class DbCredentialValidator implements CredentialValidatorInterface, AttributeFetcherInterface
{
    private const PWD_ALG = PASSWORD_ARGON2ID;
    private Storage $storage;

    public function __construct(Storage $storage)
    {
        $this->storage = $storage;
    }

    /**
     * Validate a user's credentials are return the (normalized) internal
     * user ID to use.
     */
    public function validate(string $authUser, string $authPass): string
    {
        if (null === $userInfo = $this->storage->localUserGet($authUser)) {
            throw new CredentialValidatorException('no such user');
        }
        if ($userInfo['is_disabled']) {
            throw new CredentialValidatorException('user is disabled');
        }
        if (password_verify($authPass, $userInfo['password_hash'])) {
            // rehash password with Argon2I if not in Argon2I format yet
            $this->rehashPassword($authUser, $authPass, $userInfo['password_hash']);

            return $authUser;
        }

        throw new CredentialValidatorException('invalid password');
    }

    /**
     * Set a user's password without validating the current password.
     */
    public function setAuthPass(string $authUser, string $newAuthPass): void
    {
        $this->storage->localUserSetPassword(
            $authUser,
            password_hash($newAuthPass, self::PWD_ALG)
        );
    }

    /**
     * Update a user's password after validating the current password.
     */
    public function updateAuthPass(string $authUser, string $currentAuthPass, string $newAuthPass): void
    {
        $this->validate($authUser, $currentAuthPass);
        $this->setAuthPass($authUser, $newAuthPass);
    }

    /**
     * Obtain attributes and values for specified user.
     *
     * If the user does not exist (anymore), `null` is returned. If there are
     * no attributes (and values) for the user, an empty `array` is returned.
     *
     * @return ?array<string,array<string>>
     */
    public function attributesForUser(string $userId): ?array
    {
        if (null === $this->storage->localUserGet($userId)) {
            return null;
        }

        $userAttributes = $this->storage->localUserAttributes($userId);

        // also add all groups the user is a member of
        $isMemberOf = [];
        foreach ($this->storage->localUserGroupList($userId) as $userGroup) {
            $isMemberOf[] = $userGroup['group_id'];
        }
        if (0 !== count($isMemberOf)) {
            $userAttributes['isMemberOf'] = $isMemberOf;
        }

        return $userAttributes;
    }

    /**
     * @param array<string,array<string>> $attributeNameValues
     */
    public function addUser(string $userId, string $userPass, array $attributeNameValues): void
    {
        $this->storage->localUserAdd($userId, new DateTimeImmutable());
        $this->setAuthPass($userId, $userPass);
        $this->storage->localUserAddAttribute($userId, 'uid', $userId);
    }

    private function rehashPassword(string $authUser, string $authPass, string $passwordHash): void
    {
        if (str_starts_with($passwordHash, '$argon2id$')) {
            // already Argon2, do nothing
            return;
        }
        $this->setAuthPass($authUser, $authPass);
    }
}
