<?php

declare(strict_types=1);

/*
 * Copyright (c) 2018-2024 François Kooman <fkooman@tuxed.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace fkooman\SAML\IdP\Http;

/**
 * Registers an endpoint that the portal "POSTs" to in order to trigger
 * the logout action. It will _also_ allow for authentication mechanisms to
 * "do something extra" in case logout is triggered, for example stop the SAML
 * session when using a SAML authentication backend.
 *
 * NOTE: not all authentication mechanisms support logout, e.g. BasicAuth or
 * ClientCertAuth, they will require the user to close the browser or restart
 * the device.
 */
class LogoutModule implements ServiceModuleInterface
{
    private AuthModuleInterface $authModule;
    private SessionInterface $session;

    public function __construct(AuthModuleInterface $authModule, SessionInterface $session)
    {
        $this->authModule = $authModule;
        $this->session = $session;
    }

    public function init(ServiceInterface $service): void
    {
        $service->get(
            '/_logout',
            function (Request $request, UserInfo $userInfo): Response {
                // destroy our local session before triggering any (external)
                // mechanism to facilitate logout
                $this->session->destroy();

                return $this->authModule->triggerLogout($request);
            }
        );
    }
}
