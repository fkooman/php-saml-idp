<?php

declare(strict_types=1);

/*
 * Copyright (c) 2018-2024 François Kooman <fkooman@tuxed.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace fkooman\SAML\IdP\Tests;

use fkooman\SAML\IdP\SAML\Attributes;
use PHPUnit\Framework\TestCase;

/**
 * @coversNothing
 */
class AttributesTest extends TestCase
{
    public function testNoMapping(): void
    {
        $this->assertSame(
            [
                'urn:oid:0.9.2342.19200300.100.1.1' => ['fkooman'],
                'urn:oid:1.3.6.1.4.1.5923.1.1.1.7' => ['my-entitlement', 'http://eduvpn.org/role/admin'],
                'urn:oid:0.9.2342.19200300.100.1.3' => ['fkooman@tuxed.net'],
                'urn:oid:1.3.6.1.4.1.5923.1.1.1.1' => ['employee', 'member', 'student'],
                'urn:oid:1.3.6.1.4.1.25178.1.2.9' => ['example.org'],
                'urn:oid:0.9.2342.19200300.100.1.41' => ['tel:+4912345'],
            ],
            Attributes::prepare(
                [
                    // check attributes with a valid OID mapping are not
                    // released if they are not part of the release list
                    'cn' => ['F. Kooman'],
                    // we rename an attribute for fun
                    'homePhone' => ['tel:+4912345'],
                    'uid' => ['fkooman'],
                    'isMemberOf' => ['employees', 'students', 'eduvpn-admins'],
                    // this attribute name does not have an OID mapping, so
                    // should be completely ignored
                    'unsupportedAttributeName' => ['x', 'y'],
                    // check that additional values are *added* and not replace
                    // existing values in the mappin
                    'eduPersonEntitlement' => ['my-entitlement'],
                    // check duplicates are removed
                    'mail' => ['fkooman@tuxed.net', 'fkooman@tuxed.net'],
                    // check empty attribute values are removed
                    'displayName' => [],
                ],
                [
                    // map isMemberOf to eduPersonAffiliation, users in group
                    // "students" and "employees" get the respective "student"
                    // and "employee" affiliation AND both get the "member"
                    // affiliation as well
                    [
                        'from_attribute' => 'isMemberOf',
                        'from_value' => 'employees',
                        'to_attribute' => 'eduPersonAffiliation',
                        'to_value' => 'employee',
                    ],
                    [
                        'from_attribute' => 'isMemberOf',
                        'from_value' => 'employees',
                        'to_attribute' => 'eduPersonAffiliation',
                        'to_value' => 'member',
                    ],
                    [
                        'from_attribute' => 'isMemberOf',
                        'from_value' => 'students',
                        'to_attribute' => 'eduPersonAffiliation',
                        'to_value' => 'student',
                    ],
                    [
                        'from_attribute' => 'isMemberOf',
                        'from_value' => 'students',
                        'to_attribute' => 'eduPersonAffiliation',
                        'to_value' => 'member',
                    ],
                    // our user is not a member of "alumni" so should not get
                    // this affiliation
                    [
                        'from_attribute' => 'isMemberOf',
                        'from_value' => 'alumni',
                        'to_attribute' => 'eduPersonAffiliation',
                        'to_value' => 'alum',
                    ],
                    // always add an attribute with a value
                    [
                        'from_attribute' => null,
                        'from_value' => null,
                        'to_attribute' => 'schacHomeOrganization',
                        'to_value' => 'example.org',
                    ],
                    // add an entitlement based on group membership to add to
                    // our existing entitlement
                    [
                        'from_attribute' => 'isMemberOf',
                        'from_value' => 'eduvpn-admins',
                        'to_attribute' => 'eduPersonEntitlement',
                        'to_value' => 'http://eduvpn.org/role/admin',
                    ],
                    // rename 'homePhone' to 'mobile'
                    [
                        'from_attribute' => 'homePhone',
                        'from_value' => null,
                        'to_attribute' => 'mobile',
                        'to_value' => null,
                    ],
                ],
                [
                    'uid',
                    'eduPersonEntitlement',
                    'eduPersonAffiliation',
                    'mail',
                    'displayName',
                    'unsupportedAttributeName',
                    'schacHomeOrganization',
                    'mobile',
                ]
            )
        );
    }
}
