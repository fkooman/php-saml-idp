<?php

declare(strict_types=1);

/*
 * Copyright (c) 2018-2024 François Kooman <fkooman@tuxed.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

require_once dirname(__DIR__) . '/vendor/autoload.php';
$baseDir = dirname(__DIR__);

use fkooman\SAML\IdP\Cfg\Config;
use fkooman\SAML\IdP\LdapClient;
use fkooman\SAML\IdP\Storage;

try {
    $deleteFirst = false;
    foreach ($argv as $arg) {
        if ('-d' === $arg) {
            $deleteFirst = true;
        }
    }

    $config = Config::fromFile($baseDir . '/config/config.php');
    $ldapConfig = $config->ldapAuthConfig();
    $ldapClient = new LdapClient($ldapConfig->ldapUri());
    $ldapClient->bind($ldapConfig->adminBindDn(), $ldapConfig->adminBindPass());

    $storage = new Storage(
        sprintf('sqlite:%s/data/db.sqlite', $baseDir),
        $baseDir . '/schema'
    );

    //      ( 2.5.6.6 NAME 'person'
    //         SUP top
    //         STRUCTURAL
    //         MUST ( sn $
    //               cn )
    //         MAY ( userPassword $
    //               telephoneNumber $
    //               seeAlso $ description ) )



    //( 2.16.840.1.113730.3.2.2
    //    NAME 'inetOrgPerson'
    //    SUP organizationalPerson
    //    STRUCTURAL
    //    MAY (
    //        audio $ businessCategory $ carLicense $ departmentNumber $
    //        displayName $ employeeNumber $ employeeType $ givenName $
    //        homePhone $ homePostalAddress $ initials $ jpegPhoto $
    //        labeledURI $ mail $ manager $ mobile $ o $ pager $
    //        photo $ roomNumber $ secretary $ uid $ userCertificate $
    //        x500uniqueIdentifier $ preferredLanguage $
    //        userSMIMECertificate $ userPKCS12
    //    )
    //    MUST (
    //        cn $ objectClass $ sn
    //    )
    //    MAY (
    //        description $ destinationIndicator $ facsimileTelephoneNumber $
    //        internationaliSDNNumber $ l $ ou $ physicalDeliveryOfficeName $
    //        postalAddress $ postalCode $ postOfficeBox $
    //        preferredDeliveryMethod $ registeredAddress $ seeAlso $
    //        st $ street $ telephoneNumber $ teletexTerminalIdentifier $
    //        telexNumber $ title $ userPassword $ x121Address
    //    )
    //)

    foreach ($storage->localUserList() as $userInfo) {
        $userDn = sprintf('uid=%s,%s', $userInfo['user_id'], $ldapConfig->userBaseDn());
        echo $userDn . PHP_EOL;
        $userAttributes = array_merge(
            $storage->localUserAttributes($userInfo['user_id']),
            [
                'objectClass' => [
                    'person',
                    'inetOrgPerson',
                    'eduPerson',
                ],
                'userPassword' => [
                    sprintf('{ARGON2}%s', $userInfo['password_hash']),
                ],
            ]
        );

        if (!array_key_exists('cn', $userAttributes)) {
            if (!array_key_exists('displayName', $userAttributes)) {
                $userAttributes['cn'] = $userAttributes['displayName'];
            }
        }

        if (!array_key_exists('sn', $userAttributes)) {
            $userAttributes['sn'] = ['N/A'];
        }
        var_dump($userAttributes);

        if ($deleteFirst) {
            $ldapClient->delete($userDn);
        }
        $ldapClient->add($userDn, $userAttributes);

        //        echo sprintf('dn: uid=%s,%s', $userInfo['user_id'], $baseDn) . PHP_EOL;
        //        echo sprintf('userPassword: {ARGON2}%s', $userInfo['password_hash']) . PHP_EOL;
        //        $hasEduPerson = false;
        //        $attributeList = $storage->localUserAttributes($userInfo['user_id']);
        //        if (!array_key_exists('cn', $attributeList)) {
        //            echo 'cn: ' . $userInfo['user_id'] . PHP_EOL;
        //        }
        //        if (!array_key_exists('sn', $attributeList)) {
        //            echo 'sn: Placeholder' . PHP_EOL;
        //        }
        //        foreach ($attributeList as $attributeName => $attributeValueList) {
        //            if (0 === strpos($attributeName, 'eduPerson')) {
        //                $hasEduPerson = true;
        //            }
        //            foreach ($attributeValueList as $attributeValue) {
        //                echo $attributeName . ': ' . $attributeValue . PHP_EOL;
        //            }
        //        }
        //        echo 'objectClass: person' . PHP_EOL;
        //        echo 'objectClass: inetOrgPerson' . PHP_EOL;
        //        if ($hasEduPerson) {
        //            echo 'objectClass: eduPerson' . PHP_EOL;
        //        }
        //        echo '-' . PHP_EOL;
    }
} catch (Exception $e) {
    echo sprintf('ERROR: %s', $e->getMessage()) . \PHP_EOL;

    exit(1);
}
