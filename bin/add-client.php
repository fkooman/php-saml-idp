<?php

declare(strict_types=1);

/*
 * Copyright (c) 2018-2024 François Kooman <fkooman@tuxed.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

require_once dirname(__DIR__) . '/vendor/autoload.php';
$baseDir = dirname(__DIR__);

use fkooman\OAuth\Server\ClientInfo;
use fkooman\OAuth\Server\PdoClientDb;
use fkooman\OAuth\Server\Scope;
use fkooman\SAML\IdP\Storage;

// allow group to read the created files/folders
umask(0o027);

try {
    $storage = new Storage(
        sprintf('sqlite:%s/data/db.sqlite', $baseDir),
        $baseDir . '/schema'
    );

    echo 'Client Name: ';
    $clientName = trim(fgets(\STDIN));
    if (empty($clientName)) {
        throw new RuntimeException('"Client Name" MUST NOT be empty');
    }

    echo 'Redirect URI: ';
    $redirectUri = trim(fgets(\STDIN));
    if (empty($redirectUri)) {
        throw new RuntimeException('"Redirect URI" MUST NOT be empty');
    }

    $clientId = sodium_bin2hex(random_bytes(16));
    $clientSecret = sodium_bin2hex(random_bytes(16));

    echo PHP_EOL;
    echo '*** Added ***' . PHP_EOL;
    echo 'Client ID    : ' . $clientId . PHP_EOL;
    echo 'Client Secret : ' . $clientSecret . PHP_EOL;
    echo 'Client Name : ' . $clientName . PHP_EOL;
    echo 'Redirect URI : ' . $redirectUri . PHP_EOL;

    $clientDb = new PdoClientDb($storage->dbPdo(), 'oidc_rps');
    $clientDb->add(
        new ClientInfo(
            $clientId,
            [
                $redirectUri,
            ],
            $clientName,
            $clientSecret,
            true,
            new Scope('openid')
        )
    );
} catch (Exception $e) {
    echo sprintf('ERROR: %s', $e->getMessage()) . \PHP_EOL;

    exit(1);
}
