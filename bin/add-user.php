<?php

declare(strict_types=1);

/*
 * Copyright (c) 2018-2024 François Kooman <fkooman@tuxed.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

require_once dirname(__DIR__) . '/vendor/autoload.php';
$baseDir = dirname(__DIR__);

use fkooman\SAML\IdP\Cfg\Config;
use fkooman\SAML\IdP\Http\Auth\DbCredentialValidator;
use fkooman\SAML\IdP\Http\Auth\LdapCredentialValidator;
use fkooman\SAML\IdP\NullLogger;
use fkooman\SAML\IdP\Storage;

try {
    echo 'User ID: ';
    $userId = trim(fgets(\STDIN));
    if (empty($userId)) {
        throw new RuntimeException('User ID cannot be empty');
    }

    echo sprintf('Setting password for user "%s"', $userId) . \PHP_EOL;
    // ask for password
    exec('stty -echo');
    echo 'Password: ';
    $userPass = trim(fgets(\STDIN));
    echo \PHP_EOL . 'Password (repeat): ';
    $userPassRepeat = trim(fgets(\STDIN));
    exec('stty echo');
    echo \PHP_EOL;
    if ($userPass !== $userPassRepeat) {
        throw new RuntimeException('specified passwords do not match');
    }
    if (empty($userPass)) {
        throw new RuntimeException('Password cannot be empty');
    }

    $attributeNameValueList = [
        'uid' => [$userId],
    ];

    $config = Config::fromFile($baseDir . '/config/config.php');
    switch ($config->authModule()) {
        case 'DbAuthModule':
            $storage = new Storage(
                sprintf('sqlite:%s/data/db.sqlite', $baseDir),
                $baseDir . '/schema'
            );
            $credentialValidator = new DbCredentialValidator($storage);

            break;
        case 'LdapAuthModule':
            // objectClass "person" (and inetOrgPerson) need "cn" and "sn"
            echo 'Common Name (cn): ';
            $cn = trim(fgets(\STDIN));
            if (empty($cn)) {
                throw new RuntimeException('Common Name cannot be empty');
            }
            $attributeNameValueList['cn'] = [$cn];
            echo 'Surname (sn): ';
            $sn = trim(fgets(\STDIN));
            if (empty($sn)) {
                throw new RuntimeException('Surname cannot be empty');
            }
            $attributeNameValueList['sn'] = [$sn];
            $credentialValidator = new LdapCredentialValidator(
                $config->ldapAuthConfig(),
                new NullLogger()
            );

            break;
        default:
            throw new RuntimeException('unsupported "authModule"');
    }
    $credentialValidator->addUser(
        $userId,
        $userPass,
        $attributeNameValueList
    );
} catch (Exception $e) {
    echo sprintf('ERROR: %s', $e->getMessage()) . \PHP_EOL;

    exit(1);
}
