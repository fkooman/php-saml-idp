<?php

declare(strict_types=1);

/*
 * Copyright (c) 2018-2024 François Kooman <fkooman@tuxed.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

require_once dirname(__DIR__) . '/vendor/autoload.php';
$baseDir = dirname(__DIR__);

use fkooman\Jwt\SecretKeySet;
use fkooman\OAuth\Server\OAuthServer;
use fkooman\SAML\IdP\Cfg\Config;
use fkooman\SAML\IdP\FileIO;
use fkooman\SAML\IdP\Http\JsonResponse;
use fkooman\SAML\IdP\Http\Request;
use fkooman\SAML\IdP\Json;

try {
    $config = Config::fromFile($baseDir . '/config/config.php');
    $secretKeySet = SecretKeySet::fromJwks(Json::decode(FileIO::read(sprintf('%s/config/keys/openid.jwks', $baseDir))));
    $algList = [];
    foreach ($secretKeySet->getAll() as $secretKey) {
        if (!in_array($secretKey->keyAlgorithm(), $algList, true)) {
            $algList[] = $secretKey->keyAlgorithm();
        }
    }

    $request = Request::createFromGlobals();
    switch ($request->getPathInfo()) {
        case '/.well-known/openid-configuration':
            $response = new JsonResponse(
                OAuthServer::metadata(
                    $request->getOrigin(),
                    $request->getRootUri() . 'openid/authorize',
                    $request->getRootUri() . 'openid/token',
                    $request->getRootUri() . 'openid/userinfo',
                    $algList,
                    $request->getRootUri() . 'openid/jwks'
                )
            );

            break;
        default:
            $response = new JsonResponse(['error' => 'not found'], [], 404);
    }

    $response->send();
} catch (Exception $e) {
    $response = new JsonResponse(
        ['error' => $e::class . ':' . $e->getMessage()],
        [],
        500
    );
    $response->send();
}
