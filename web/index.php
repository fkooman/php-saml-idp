<?php

declare(strict_types=1);

/*
 * Copyright (c) 2018-2024 François Kooman <fkooman@tuxed.net>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

require_once dirname(__DIR__) . '/vendor/autoload.php';
$baseDir = dirname(__DIR__);

use fkooman\Jwt\SecretKeySet;
use fkooman\OAuth\Server\BearerValidator as OidcBearerValidator;
use fkooman\OAuth\Server\LocalAccessTokenVerifier;
use fkooman\OAuth\Server\OAuthServer;
use fkooman\OAuth\Server\PairwiseSubject;
use fkooman\OAuth\Server\PdoClientDb;
use fkooman\OAuth\Server\PdoStorage as OAuthPdoStorage;
use fkooman\OAuth\Server\SecretBox;
use fkooman\OAuth\Server\UserInfoEndpoint;
use fkooman\Otp\Storage as OtpStorage;
use fkooman\SAML\IdP\Cfg\Config;
use fkooman\SAML\IdP\FileIO;
use fkooman\SAML\IdP\GroupAdminModule;
use fkooman\SAML\IdP\Http\AdminHook;
use fkooman\SAML\IdP\Http\Auth\DbCredentialValidator;
use fkooman\SAML\IdP\Http\Auth\LdapCredentialValidator;
use fkooman\SAML\IdP\Http\Auth\RadiusCredentialValidator;
use fkooman\SAML\IdP\Http\Auth\UserPassAuthModule;
use fkooman\SAML\IdP\Http\HtmlService;
use fkooman\SAML\IdP\Http\LogoutModule;
use fkooman\SAML\IdP\Http\Request;
use fkooman\SAML\IdP\Http\Response;
use fkooman\SAML\IdP\Http\SeSession;
use fkooman\SAML\IdP\Json;
use fkooman\SAML\IdP\MyTpl;
use fkooman\SAML\IdP\OIDC\JwtSigner;
use fkooman\SAML\IdP\OIDC\OpenIdModule;
use fkooman\SAML\IdP\OidcAdminModule;
use fkooman\SAML\IdP\SAML\Certificate;
use fkooman\SAML\IdP\SAML\Key;
use fkooman\SAML\IdP\SAML\PdoSpDb;
use fkooman\SAML\IdP\SAML\SamlModule;
use fkooman\SAML\IdP\SamlAdminModule;
use fkooman\SAML\IdP\Storage;
use fkooman\SAML\IdP\SysLogger;
use fkooman\SAML\IdP\TotpModule;
use fkooman\SAML\IdP\UserAdminModule;
use fkooman\SAML\IdP\UserModule;
use fkooman\SeCookie\CookieOptions;

try {
    $logger = new SysLogger('php-saml-idp');

    $config = Config::fromFile($baseDir . '/config/config.php');
    $request = Request::createFromGlobals();

    $enableUserManagement = 'DbAuthModule' === $config->authModule();
    $enablePasswordReset = 'RadiusAuthModule' !== $config->authModule();

    $tpl = new MyTpl(
        $baseDir,
        [
            'isAdmin' => true,
            'enableUserManagement' => $enableUserManagement,
            'enablePasswordReset' => $enablePasswordReset,
            'enabledLanguages' => $config->enabledLanguages(),
            'showLogoutButton' => true,
            'displayName' => $config->displayName(),
        ]
    );
    $tpl->setAppRoot($request->getRoot());
    $samlKey = Key::fromFile($baseDir . '/config/keys/saml.key');
    $samlCert = Certificate::fromFile($baseDir . '/config/keys/saml.crt');

    $storage = new Storage(
        sprintf('sqlite:%s/data/db.sqlite', $baseDir),
        $baseDir . '/schema'
    );

    $seSession = new SeSession(CookieOptions::init()->withPath($request->getRoot()));
    $credentialValidator = null;
    switch ($config->authModule()) {
        case 'DbAuthModule':
            $credentialValidator = new DbCredentialValidator($storage);
            $authModule = new UserPassAuthModule(
                $credentialValidator,
                $seSession,
                $tpl
            );
            $attributeFetcher = $credentialValidator;

            break;
        case 'LdapAuthModule':
            $credentialValidator = new LdapCredentialValidator(
                $config->ldapAuthConfig(),
                $logger
            );
            $authModule = new UserPassAuthModule(
                $credentialValidator,
                $seSession,
                $tpl
            );
            $attributeFetcher = $credentialValidator;

            break;
        case 'RadiusAuthModule':
            $credentialValidator = new RadiusCredentialValidator(
                $config->radiusAuthConfig(),
                $logger
            );
            $authModule = new UserPassAuthModule(
                $credentialValidator,
                $seSession,
                $tpl
            );
            $attributeFetcher = $credentialValidator;

            break;
        default:
            throw new RuntimeException('unsupported authentication mechanism');
    }

    $service = new HtmlService($authModule, $tpl);

    $clientDb = new PdoClientDb($storage->dbPdo(), 'oidc_rps');
    $pdoSpDb = new PdoSpDb($storage->dbPdo());

    // User UI
    $service->addModule(
        new UserModule(
            $attributeFetcher,
            $credentialValidator,
            $tpl
        )
    );

    // User Admin UI
    if ($enableUserManagement) {
        $service->addModule(
            new UserAdminModule(
                $config,
                $storage,
                $credentialValidator,
                $tpl,
                new OtpStorage($storage->dbPdo())
            )
        );
        $service->addModule(
            new GroupAdminModule(
                $storage,
                $tpl
            )
        );
    }

    $service->addModule(
        new SamlAdminModule(
            $storage,
            $tpl,
            $pdoSpDb
        )
    );

    // OpenID
    $secretKeySet = SecretKeySet::fromJwks(Json::decode(FileIO::read(sprintf('%s/config/keys/openid.jwks', $baseDir))));
    $secretKey = $secretKeySet->first();
    if (null !== $useKey = $config->useKey()) {
        if (null === $secretKey = $secretKeySet->get($useKey)) {
            throw new RuntimeException(sprintf('no such key "%s"', $useKey));
        }
    }

    $jwtSigner = new JwtSigner($secretKey);
    $secretBox = SecretBox::loadKey(FileIO::read($baseDir . '/config/keys/oauth.key'));

    $service->addModule(
        new OidcAdminModule(
            $tpl,
            $clientDb
        )
    );

    // TOTP
    $totpModule = new TotpModule($storage, $seSession, $tpl);
    $service->addModule($totpModule);
    $service->addBeforeHook($totpModule);
    $service->addBeforeHook(new AdminHook($config->adminUserIdList(), $tpl));

    $oauthStorage = new OAuthPdoStorage($storage->dbPdo(), 'oauth_');
    $pairwiseSubject = new PairwiseSubject(
        FileIO::read($baseDir . '/config/keys/hmac.key'),
        $request->getOrigin()
    );

    $oauthServer = new OAuthServer(
        $oauthStorage,
        $clientDb,
        $secretBox,
        $jwtSigner,
        $request->getOrigin(),  // iss
        $pairwiseSubject
    );

    $userInfoEndpoint = new UserInfoEndpoint(
        $attributeFetcher,
        $pairwiseSubject
    );

    $bearerValidator = new OidcBearerValidator(
        $secretBox,
        new LocalAccessTokenVerifier($clientDb, $oauthStorage)
    );

    $service->addModule(
        new OpenIdModule(
            $storage,
            $seSession,
            $oauthServer,
            $bearerValidator,
            $userInfoEndpoint,
            $secretKeySet->publicKeySet(),
            $totpModule,
            $tpl
        )
    );

    // SAML
    $service->addModule(
        new SamlModule(
            $attributeFetcher,
            $storage,
            $logger,
            $config,
            $pdoSpDb,
            $seSession,
            $tpl,
            $totpModule,
            $samlKey,
            $samlCert,
            FileIO::read($baseDir . '/config/keys/salt.key')
        )
    );

    $service->addModule(new LogoutModule($authModule, $seSession));

    $r = $service->run($request);
    $seSession->stop();
    $r->send();
} catch (Exception $e) {
    $response = new Response(
        'ERROR: ' . $e->getMessage(),
        [],
        500
    );
    $response->send();
}
